#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

PROJECT_DIR=$SCRIPTPATH
FRONTEND_DIR=$PROJECT_DIR"/src/main/frontend"

echo $FRONTEND_DIR

cd $FRONTEND_DIR && npm run package && cd $PROJECT_DIR && mvn -Dmaven.test.skip=true package

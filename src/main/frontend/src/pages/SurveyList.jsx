import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {
  findSurveys,
  findSurveysLastCreated,
  findSurveysLatestContributions,
  findSurveysOrganization,
} from '../actions/api';

import * as Utils from '../utils';

import CreateButton from '../components/CreateButton';
import Spacer from '../components/Spacer';

import PagedEntities from '../components/PagedEntities';
import BrowsableEntity from '../components/BrowsableEntity';

class SurveyList extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.cookie.organization.selected !== this.props.cookie.organization.selected) {
      const { selected } = nextProps.cookie.organization;
      if (selected) {
        this.props.findSurveysOrganization(selected.organizationId, 5);
      }
    }
  }

  renderFormTitle = (title) => {
    const DELIMITER = ';';

    if (title.includes(DELIMITER)) {
      const mainTitle = title.substring(0, title.indexOf(DELIMITER));
      const subTitle = title.substring(title.indexOf(DELIMITER) + 1);
      return (
        <div>
          {mainTitle}
          <br />
          <small>{subTitle}</small>
        </div>
      );
    }

    return <div>{title}</div>;
  };

  renderSurveys = () => (
    <div className="row">
      <div className="col-sm-6">
        <PagedEntities
          title="Latest surveys"
          entities={this.props.surveys.lastCreated}
          recall={(size) => this.props.findSurveysLastCreated(size)}
          renderItem={Utils.renderItemSurvey}
          showDate
        />
      </div>
      <div className="col-sm-6">
        <PagedEntities
          title="Most recent contributions"
          entities={this.props.surveys.latestContributions}
          recall={(size) => this.props.findSurveysLatestContributions(size)}
          renderItem={Utils.renderItemSurvey}
          getDate={(e) => e.latestContribution}
          showDate
        />
      </div>
    </div>
  );

  renderEntity = (e) => {
    const { formId, formTitle, archived } = e;
    return (
      <div className={`${archived ? 'archived' : ''}`}>
        <Link to={`/survey/by-form-id/${formId}`} className="text-black">
          {this.renderFormTitle(formTitle)}
          <small className="text-muted">{formId}</small>
        </Link>
      </div>
    );
  };

  render() {
    return (
      <div>
        <div className="btn-toolbar float-right">
          <CreateButton to="/survey/create">Create Survey</CreateButton>
        </div>

        <h2>Surveys</h2>
        <Spacer height={10} />
        {this.renderSurveys()}
        <Spacer height={20} />
        <BrowsableEntity
          title="Browse Surveys"
          entity={this.props.surveys.find}
          find={this.props.findSurveys}
          renderItem={this.renderEntity}
          size={40}
          allowArchived
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  surveys: state.surveys,
  cookie: state.cookie,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  findSurveys,
  findSurveysLastCreated,
  findSurveysLatestContributions,
  findSurveysOrganization,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SurveyList);

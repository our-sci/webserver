import React, { Component } from 'react';
import { connect } from 'react-redux';
import QueryString from 'query-string';

import SurveyTable from '../components/survey/SurveyTable';

class SurveySingle extends Component {
  render() {
    const { formId } = this.props.match.params;
    const query = QueryString.parse(this.props.location.search);

    return <SurveyTable formId={formId} sharecode={query.sharecode} showDateFilter showControls />;
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  surveyResults: state.surveyResults,
  surveys: state.surveys,
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SurveySingle);

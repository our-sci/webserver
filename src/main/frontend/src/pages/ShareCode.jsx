import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import moment from 'moment';
import _ from 'lodash';

import PagedEntites from '../components/PagedEntities';

import * as Utils from '../utils';

import { getSharecodeResults, getSharecodes, findDashboardsOrganization } from '../actions/api';
import { showFeedback } from '../actions/utils';

class ShareCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      surveys: {},
      organizations: {},
      dashboards: {},
      viewDetails: false,
      selectedOrganization: null,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    const { code } = this.props.match.params;
    this.props
      .getSharecodes(code)
      .then((response) => {
        this.extractSurveysAndOrganizations(response.data);
        this.extractDashboards(response.data);
      })
      .catch((error) => {
        this.props.showFeedback({ error, title: 'Nothing to see here...' });
      });
  };

  extractDashboards = (sharecodes) => {
    const dashboards = {};

    _.forEach(sharecodes, (sharecode) => {
      if (sharecode.dashboard) {
        const { dashboard } = sharecode;
        const did = dashboard.dashboardId;
        if (!dashboards[did]) {
          dashboards[did] = {
            total: 0,
            name: dashboard.name,
            id: dashboard.dashboardId,
          };
        }
        dashboards[did].total += 1;
      }
    });

    this.setState({ dashboards });
  };

  extractSurveysAndOrganizations = (sharecodes) => {
    const surveys = {};
    const organizations = {};

    _.map(sharecodes, (sharecode) => sharecode.surveyResult).forEach((result) => {
      // fill organizations
      if (result.organization) {
        const { organization } = result;
        const oid = organization.organizationId;
        if (!organizations[oid]) {
          organizations[oid] = {
            total: 0,
            latest: 0,
            name: organization.name,
            id: organization.organizationId,
          };
        }
        organizations[oid].total += 1;
        if (result.modified > organizations[oid].latest) {
          organizations[oid].latest = result.modified;
        }
      }

      // fill surveys
      const { survey } = result;
      const fid = survey.formId;
      if (!surveys[fid]) {
        surveys[fid] = { total: 0, latest: 0, name: survey.formTitle, id: survey.formId };
      }
      surveys[fid].total += 1;
      if (result.modified > surveys[fid].latest) {
        surveys[fid].latest = result.modified;
      }
    });

    this.setState({ organizations, surveys, selectedOrganization: Object.keys(organizations)[0] });
  };

  renderDashboards = () => {
    const { dashboards } = this.state;
    const { code } = this.props.match.params;

    if (_.isEmpty(dashboards)) {
      return null;
    }

    return (
      <div style={{ fontSize: '1.5rem' }}>
        <h3>View your results</h3>
        <ul className="list-group my-4">
          {_.map(dashboards, (dashboard) => {
            const { id, name, total } = dashboard;
            return (
              <li className="list-group-item" key={id}>
                <Link to={`/dashboard/by-dashboard-id/${id}?sharecode=${code}`}>{name}</Link>
                <br />
                <small className="text-muted">{total} total</small>
              </li>
            );
          })}
        </ul>
      </div>
    );
  };

  renderStats = () => {
    const { surveys, organizations, viewDetails } = this.state;
    if (_.isEmpty(surveys)) {
      return null;
    }
    let total = 0;
    _.each(surveys, (survey) => {
      total += survey.total;
    });

    if (!viewDetails) {
      return (
        <div className="mt-4">
          You made a total of {total} {Utils.singularOrPlural('contribution', total)}.<br />
          <a
            className="text-muted"
            href="/details/show/"
            onClick={(ev) => {
              ev.preventDefault();
              this.setState({ viewDetails: true });
            }}
          >
            View details...
          </a>
        </div>
      );
    }

    return (
      <div className="mt-4">
        You made a total of {total} {Utils.singularOrPlural('contribution', total)} to{' '}
        {_.size(surveys)} {Utils.singularOrPlural('survey', _.size(surveys))} and{' '}
        {_.size(organizations)} {Utils.singularOrPlural('organization', _.size(organizations))}.
        <br />
        <a
          className="text-muted"
          href="/details/hide/"
          onClick={(ev) => {
            ev.preventDefault();
            this.setState({ viewDetails: false });
          }}
        >
          Hide details...
        </a>
      </div>
    );
  };

  renderSurveys = () => {
    const { surveys } = this.state;
    const { code } = this.props.match.params;

    if (!surveys || _.isEmpty(surveys)) {
      return <div>No surveys here</div>;
    }

    return (
      <div className="mt-4">
        <h4>Surveys</h4>
        {_.map(surveys, (survey) => {
          const { id, name, total, latest } = survey;
          const fromNow = moment(latest).fromNow();
          return (
            <div className="mb-2" key={id}>
              <Link to={`/survey/by-form-id/${id}?sharecode=${code}`}>{name}</Link>
              <br />
              <small className="text-muted">
                {fromNow} ({total} total)
              </small>
            </div>
          );
        })}
      </div>
    );
  };

  renderOrganizations = () => {
    const { organizations } = this.state;
    if (_.isEmpty(organizations)) {
      return null;
    }

    return (
      <div className="mt-4">
        <h4>Organizations</h4>
        <div className="row">
          <div className="col-md-6">{this.renderOrganizationList()}</div>
          <div className="col-md-6">{this.renderOrganizationDashboards()}</div>
        </div>
      </div>
    );
  };

  renderOrganizationList = () => {
    const { organizations } = this.state;

    if (!organizations || _.isEmpty(organizations)) {
      return null;
    }

    return (
      <div className="mb-2">
        {_.map(organizations, (organization) => (
          <div key={organization.id}>
            <a
              href={`/view/dashboard/${organization.id}`}
              onClick={(ev) => {
                ev.preventDefault();
                this.setState({ selectedOrganization: organization.id });
              }}
            >
              {organization.name}
            </a>
            <br />
            <small className="text-muted">
              {moment(organization.latest).fromNow()} ({organization.total} total)
            </small>
          </div>
        ))}
      </div>
    );
  };

  renderOrganizationDashboards = () => {
    const { selectedOrganization, organizations } = this.state;
    if (!selectedOrganization) {
      return null;
    }

    const organization = organizations[selectedOrganization];
    if (!organization) {
      return null;
    }

    const title = (
      <span>
        <i className="far fa-bookmark mr-2" />
        <span>
          Dashboards <br />
          <small>
            <Link to={`/organization/${organization.id}`}>{organization.name}</Link>
          </small>
        </span>
      </span>
    );

    const query = { sharecode: this.props.match.params.code };

    return (
      <div>
        <PagedEntites
          title={title}
          entities={this.props.dashboards.organization}
          id={organization.id}
          recall={(size) => this.props.findDashboardsOrganization(organization.id, size)}
          renderItem={Utils.renderItemDashboard}
          query={query}
        />
      </div>
    );
  };

  render() {
    const { viewDetails } = this.state;

    return (
      <div>
        {this.renderDashboards()}
        {this.renderStats()}
        {viewDetails && this.renderSurveys()}
        {viewDetails && this.renderOrganizations()}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  tpcredential: state.tpcredential,
  dashboards: state.dashboards,
});

const mapDispatchToProps = {
  getSharecodeResults,
  getSharecodes,
  showFeedback,
  findDashboardsOrganization,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ShareCode)
);

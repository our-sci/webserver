import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import axios from 'axios';
import moment from 'moment';
import { Alert } from 'react-bootstrap';
import { Button } from 'reactstrap';

import _ from 'lodash';

import C from '../constants';
import {
  getOrganizationById,
  addOrganizationMember,
  removeOrganizationMember,
  updateOrganizationMember,
  addOrganizationFavorite,
  removeOrganizationFavorite,
  updateOrganizationFavorite,
} from '../actions/api';

const MODE = { add: 'ADD', edit: 'EDIT' };

class OrganizationFavoritesEdit extends Component {
  constructor(props) {
    super(props);
    this.renderOrganization = this.renderOrganization.bind(this);

    const { search } = this.props.location;
    const params = new URLSearchParams(search);
    const editId = parseInt(params.get('id'), 10);

    const { organizationId } = this.props.match.params;

    this.state = {
      id: 0,
      type: 'survey',
      typeIdentifier: '',
      description: '',
      priority: 0,
      error: null,
      mode: editId ? MODE.edit : MODE.add,
      editId,
      organizationId,
      options: [],
      isLoading: false,
      selected: [],
    };
  }

  componentDidMount() {
    const { organizationId } = this.props.match.params;
    this.props.getOrganizationById(organizationId);
  }

  componentWillReceiveProps(nextProps) {
    const { current } = nextProps.organizations;
    const { editId } = this.state;
    if (current.status === C.STATUS_SUCCESS) {
      _.map(current.data.favorites, (category) =>
        _.map(category, (favorite) => {
          if (favorite.id !== editId) {
            return;
          }

          const { id, type, typeIdentifier, description, priority } = favorite;
          this.setState({ id, type, typeIdentifier, description, priority });
        })
      );
    }
  }

  getLabelKey = (option) => {
    const DELIMITER = ';';

    switch (this.state.type) {
      case 'survey': {
        const label = option.formTitle;
        if (label.includes(DELIMITER)) {
          return label.substring(0, label.indexOf(DELIMITER));
        }
        return label;
      }
      case 'dashboard': {
        return option.name;
      }
      case 'script': {
        return option.name;
      }
      default:
        return option.label;
    }
  };

  getTypeIdentifier = (option) => {
    if (!option) return '';

    switch (this.state.type) {
      case 'survey':
        return option.formId;
      case 'dashboard':
        return option.dashboardId;
      case 'script':
        return option.scriptId;
      default:
        return option.id;
    }
  };

  getPlaceHolder = () => {
    switch (this.state.type) {
      case 'survey':
        return 'choose survey...';
      case 'dashboard':
        return 'choose dashboard...';
      case 'script':
        return 'choose script...';
      default:
        return 'choose...';
    }
  };

  handleFavoriteDelete = () => {
    const { editId, organizationId } = this.state;
    this.props
      .removeOrganizationFavorite(editId, organizationId)
      .then(() => this.navigateBack())
      .catch((error) => this.setState({ error }));
  };

  handleFavoriteAdd = (favorite) => {
    this.props
      .addOrganizationFavorite(favorite)
      .then(() => this.navigateBack())
      .catch((error) => this.setState({ error }));
  };

  handleFavoriteUpdate = (favorite) => {
    const { editId } = this.state;

    this.props
      .updateOrganizationFavorite({ id: editId, ...favorite })
      .then(() => this.navigateBack())
      .catch((error) => this.setState({ error }));
  };

  handleSearch = (query) => {
    this.setState({ isLoading: true });
    axios({
      url: `/api/${this.state.type}/find?search=${encodeURIComponent(query)}`,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      this.setState({ options: response.data.content, isLoading: false });
    });
  };

  navigateBack = () => {
    const { organizationId } = this.props.match.params;
    this.props.history.push(`/organization/${organizationId}/edit`);
  };

  renderMenuTitle = (option) => {
    const DELIMITER = ';';

    let title = '';
    let identifier = '';

    switch (this.state.type) {
      case 'survey':
        title = option.formTitle;
        identifier = option.formId;
        break;
      case 'dashboard':
        title = option.name;
        identifier = option.dashboardId;
        break;
      case 'script':
        title = option.name;
        identifier = option.scriptId;
        break;
      default:
        title = 'Subtitle';
        identifier = 'identifier';
    }

    if (title.includes(DELIMITER)) {
      const mainTitle = title.substring(0, title.indexOf(DELIMITER));
      const subTitle = title.substring(title.indexOf(DELIMITER) + 1);
      return (
        <div style={{ marginBottom: 5 }}>
          {mainTitle}
          <br />
          <span style={{ fontSize: '95%' }}>{subTitle}</span>
          <br />
          <small>{identifier}</small>
        </div>
      );
    }

    return (
      <div style={{ marginBottom: 5 }}>
        {title}
        <br />
        <small>{identifier}</small>
      </div>
    );
  };

  renderMenuItems = (option, props) => (
    <div key={option.id}>
      <span className="float-right">
        <small>{moment(option.date).format('YYYY-MM-DD, HH:mm')}</small>
      </span>
      {this.renderMenuTitle(option)}
    </div>
  );

  renderSubmitButton = () => {
    const { organizationId, type, typeIdentifier, description, priority } = this.state;
    const favorite = { organizationId, type, typeIdentifier, description, priority };

    if (this.state.mode === MODE.edit) {
      return (
        <button
          type="button"
          className="btn btn-primary"
          onClick={() => this.handleFavoriteUpdate(favorite)}
        >
          Update
        </button>
      );
    }

    return (
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => this.handleFavoriteAdd(favorite)}
      >
        Submit
      </button>
    );
  };

  renderDeleteButton = () => {
    if (this.state.mode === MODE.edit) {
      return (
        <Button color="danger" onClick={this.handleFavoriteDelete}>
          <i className="fas fa-trash-alt mr-2" />
          Delete
        </Button>
      );
    }

    return null;
  };

  renderTypeIdLabel = () => {
    switch (this.state.type) {
      case 'survey':
        return 'formId';
      case 'dashboard':
        return 'dashboardId';
      case 'script':
        return 'scriptId';
      default:
        return '';
    }
  };

  renderForm = () => {
    const { type, priority, description } = this.state;

    return (
      <form style={{ width: '600px' }}>
        <div className="btn-toolbar float-right">{this.renderDeleteButton()}</div>
        <h3>{this.state.mode === MODE.edit ? 'Edit favorite' : 'Add a new favorite'}</h3>
        <div className="form-group">
          <label htmlFor="type">Type</label>
          <select
            className="form-control"
            id="type"
            disabled={this.state.mode === MODE.edit}
            value={type}
            onChange={(ev) => {
              this.setState({
                type: ev.target.value,
                options: [],
                selected: [],
                typeIdentifier: '',
              });
            }}
          >
            <option value="survey">Survey</option>
            <option value="dashboard">Dashboard</option>
            <option value="script">Script</option>
          </select>
        </div>
        <div className="form-group">
          <label>{this.state.type[0].toUpperCase() + this.state.type.slice(1)}</label>
          <AsyncTypeahead
            onChange={(option) => {
              this.setState({
                selected: option,
                typeIdentifier: this.getTypeIdentifier(option[0]),
              });
            }}
            selected={this.state.selected}
            labelKey={this.getLabelKey}
            onSearch={this.handleSearch}
            isLoading={this.state.isLoading}
            placeholder={this.getPlaceHolder()}
            options={this.state.options}
            renderMenuItemChildren={this.renderMenuItems}
            minLength={1}
            disabled={this.state.mode === MODE.edit}
          />
          <small>{this.state.typeIdentifier}</small>
        </div>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <input
            className="form-control"
            id="description"
            value={description}
            onChange={(ev) => this.setState({ description: ev.target.value })}
          />
        </div>
        <div className="form-group">
          <label htmlFor="description">Priority</label>
          <input
            className="form-control"
            id="priority"
            value={priority}
            onChange={(ev) => this.setState({ priority: ev.target.value })}
          />
          <small>An optional integer value, higher values are listed earlier</small>
        </div>

        <div className="btn-toolbar float-right">
          <button type="button" className="btn btn-default mr-1" onClick={this.navigateBack}>
            Cancel
          </button>
          {this.renderSubmitButton()}
        </div>
      </form>
    );
  };

  renderError = () => {
    const { error } = this.state;
    if (error) {
      return (
        <Alert bsStyle="danger" onDismiss={() => this.setState({ error: null })}>
          <h4>Error {error.data.message.key}</h4>
          <p>{error.data.message.description}</p>
        </Alert>
      );
    }
    return null;
  };

  renderOrganization() {
    const { current } = this.props.organizations;

    if (current.status === C.STATUS_LOADING) {
      return <div>loading...</div>;
    }

    if (current.status === C.STATUS_ERROR) {
      return <div>Error: {current.data}</div>;
    }

    const organization = current.data;

    return (
      <div>
        {this.renderError()}
        <h4>{organization.name}</h4>
        {this.renderForm()}
      </div>
    );
  }

  render() {
    return this.renderOrganization();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  scripts: state.scripts,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  getOrganizationById,
  addOrganizationMember,
  removeOrganizationMember,
  updateOrganizationMember,
  addOrganizationFavorite,
  removeOrganizationFavorite,
  updateOrganizationFavorite,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(OrganizationFavoritesEdit)
);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import C from '../constants';
import * as Utils from '../utils';

import { hasValidToken, isLoggedIn } from '../actions/auth';
import {
  findSurveysOrganization,
  findDashboardsOrganization,
  findSurveysByUser,
  findSurveysLastCreated,
  findSurveysLatestContributions,
  getSurveyResultContributions,
} from '../actions/api';

import PagedEntities from '../components/PagedEntities';

import SurveyTable from '../components/survey/SurveyTable';

import LiveTracker from '../components/LiveTracker';

const WELCOME_MESSAGES = [
  (name) => `Hello ${name}`,
  (name) => `Welcome ${name}`,
  (name) => `Hola ${name}`,
  (name) => `Hallo ${name}`,
  (name) => `Bonjour ${name}`,
  (name) => `Hi ${name}`,
  (name) => `Hi again, ${name}`,
  (name) => `Happy to see you, ${name}!`,
  (name) => `Welcome back, ${name}`,
];

const WELCOME_MESSAGES_ANON = [
  'Hello',
  'Welcome',
  'Guten Tag',
  'Buongiorno!',
  'Hola',
  'Bonjour',
  'Grüezi',
];

class LandingPage extends Component {
  componentDidMount() {
    // this.props.findSurveysByUser('Yg8sw1u7fFRD6vVDGen6fNpjWe12');
  }

  renderOrganizationSurveys = () => {
    const selectedOrganization = this.props.cookie.organization.selected;

    if (!selectedOrganization) {
      return null;
    }

    const title = (
      <span>
        <i className="far fa-bookmark mr-2" />
        <span>
          Surveys <br />
          <small>
            <Link to={`/organization/${selectedOrganization.organizationId}`}>
              {selectedOrganization.name}
            </Link>
          </small>
        </span>
      </span>
    );

    return (
      <PagedEntities
        title={title}
        entities={this.props.surveys.organization}
        recall={(size) =>
          this.props.findSurveysOrganization(selectedOrganization.organizationId, size)
        }
        renderItem={Utils.renderItemSurvey}
      />
    );
  };

  renderOrganizationDashboards = () => {
    const selectedOrganization = this.props.cookie.organization.selected;

    if (!selectedOrganization) {
      return null;
    }

    const title = (
      <span>
        <i className="far fa-bookmark mr-2" />
        <span>
          Dashboards <br />
          <small>
            <Link to={`/organization/${selectedOrganization.organizationId}`}>
              {selectedOrganization.name}
            </Link>
          </small>
        </span>
      </span>
    );

    return (
      <PagedEntities
        title={title}
        entities={this.props.dashboards.organization}
        renderItem={Utils.renderItemDashboard}
        recall={(size) =>
          this.props.findDashboardsOrganization(selectedOrganization.organizationId, size)
        }
      />
    );
  };

  renderSurveysByUser = (username) => (
    <PagedEntities
      title="Your surveys"
      renderItem={Utils.renderItemSurvey}
      entities={this.props.surveys.user[username]}
      recall={(size) => this.props.findSurveysByUser(username, size)}
      showDate
    />
  );

  renderQuickTable = (lastContribution) => {
    if (!lastContribution) {
      return null;
    }

    return (
      <div className="row mt-4">
        <div className="col">
          <SurveyTable formId={lastContribution.survey.formId} checkable={false} size={10} />
        </div>
      </div>
    );
  };

  /*
  renderItemDashboard = (item) => {
    const { dashboardId, name } = item;
    return <Link to={`/dashboard/by-dashboard-id/${dashboardId}`}>{name}</Link>;
  };

  renderItemSurvey = (item) => {
    const { formId, formTitle } = item;
    return <Link to={`/survey/by-form-id/${formId}`}>{formTitle}</Link>;
  };

  renderItemContribution = (item) => {
    const { survey, total } = item;
    const contributions = (
      <small>
        {total} {`${total > 1 ? 'contributions' : 'contribution'}`}
      </small>
    );
    return (
      <div>
        <Link to={`/survey/by-form-id/${survey.formId}`}>{survey.formTitle}</Link>
        <br />
        {contributions}
      </div>
    );
  };
  */

  renderNotLoggedIn = () => (
    <div>
      <div className="clearfix">
        <h3 className="float-left">
          {WELCOME_MESSAGES_ANON[Math.floor(Math.random() * WELCOME_MESSAGES_ANON.length)]}
        </h3>
        <LiveTracker className="float-right" />
      </div>
      <p>
        <Link to="/login">Sign in</Link> to see your contributions, relevant surveys, organizations,
        etc.
      </p>
      <div className="row">
        <div className="col-md-6">{this.renderOrganizationDashboards()}</div>
        <div className="col-md-6">{this.renderOrganizationSurveys()}</div>
        <div className="col-md-6">
          <PagedEntities
            title="Recent contributions"
            renderItem={Utils.renderItemSurvey}
            getDate={(e) => e.latestContribution}
            entities={this.props.surveys.latestContributions}
            recall={(size) => this.props.findSurveysLatestContributions(size)}
            showDate
          />
        </div>
        <div className="col-md-6">
          <PagedEntities
            title="New surveys"
            renderItem={Utils.renderItemSurvey}
            entities={this.props.surveys.lastCreated}
            recall={(size) => this.props.findSurveysLastCreated(size)}
            showDate
          />
        </div>
      </div>
    </div>
  );

  render() {
    if (!this.props.isLoggedIn()) {
      return this.renderNotLoggedIn();
    }

    if (this.props.api.client.profile.status !== C.STATUS_SUCCESS) {
      return null;
    }

    const { displayName, username, lastContribution } = this.props.api.client.profile.data;

    const welcomeMessage = WELCOME_MESSAGES[Math.floor(Math.random() * WELCOME_MESSAGES.length)](
      displayName
    );

    return (
      <div>
        <div className="clearfix">
          <h3 className="float-left">{welcomeMessage}</h3>
          <LiveTracker className="float-right" />
        </div>
        <div className="row">
          <div className="col-md-6">{this.renderOrganizationDashboards()}</div>
          <div className="col-md-6">{this.renderOrganizationSurveys()}</div>
          <div className="col-md-6">
            <PagedEntities
              title="Your recent contributions"
              renderItem={Utils.renderItemContribution}
              entities={this.props.contributions[username]}
              getDate={(e) => e.latest}
              recall={(size) => this.props.getSurveyResultContributions({ uid: username, size })}
              showDate
            />
          </div>
          <div className="col-md-6">{this.renderSurveysByUser(username)}</div>
        </div>

        {this.renderQuickTable(lastContribution)}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  surveys: state.surveys,
  dashboards: state.dashboards,
  cookie: state.cookie,
  organizations: state.organizations,
  contributions: state.contributions,
});

const mapDispatchToProps = {
  hasValidToken,
  findSurveysOrganization,
  findDashboardsOrganization,
  findSurveysByUser,
  isLoggedIn,
  findSurveysLastCreated,
  findSurveysLatestContributions,
  getSurveyResultContributions,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LandingPage);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Script from 'react-load-script';

import { getScriptByScriptId, getScriptByDatabaseId } from '../actions/api';

class Macro extends Component {
  componentDidMount() {}

  render() {
    return (
      <div>
        <div id="script-container">Loading the script</div>
        <Script url="http://localhost/oursci/processor-browser-debug.js" />
      </div>
    );
  }

  /*
  render() {
    return (
      <div className="hello-macro">
        <div id="script-container">Loading the script</div>
      </div>
    );
  }
  */
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  scripts: state.scripts,
});

const mapDispatchToProps = {
  getScriptByScriptId,
  getScriptByDatabaseId,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Macro)
);

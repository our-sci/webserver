/* eslint-disable react/button-has-type */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import { Link, withRouter } from 'react-router-dom';

import {
  openAuthWithProvider,
  openAuthWithEmailAndPassword,
  registerWithEmailAndPassword,
  sendPasswordResetEmail,
  logoutUser,
  emailChanged,
  passwordChanged,
} from '../actions/auth';

import { claimInvitation } from '../actions/api';

import { invitationCodeChanged } from '../actions/invitation';

import C from '../constants';
import SocialButton from '../components/SocialButton';
import Spacer from '../components/Spacer';

class Auth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      haveInvitationCode: false,
    };
  }

  componentDidMount() {
    const { code } = this.props.match.params;
    if (code) {
      this.setState({ haveInvitationCode: true });
      this.props.invitationCodeChanged(code);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.auth.status !== nextProps.auth.status) {
      if (nextProps.auth.status === C.AUTH_LOGGED_IN) {
        this.props.history.push('/');
      }
    }

    if (this.props.invitation.code !== nextProps.invitation.code) {
      if (nextProps.invitation.code === '') {
        this.setState({ haveInvitationCode: false });
      }
    }
  }

  handleEmailChange = (ev) => {
    this.props.emailChanged(ev.target.value);
  };

  handlePasswordChange = (ev) => {
    this.props.passwordChanged(ev.target.value);
  };

  handleSubmit = (ev) => {
    console.log('handleSubmit...');
    ev.preventDefault();

    const { code } = this.props.invitation;
    if (code) {
      this.handleRegisterWithEmailAndPassword();
    } else {
      this.handleAuthWithEmailAndPassword();
    }
  };

  handleAuthWithProvider = (provider) => () => {
    this.props.openAuthWithProvider(provider);
  };

  handleAuthWithEmailAndPassword = () => {
    console.log('handleAuthWithEmailAndPassword...');
    const { email, password } = this.props.auth;
    this.props.openAuthWithEmailAndPassword(email, password);
  };

  handleRegisterWithEmailAndPassword = () => {
    console.log('handleRegisterWithEmailAndPassword...');
    const { email, password } = this.props.auth;
    this.props.registerWithEmailAndPassword(email, password);
  };

  clearInvitationCode = () => {
    this.setState({ haveInvitationCode: false });
    this.props.invitationCodeChanged('');
  };

  renderLoginLogout = () => {
    const { status, email, password } = this.props.auth;
    const { haveInvitationCode } = this.state;

    if (status === C.AUTH_LOGGED_IN) {
      return (
        <div>
          <button
            type="button"
            className="btn btn-outline-secondary float-right"
            onClick={this.props.logoutUser}
          >
            Log out
          </button>
          <h2>Hello</h2>

          <h5>
            You are currently logged in as <Link to="/profile">{this.props.auth.username}</Link>.
          </h5>
        </div>
      );
    }

    return (
      <div>
        {(() => {
          if (status === C.AUTH_AWAITING_RESPONSE) {
            return (
              <h2>
                Signing in... <span className="fa fa-spinner fa-spin" />
              </h2>
            );
          }
          return <h2>Sign in</h2>;
        })()}
        <div>
          <form>
            <div className="form-group">
              <label htmlFor="email">E-Mail:</label>
              <input
                className="form-control"
                id="email"
                value={this.props.auth.email}
                onChange={this.handleEmailChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password:</label>
              <input
                className="form-control"
                id="password"
                type="password"
                value={this.props.auth.password}
                onChange={this.handlePasswordChange}
              />
            </div>
            <div className="clearfix">
              <div className="btn-toolbar float-right">
                {email !== '' &&
                  password === '' && (
                    <button
                      type="button"
                      onClick={() => this.props.sendPasswordResetEmail(email)}
                      className="btn btn-outline-secondary mr-1"
                    >
                      Forgot password
                    </button>
                  )}
                <button
                  type={haveInvitationCode ? 'submit' : 'button'}
                  onClick={(ev) => {
                    ev.preventDefault();
                    this.handleRegisterWithEmailAndPassword();
                  }}
                  className="btn btn-info mr-1"
                >
                  Register
                </button>
                <button
                  type={haveInvitationCode ? 'button' : 'submit'}
                  className="btn btn-success"
                  onClick={(ev) => {
                    ev.preventDefault();
                    this.handleAuthWithEmailAndPassword();
                  }}
                >
                  Sign in
                </button>
              </div>
            </div>
          </form>
          <hr />

          <SocialButton
            provider="google"
            onClick={this.handleAuthWithProvider(new firebase.auth.GoogleAuthProvider())}
          >
            Sign in with Google
          </SocialButton>
        </div>
      </div>
    );
  };

  renderInvitationCode = () => {
    const { status } = this.props.auth;
    const { haveInvitationCode } = this.state;
    const { code } = this.props.invitation;

    if (!haveInvitationCode) {
      return (
        <div className="text-center">
          <a
            href="/#/login/invitation/"
            className="text-secondary"
            onClick={(ev) => {
              ev.preventDefault();
              this.setState({ haveInvitationCode: true });
            }}
          >
            I have an invitation code...
          </a>
        </div>
      );
    }

    return (
      <div>
        <label htmlFor="invitation-code">Invitation Code</label>

        <button
          type="button"
          className="close"
          aria-label="Close"
          onClick={this.clearInvitationCode}
        >
          <span aria-hidden="true">&times;</span>
        </button>

        <div className="input-group">
          <input
            className="form-control"
            id="invitation-code"
            value={this.props.invitation.code}
            onChange={(ev) => this.props.invitationCodeChanged(ev.target.value)}
            autoComplete="new-password"
          />
          {status === C.AUTH_LOGGED_IN &&
            code && (
              <div className="input-group-append">
                <button
                  className="btn btn-primary"
                  type="button"
                  id="button-apply"
                  onClick={() => this.props.claimInvitation()}
                >
                  Apply
                </button>
              </div>
            )}
        </div>
      </div>
    );
  };

  render() {
    return (
      <div style={{ maxWidth: '40rem' }}>
        {this.renderLoginLogout()}
        <Spacer height="3em" />
        {this.renderInvitationCode()}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({ auth: state.auth, invitation: state.invitation });

const mapDispatchToProps = {
  openAuthWithProvider,
  openAuthWithEmailAndPassword,
  registerWithEmailAndPassword,
  sendPasswordResetEmail,
  logoutUser,
  emailChanged,
  passwordChanged,
  invitationCodeChanged,
  claimInvitation,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Auth)
);

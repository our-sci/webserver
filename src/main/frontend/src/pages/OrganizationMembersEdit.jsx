import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import C from '../constants';
import {
  getOrganizationById,
  addOrganizationMember,
  removeOrganizationMember,
  updateOrganizationMember,
} from '../actions/api';
import Button from '../components/Button';

const MODE = { add: 'ADD', edit: 'EDIT' };

class OrganizationMembersEdit extends Component {
  constructor(props) {
    super(props);

    const { search } = this.props.location;
    const params = new URLSearchParams(search);
    const editId = parseInt(params.get('id'), 10);

    const { organizationId } = this.props.match.params;

    this.state = {
      email: '',
      description: '',
      role: 0,
      admin: false,
      error: null,
      mode: editId ? MODE.edit : MODE.add,
      editId,
      organizationId,
    };
  }

  componentDidMount() {
    const { organizationId } = this.props.match.params;
    this.props.getOrganizationById(organizationId);
  }

  componentWillReceiveProps(nextProps) {
    const { current } = nextProps.organizations;
    const { editId } = this.state;
    if (current.status === C.STATUS_SUCCESS) {
      _.map(current.data.members, (member) => {
        if (member.id !== editId) {
          return;
        }

        const { admin, description, role } = member;
        const { email } = member.user;
        this.setState({ email, admin, description, role });
      });
    }
  }

  navigateBack = () => {
    this.props.history.goBack();
  };

  deleteMember = () => {
    const { editId, organizationId } = this.state;
    this.props
      .removeOrganizationMember(editId, organizationId)
      .then(() => this.navigateBack())
      .catch((error) => this.setState({ error }));
  };

  handleMemberAdd = (organizationId, email, description, role, admin) => {
    this.props
      .addOrganizationMember(organizationId, email, description, role, admin)
      .then(() => this.navigateBack())
      .catch((error) => this.setState({ error }));
  };

  handleMemberUpdate = (memberId, description, role, admin) => {
    this.props
      .updateOrganizationMember(memberId, description, role, admin)
      .then(() => this.navigateBack())
      .catch((error) => this.setState({ error }));
  };

  renderSubmitButton = () => {
    const { organizationId, editId, email, description, role, admin } = this.state;

    if (this.state.mode === MODE.edit) {
      return (
        <button
          type="button"
          className="btn btn-primary"
          onClick={() => this.handleMemberUpdate(editId, description, role, admin)}
        >
          Update
        </button>
      );
    }

    return (
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => this.handleMemberAdd(organizationId, email, description, role, admin)}
      >
        Submit
      </button>
    );
  };

  renderDeleteButton = () => {
    if (this.state.mode === MODE.edit) {
      return (
        <Button icon="glyphicon glyphicon-trash" type="danger" onClick={this.deleteMember}>
          Delete
        </Button>
      );
    }

    return null;
  };

  renderForm = () => {
    const { email, description, role, admin } = this.state;
    const { current } = this.props.organizations;
    const organization = current.data;

    return (
      <form>
        <div className="btn-toolbar float-right">{this.renderDeleteButton()}</div>
        <h3>{this.state.mode === MODE.edit ? 'Edit member' : 'Add a new member'}</h3>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
            disabled={this.state.mode === MODE.edit}
            className="form-control"
            id="email"
            value={email}
            onChange={(ev) => this.setState({ email: ev.target.value })}
          />
        </div>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <input
            className="form-control"
            id="description"
            value={description || ' '}
            onChange={(ev) => this.setState({ description: ev.target.value })}
          />
        </div>
        <div className="form-group">
          <label htmlFor="role">Role</label>
          <input
            className="form-control"
            id="role"
            value={role || '0'}
            onChange={(ev) => this.setState({ role: ev.target.value })}
          />
          <small>An optional integer value, e.g. 0, 1 or 17 etc.</small>
        </div>
        <div className="form-group form-check">
          <input
            type="checkbox"
            className="form-check-input"
            id="exampleCheck1"
            onChange={(ev) => this.setState({ admin: ev.target.checked })}
            checked={admin}
          />{' '}
          <label className="form-check-label" htmlFor="exampleCheck1">
            {' '}
            Admin
          </label>
          <br />
          <small>Make this member an admin of {organization.name}</small>
        </div>
        <div className="btn-toolbar float-right">
          <button type="button" className="btn btn-default mr-1" onClick={this.navigateBack}>
            Cancel
          </button>
          {this.renderSubmitButton()}
        </div>
      </form>
    );
  };

  renderError = () => {
    const { error } = this.state;

    if (!error) {
      return null;
    }

    return (
      <div className="alert alert-warning">
        <button type="button" className="close ml-2" onClick={() => this.setState({ error: null })}>
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>{error.data.message.key}</strong> {error.data.message.description}
      </div>
    );
  };

  renderEdit = () => {
    const { current } = this.props.organizations;

    if (current.status === C.STATUS_LOADING) {
      return <div>loading...</div>;
    }

    if (current.status === C.STATUS_ERROR) {
      return <div>Error: {current.data}</div>;
    }

    const organization = current.data;

    return (
      <div className="row">
        <div className="col-12 col-md-6">
          {this.renderError()}

          <h4>{organization.name}</h4>
          {this.renderForm()}
        </div>
      </div>
    );
  };

  render() {
    return this.renderEdit();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  scripts: state.scripts,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  getOrganizationById,
  addOrganizationMember,
  removeOrganizationMember,
  updateOrganizationMember,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(OrganizationMembersEdit)
);

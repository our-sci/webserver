import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import C from '../constants';

import OrganizationChooser from '../components/organization/OrganizationChooser';
import ProfileWidget from '../components/ProfileWidget';
import Token from '../components/Token';
import Spacer from '../components/Spacer';
import BrowsableEntity from '../components/BrowsableEntity';

import { findTpCredentials, isAdmin } from '../actions/api';

class Profile extends Component {
  renderTpCredentialItem = (e) => {
    const { type, url, id } = e;
    return (
      <Link to={`/tpcredential/edit/${id}`}>
        <small>{type}</small>
        <br />
        <strong>{url}</strong>
      </Link>
    );
  };

  render() {
    if (this.props.auth.status !== C.AUTH_LOGGED_IN) {
      return (
        <div>
          <h3>Not much here</h3>
          You are not signed in.
        </div>
      );
    }

    const uid = this.props.match.params.uid || this.props.auth.uid;
    const self = uid === this.props.auth.uid;

    const title = (
      <span>
        Third party credentials
        <small className="float-right">
          <Link to={`/tpcredential/add/${uid}`}>Add</Link>
        </small>
      </span>
    );

    return (
      <div style={{ maxWidth: '60rem' }}>
        <ProfileWidget uid={uid} editable={self || this.props.isAdmin()} />
        <Spacer height={15} />
        {self && <Token />}
        {self && <OrganizationChooser />}

        {(self || this.props.isAdmin()) && (
          <BrowsableEntity
            id={uid}
            className="mt-5"
            title={title}
            entity={this.props.tpcredential.find}
            find={(params) => this.props.findTpCredentials({ uid, ...params })}
            renderItem={this.renderTpCredentialItem}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  tpcredential: state.tpcredential,
});

const mapDispatchToProps = {
  findTpCredentials,
  isAdmin,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Profile)
);

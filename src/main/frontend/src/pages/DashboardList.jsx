import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { findDashboards } from '../actions/api';
import CreateButton from '../components/CreateButton';

import BrowsableEntity from '../components/BrowsableEntity';

class DashboardList extends Component {
  renderEntity = (e) => {
    const { dashboardId, name, archived } = e;
    return (
      <div className={`${archived ? 'archived' : ''}`}>
        <Link to={`/dashboard/by-dashboard-id/${dashboardId}`} className="text-black">
          {name}
          <br />
          <small>{dashboardId}</small>
        </Link>
      </div>
    );
  };

  render() {
    return (
      <div>
        <div className="btn-toolbar float-right">
          <CreateButton to="/dashboard/create">Create Dashboard</CreateButton>
        </div>
        <BrowsableEntity
          title="Browse Dashboards"
          entity={this.props.dashboards.find}
          find={this.props.findDashboards}
          renderItem={this.renderEntity}
          allowArchived
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  dashboards: state.dashboards,
});

const mapDispatchToProps = {
  findDashboards,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardList);

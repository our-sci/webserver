import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { cookieChooseOrganization, cookieChooseOrganizationNone } from '../actions/cookie';

class OrganizationSelect extends Component {
  componentDidMount() {
    const { organizationId } = this.props.match.params;
    this.loadOrganization(organizationId);
  }

  componentDidUpdate() {
    const { organizationId } = this.props.match.params;
    this.loadOrganization(organizationId);
  }

  loadOrganization = (id) => {
    axios({
      url: `/api/organization/id/${id}`,
      method: 'get',
    })
      .then((response) => {
        const organization = response.data;
        this.props.cookieChooseOrganization(organization);
        this.goToHomepage();
      })
      .catch(() => {
        this.props.cookieChooseOrganizationNone();
      });
  };

  goToHomepage = () => {
    this.props.history.push('/');
  };

  render() {
    const { organizationId } = this.props.match.params;
    return <div>{`You like ${organizationId}`}</div>;
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  scripts: state.scripts,
});

const mapDispatchToProps = {
  cookieChooseOrganization,
  cookieChooseOrganizationNone,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrganizationSelect);

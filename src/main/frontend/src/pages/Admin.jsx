import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { findUsers, isAdmin, findTpCredentials } from '../actions/api';

import BrowsableEntity from '../components/BrowsableEntity';
import SurveyResultResubmission from '../components/admin/SurveyResultResubmission';

import Tab from '../components/Tab';
import Tabs from '../components/Tabs';

import { basename } from '../actions/utils';

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: 0 };
  }

  componentDidMount() {
    this.switchTab();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.url !== this.props.match.url) {
      this.switchTab();
    }
  }

  switchTab = () => {
    const tail = basename(this.props.match.url);

    let activeTab = 0;
    switch (tail) {
      case 'users':
        activeTab = 0;
        break;
      case 'tpcredentials':
        activeTab = 1;
        break;
      case 'resubmissions':
        activeTab = 2;
        break;
      default:
        activeTab = 0;
    }

    this.setState({ activeTab });
  };

  renderUserItem = (e) => {
    const { displayName, email, username } = e;

    return (
      <Link to={`/profile/${username}`} className="text-secondary">
        {`"${displayName || email}" <${email}>`}
      </Link>
    );
  };

  renderTpCredentialItem = (e) => {
    const { type, url, id, user } = e;
    return (
      <div>
        <Link className="text-secondary" to={`/tpcredential/edit/${id}`}>
          {type}: <strong>{url}</strong>
        </Link>
        <br />
        <Link to={`/profile/${user.username}`}>
          <small>{`${user.displayName || user.email} <${user.email}>`}</small>
        </Link>
      </div>
    );
  };

  render() {
    return (
      <div>
        <h2>Admin area</h2>
        <Tabs
          activeTab={this.state.activeTab}
          onSelect={(index) => this.setState({ activeTab: index })}
        >
          <Tab title="Users" link="/admin/users">
            <BrowsableEntity
              title="Browse users"
              entity={this.props.users.find}
              find={this.props.findUsers}
              renderItem={this.renderUserItem}
            />
          </Tab>

          <Tab title="Third Party" link="/admin/tpcredentials">
            <BrowsableEntity
              title="Browse third party credentials"
              entity={this.props.tpcredential.find}
              find={this.props.findTpCredentials}
              renderItem={this.renderTpCredentialItem}
            />
          </Tab>

          <Tab title="Resubmissions" link="/admin/resubmissions">
            <SurveyResultResubmission />
          </Tab>
        </Tabs>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  users: state.users,
  tpcredential: state.tpcredential,
});

const mapDispatchToProps = {
  isAdmin,
  findUsers,
  findTpCredentials,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Admin);

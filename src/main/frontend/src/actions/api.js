import React from 'react';

import axios from 'axios';
import * as Papa from 'papaparse';
import _ from 'lodash';

import C from '../constants';
import * as utils from './utils';
import { cookieChooseOrganization } from './cookie';

export const getScripts = () => (dispatch) => {
  dispatch({
    type: C.API_SCRIPT_LIST_LOADING,
  });

  axios({
    url: '/api/script/list',
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_SCRIPT_LIST_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_SCRIPT_LIST_ERROR,
        payload: error.message,
      })
    );
};

export const findScripts = ({ search = '', page = 0, size = 20, archived = false }) => (
  dispatch,
  getState
) => {
  axios({
    url: '/api/script/find',
    params: {
      search,
      page,
      size,
      archived,
    },
  })
    .then((response) => {
      dispatch({
        type: C.API_SCRIPT_FIND_SUCCESS,
        payload: response.data,
      });
    })
    .catch((error) => {
      dispatch({
        type: C.API_SCRIPT_FIND_ERROR,
        payload: error.response,
      });
      dispatch(utils.showError(error));
    });
};

export const getScriptByScriptId = (scriptId) => (dispatch) => {
  dispatch({
    type: C.API_SCRIPT_ID_LOADING,
  });

  axios({
    url: `/api/script/by-script-id/${scriptId}`,
    method: 'get',
  })
    .then((response) =>
      dispatch({
        type: C.API_SCRIPT_ID_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_SCRIPT_ID_ERROR,
        payload: error.message,
      })
    );
};

export const getScriptByDatabaseId = (databaseId) => (dispatch) => {
  dispatch({
    type: C.API_SCRIPT_ID_LOADING,
  });

  axios({
    url: `/api/script/by-database-id/${databaseId}`,
    method: 'get',
  })
    .then((response) =>
      dispatch({
        type: C.API_SCRIPT_ID_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_SCRIPT_ID_ERROR,
        payload: error.message,
      })
    );
};

export const getDashboards = () => (dispatch) => {
  dispatch({
    type: C.API_DASHBOARD_LIST_LOADING,
  });

  axios({
    url: '/api/dashboard/list',
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_DASHBOARD_LIST_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_DASHBOARD_LIST_ERROR,
        payload: error.message,
      })
    );
};

export const findDashboards = ({ search = '', page = 0, size = 20, archived = false }) => (
  dispatch,
  getState
) => {
  axios({
    url: '/api/dashboard/find',
    params: {
      search,
      page,
      size,
      archived,
    },
  })
    .then((response) => {
      dispatch({
        type: C.API_DASHBOARD_FIND_SUCCESS,
        payload: response.data,
      });
    })
    .catch((error) => {
      dispatch({
        type: C.API_DASHBOARD_FIND_ERROR,
        payload: error.response,
      });
      dispatch(utils.showError(error));
    });
};

export const getDashboardByDashboardId = (dashboardId) => (dispatch) => {
  dispatch({
    type: C.API_DASHBOARD_ID_LOADING,
  });

  axios({
    url: `/api/dashboard/by-dashboard-id/${dashboardId}`,
    method: 'get',
  })
    .then((response) =>
      dispatch({
        type: C.API_DASHBOARD_ID_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_DASHBOARD_ID_ERROR,
        payload: error.message,
      })
    );
};

export const getDashboardByDatabaseId = (databaseId) => (dispatch) => {
  dispatch({
    type: C.API_DASHBOARD_ID_LOADING,
  });

  axios({
    url: `/api/dashboard/by-database-id/${databaseId}`,
    method: 'get',
  })
    .then((response) =>
      dispatch({
        type: C.API_DASHBOARD_ID_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_DASHBOARD_ID_ERROR,
        payload: error.message,
      })
    );
};

export const getOrganizations = () => (dispatch) => {
  dispatch({
    type: C.API_ORGANIZATION_LIST_LOADING,
  });

  axios({
    url: '/api/organization/list',
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_ORGANIZATION_LIST_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_ORGANIZATION_LIST_ERROR,
        payload: error.message,
      })
    );
};

export const getOrganizationById = (organizationId) => (dispatch) => {
  dispatch({
    type: C.API_ORGANIZATION_ID_LOADING,
  });

  return axios({
    url: `/api/organization/id/${organizationId}`,
    method: 'get',
  })
    .then((response) => {
      dispatch({
        type: C.API_ORGANIZATION_ID_SUCCESS,
        payload: response.data,
      });
      return response.data;
    })
    .catch((error) => {
      dispatch({
        type: C.API_ORGANIZATION_ID_ERROR,
        payload: error.message,
      });
      Promise.reject();
    });
};

export const getOrganizationAssets = (organizationId) => (dispatch) => {
  dispatch({
    type: C.API_ORGANIZATION_ASSETS_LOADING,
  });

  axios({
    url: `/api/organization/id/${organizationId}/assets`,
    method: 'get',
  })
    .then((response) =>
      dispatch({
        type: C.API_ORGANIZATION_ASSETS_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_ORGANIZATION_ASSETS_ERROR,
        payload: error.message,
      })
    );
};

export const deleteOrganizationAsset = (organizationId, name) => (dispatch, getState) => {
  dispatch({
    type: C.API_ORGANIZATION_ASSETS_LOADING,
  });

  const state = getState();
  const { auth } = state;

  const headers = utils.createGetHeaders(auth);

  axios({
    url: `/api/organization/id/${organizationId}/assets/remove`,
    method: 'get',
    headers,
    params: {
      name,
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_ORGANIZATION_ASSETS_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_ORGANIZATION_ASSETS_ERROR,
        payload: error.message,
      })
    );
};

export const addOrganizationMember = (organizationId, email, description, role, admin) => (
  dispatch,
  getState
) => {
  const state = getState();
  const { token } = state.auth;

  return axios({
    url: '/api/organization/addMember',
    method: 'post',
    params: {
      id: organizationId,
      email,
      description,
      role,
      admin,
    },
    headers: {
      'Content-Type': 'application/json',
      'X-Authorization-Firebase': token,
    },
  })
    .then((response) =>
      // dispatch(getOrganizationById(organizationId));
      Promise.resolve()
    )
    .catch((error) => Promise.reject(error.response));
};

export const updateOrganizationMember = (memberId, description, role, admin) => (
  dispatch,
  getState
) => {
  const state = getState();
  const { token } = state.auth;

  return axios({
    url: '/api/organization/updateMember',
    method: 'post',
    params: {
      id: memberId,
      description,
      role,
      admin,
    },
    headers: {
      'Content-Type': 'application/json',
      'X-Authorization-Firebase': token,
    },
  })
    .then((response) =>
      // dispatch(getOrganizationById(organizationId));
      Promise.resolve()
    )
    .catch((error) => Promise.reject(error.response));
};

export const removeOrganizationMember = (id, organizationId) => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  return axios({
    url: '/api/organization/removeMember',
    method: 'post',
    params: {
      id,
      organizationId,
    },
    headers: {
      'Content-Type': 'application/json',
      'X-Authorization-Firebase': token,
    },
  })
    .then((response) => Promise.resolve())
    .catch((error) => Promise.reject(error.response));
};

export const addOrganizationFavorite = ({
  organizationId,
  type,
  typeIdentifier,
  description,
  priority,
}) => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  return axios({
    url: '/api/favorite/organization',
    method: 'post',
    params: {
      organizationId,
      type,
      typeIdentifier,
      description,
      priority,
    },
    headers: {
      'Content-Type': 'application/json',
      'X-Authorization-Firebase': token,
    },
  })
    .then((response) =>
      // dispatch(getOrganizationById(organizationId));
      Promise.resolve()
    )
    .catch((error) => Promise.reject(error.response));
};

export const updateOrganizationFavorite = ({ id, description, priority }) => (
  dispatch,
  getState
) => {
  const state = getState();
  const { token } = state.auth;

  return axios({
    url: '/api/favorite/organization/update',
    method: 'post',
    params: {
      id,
      description,
      priority,
    },
    headers: {
      'Content-Type': 'application/json',
      'X-Authorization-Firebase': token,
    },
  })
    .then((response) =>
      // dispatch(getOrganizationById(organizationId));
      Promise.resolve()
    )
    .catch((error) => Promise.reject(error.response));
};

export const removeOrganizationFavorite = (id, organizationId) => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  return axios({
    url: '/api/favorite/organization/remove',
    method: 'post',
    params: {
      id,
      organizationId,
    },
    headers: {
      'Content-Type': 'application/json',
      'X-Authorization-Firebase': token,
    },
  })
    .then((response) => Promise.resolve())
    .catch((error) => Promise.reject(error.response));
};

export const getProfile = () => (dispatch, getState) => {
  axios({
    url: '/api/profile/self',
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
      'X-Authorization-Firebase': getState().auth.token,
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_CLIENT_PROFILE_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_CLIENT_PROFILE_ERROR,
        payload: error.message,
      })
    );
};

export const getSurveyById = (formId) => (dispatch) => {
  // dispatch({ type: C.API_SURVEY_ID_LOADING });
  axios({
    url: `/api/survey/by-form-id/${formId}`,
    method: 'get',
  })
    .then((response) =>
      dispatch({
        type: C.API_SURVEY_ID_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_SURVEY_ID_ERROR,
        payload: error.message,
      })
    );
};

export const getSurveyResultCsvById = ({ formId, from, to, size, user, sharecode }) => (
  dispatch,
  getState
) => {
  dispatch({
    type: C.API_SURVEY_RESULT_CSV_LOADING,
    id: formId,
  });

  const { auth } = getState();
  let headers = {};

  if (utils.isTokenValid(auth.token)) {
    headers = {
      'Content-Type': 'application/json',
      'X-Authorization-Firebase': auth.token,
    };
  }

  const baseURL = `/api/survey/result/csv/by-form-id/${formId}`;
  const query = utils.createQueryString({ from, to, size, user, sharecode });

  const url = `${baseURL}${query !== '' ? `?${query}` : ''}`;
  console.log(url);

  axios({
    url,
    method: 'get',
    headers,
  })
    .then((response) => {
      const csv = Papa.parse(response.data, { dynamicTyping: true }).data;

      const data = _.drop(csv, 1).map((row) => {
        const _id = row[2]; // instance id
        return { _id, ..._.zipObject(csv[0], row) };
      });

      const header = csv[0];

      const columns = _.map(csv[0], (v, i) => ({
        Header: v,
        accessor: (d) => d[v],
        minWidth: v.endsWith('meta.filename') ? 400 : 230,
        id: csv[0][i],
        Cell: (props) => {
          const ret = props.value;
          if (v.endsWith('meta.filename')) {
            if (ret === undefined || ret === null || ret === '') {
              return null;
            }

            const linkname = ret.split(/[\\/]/).pop();

            const scriptId = csv[props.index + 1][i + 1];
            const filename = linkname;
            const instanceId = csv[props.index + 1][2]; // TODO: dont use this dirty hack (metaInstanceID is at position 2)
            const processorLink = `/processor/${scriptId}?formId=${formId}&instanceId=${instanceId}&filename=${filename}`;

            return (
              <span>
                <a href={processorLink} target="_blank" rel="noopener noreferrer">
                  {decodeURIComponent(linkname)}
                </a>{' '}
                <a href={ret} target="_blank" rel="noopener noreferrer">
                  <i className="fas fa-download" />
                </a>
              </span>
            );
          }
          if (v.endsWith('meta.error')) {
            if (ret === true) {
              return 'true';
            }
          }
          return ret;
        },
      }));

      dispatch({
        type: C.API_SURVEY_RESULT_CSV_SUCCESS,
        payload: { csv, table: { data, columns, header } },
        id: formId,
      });
    })
    .catch((error) =>
      dispatch({
        type: C.API_SURVEY_RESULT_CSV_ERROR,
        payload: error.message,
        id: formId,
      })
    );
};

export const getSurveyResult = (instanceId) => (dispatch) => axios.get(`/api/survey/result/${instanceId}`)

export const findSurveys = ({ search = '', page = 0, size = 20, archived = false }) => (
  dispatch,
  getState
) => {
  axios({
    url: '/api/survey/find',
    params: {
      search,
      page,
      size,
      archived,
    },
  })
    .then((response) => {
      dispatch({
        type: C.API_SURVEY_FIND_SUCCESS,
        payload: response.data,
      });
    })
    .catch((error) => {
      dispatch({
        type: C.API_SURVEY_FIND_ERROR,
        payload: error.response,
      });
      dispatch(utils.showError(error));
    });
};

export const findSurveysByUser = (user, size = 20, search = '', page = 0, archived = false) => (
  dispatch
) => {
  axios({
    url: `/api/survey/find?user=${user}&search=${encodeURIComponent(
      search
    )}&page=${page}&size=${size}&sort=date,desc&archived=${archived}`,
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_SURVEY_USER_SUCCESS,
        payload: response.data,
        id: user,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_SURVEY_USER_ERROR,
        payload: error.message,
        id: user,
      })
    );
};

export const findSurveysLastCreated = (size) => (dispatch) => {
  axios({
    url: `/api/survey/find?size=${size}&sort=date,desc`,
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_SURVEY_LASTCREATED_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_SURVEY_LASTCREATED_ERROR,
        payload: error.message,
      })
    );
};

export const findSurveysLatestContributions = (size) => (dispatch) => {
  axios({
    url: `/api/survey/find?size=${size}&sort=latestContribution,desc`,
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_SURVEY_LATESTCONTRIBUTIONS_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_SURVEY_LATESTCONTRIBUTIONS_ERROR,
        payload: error.message,
      })
    );
};

export const findSurveysArchived = (search, page) => (dispatch) => {
  axios({
    url: `/api/survey/find?search=${encodeURIComponent(
      search
    )}&page=${page}&sort=formTitle,asc&archived=true`,
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_SURVEY_ARCHIVED_SUCCESS,
        payload: response.data,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_SURVEY_ARCHIVED_ERROR,
        payload: error.message,
      })
    );
};

export const findOrganizations = ({ search = '', page = 0, size = 20, archived = false }) => (
  dispatch,
  getState
) => {
  axios({
    url: '/api/organization/find',
    params: {
      search,
      page,
      size,
      archived,
    },
  })
    .then((response) => {
      dispatch({
        type: C.API_ORGANIZATION_FIND_SUCCESS,
        payload: response.data,
      });
    })
    .catch((error) => {
      dispatch({
        type: C.API_ORGANIZATION_FIND_ERROR,
        payload: error.response,
      });
      dispatch(utils.showError(error));
    });
};

export const findMyOrganizations = () => (dispatch, getState) => {
  const state = getState();
  const { auth, cookie } = state;
  if (auth.status !== C.AUTH_LOGGED_IN) {
    return;
  }

  const { uid } = auth;

  axios({
    url: `/api/organization/list?username=${uid}`,
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) => {
      dispatch({
        type: C.API_ORGANIZATION_MYORGANIZATIONS_SUCCESS,
        payload: response.data,
      });
      if (cookie.organization && cookie.organization.selected) {
        const cookieOrganization = cookie.organization.selected;
        const updatedOrganization = _.find(response.data, { id: cookieOrganization.id });
        if (updatedOrganization) {
          dispatch({ type: C.COOKIE_CHOOSE_ORGANIZATION, payload: updatedOrganization });
        }
      }
      Promise.resolve();
    })
    .catch((error) => {
      dispatch({
        type: C.API_ORGANIZATION_MYORGANIZATIONS_ERROR,
        payload: error.message,
      });
      Promise.reject(error.response);
    });
};

export const findSurveysOrganization = (organization, size = 5) => (dispatch) => {
  axios({
    url: `/api/survey/find?size=${size}&organization=${organization}`,
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_SURVEY_ORGANIZATION_SUCCESS,
        payload: response.data,
        organizationId: organization,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_SURVEY_ORGANIZATION_ERROR,
        payload: error.message,
        organizationId: organization,
      })
    );
};

export const findDashboardsOrganization = (organization, size = 5) => (dispatch) => {
  axios({
    url: `/api/dashboard/find?size=${size}&organization=${organization}`,
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) =>
      dispatch({
        type: C.API_DASHBOARD_ORGANIZATION_SUCCESS,
        payload: response.data,
        organizationId: organization,
      })
    )
    .catch((error) =>
      dispatch({
        type: C.API_DASHBOARD_ORGANIZATION_ERROR,
        payload: error.message,
        organizationId: organization,
      })
    );
};

export const postOrganizationPreferences = ({ organizationId, preferences, clear }) => (
  dispatch,
  getState
) => {
  const state = getState();
  const { token } = state.auth;

  return axios({
    url: `/api/organization/id/${organizationId}/preferences`,
    method: 'post',
    data: preferences,
    params: {
      clear: clear || false,
    },
    headers: {
      'Content-Type': 'application/json',
      'X-Authorization-Firebase': token,
    },
  })
    .then((response) =>
      // dispatch(getOrganizationById(organizationId));
      Promise.resolve()
    )
    .catch((error) => Promise.reject(error.response));
};

export const archiveSurveyResult = (selection) => (dispatch, getState) => {
  const state = getState();
  const { auth } = state;

  const headers = utils.createPostHeaders(auth, 'application/json');

  return axios({
    url: `/api/survey/result/archive`,
    method: 'post',
    headers,
    data: selection,
  });
};

export const archiveOrganization = (organizationId, archived = true) => (dispatch, getState) => {
  const state = getState();
  const { auth } = state;

  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
      'X-Authorization-Firebase': auth.token,
    },
  };

  const formData = new FormData();
  if (organizationId !== undefined) formData.append('id', organizationId);
  if (archived !== undefined) formData.append('archived', archived);

  return axios.post('/api/organization/update', formData, config);
};

export const getSurveyResultContributions2 = ({ uid, size }) => (dispatch) =>
  axios({
    url: `/api/survey/result/contribution/user/${uid}?size=${size || 5}`,
  });

export const getSurveyResultContributions = ({ uid, size }) => (dispatch) => {
  axios({
    url: `/api/survey/result/contribution/user/${uid}?size=${size || 5}`,
  })
    .then((response) =>
      dispatch({
        type: C.API_CONTRIBUTION_USER_SUCCESS,
        payload: response.data,
        uid,
      })
    )
    .catch((error) => dispatch(utils.showError(error)));
};

export const updateProfile = ({ uid, displayName, picture, clearPicture }) => (
  dispatch,
  getState
) => {
  const state = getState();
  const { token } = state.auth;

  const formData = new FormData();
  formData.append('uid', uid);
  formData.append('displayName', displayName);
  formData.append('picture', picture);
  formData.append('clearPicture', clearPicture);

  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
      'X-Authorization-Firebase': token,
    },
  };

  return axios
    .post('/api/profile/', formData, config)
    .catch((error) => dispatch(utils.showError(error)));
};

export const createInvitation = (organizationId, emails) => (dispatch, getState) => {
  axios({
    url: `/api/invitation/create`,
    method: 'get',
    headers: utils.createGetHeaders(getState().auth),
    params: {
      organizationId,
      emails,
    },
  })
    .then(() => dispatch(getOrganizationById(organizationId)))
    .catch((error) => dispatch(utils.showError(error)));
};

export const deleteInvitation = (organizationId, id) => (dispatch, getState) =>
  axios({
    url: `/api/invitation/delete`,
    method: 'get',
    headers: utils.createGetHeaders(getState().auth),
    params: {
      id,
    },
  })
    .then(() => dispatch(getOrganizationById(organizationId)))
    .catch((error) => dispatch(utils.showError(error)));

export const testInvitation = (code) => (dispatch, getState) =>
  axios({
    url: '/api/invitation/test',
    method: 'get',
    params: {
      code,
    },
  });

export const claimInvitation = () => (dispatch, getState) => {
  const state = getState();
  const { auth } = state;
  const { code } = state.invitation;

  if (!code) {
    return;
  }

  const headers = utils.createGetHeaders(auth);

  axios({
    url: '/api/invitation/claim',
    method: 'get',
    headers,
    params: {
      code,
    },
  })
    .then((response) => {
      dispatch({
        type: C.FEEDBACK_SHOW,
        payload: {
          title: `${response.data.organization.name} likes you`,
          message: `And you like them, too! You successfully joined their organization :-)`,
          type: C.FEEDBACK_TYPE_INFO,
        },
      });
      dispatch(cookieChooseOrganization(response.data.organization));
    })
    .catch((error) => {
      dispatch({
        type: C.FEEDBACK_SHOW,
        payload: {
          title: 'Error',
          message: error.response.data.message.description,
          type: C.FEEDBACK_TYPE_DANGER,
        },
      });
    })
    .finally(() => {
      dispatch({
        type: C.INVITATION_CODE_CHANGED,
        payload: '',
      });
    });
};

export const showInvitationCode = (id) => (dispatch, getState) =>
  axios({
    url: '/api/invitation/showcode',
    method: 'get',
    headers: utils.createGetHeaders(getState().auth),
    params: {
      id,
    },
  }).catch((error) => {
    dispatch({
      type: C.FEEDBACK_SHOW,
      payload: {
        title: 'Error',
        message: error.response.data.message.description,
        type: C.FEEDBACK_TYPE_DANGER,
      },
    });
  });

export const resendInvitation = (id, subject, body) => (dispatch, getState) =>
  axios({
    url: '/api/invitation/resend',
    method: 'get',
    headers: utils.createGetHeaders(getState().auth),
    params: {
      id,
      subject,
      body,
    },
  }).catch((error) => {
    dispatch({
      type: C.FEEDBACK_SHOW,
      payload: {
        title: 'Error',
        message: error.response.data.message.description,
        type: C.FEEDBACK_TYPE_DANGER,
      },
    });
  });

export const getTpCredential = (id) => (dispatch, getState) =>
  axios({
    url: '/api/tpcredential',
    headers: utils.createGetHeaders(getState().auth),
    params: {
      id,
    },
  }).catch((error) => dispatch(utils.showError(error)));

export const findTpCredentials = ({ search = '', uid = null, page = 0, size = 20 }) => (
  dispatch,
  getState
) => {
  axios({
    url: '/api/tpcredential/find',
    headers: utils.createGetHeaders(getState().auth),
    params: {
      search,
      uid,
      page,
      size,
    },
  })
    .then((response) => {
      dispatch({
        type: C.API_TPCREDENTIAL_FIND_SUCCESS,
        payload: response.data,
      });
    })
    .catch((error) => {
      dispatch({
        type: C.API_TPCREDENTIAL_FIND_ERROR,
        payload: error.response,
      });
      dispatch(utils.showError(error));
    });
};

export const postTpCredential = (params) => (dispatch, getState) =>
  axios({
    url: '/api/tpcredential',
    method: 'post',
    data: params,
    headers: utils.createPostJsonHeaders(getState().auth),
  });

export const deleteTpCredential = (params) => (dispatch, getState) =>
  axios({
    url: '/api/tpcredential',
    method: 'delete',
    data: params,
    headers: utils.createPostJsonHeaders(getState().auth),
  });

export const findUsers = ({ search = '', page = 0, size = 20 }) => (dispatch, getState) => {
  axios({
    url: '/api/user/find',
    headers: utils.createGetHeaders(getState().auth),
    params: {
      search,
      page,
      size,
    },
  })
    .then((response) => {
      dispatch({
        type: C.API_USERS_FIND_SUCCESS,
        payload: response.data,
      });
    })
    .catch((error) => {
      dispatch({
        type: C.API_USERS_FIND_ERROR,
        payload: error.response,
      });
      dispatch(utils.showError(error));
    });
};

export const getSharecodeResults = (code) => (dispatch, getState) =>
  axios({
    url: '/api/sharecode/results',
    params: {
      code,
    },
  });

export const getSharecodes = (code) => (dispatch, getState) =>
  axios({
    url: '/api/sharecode',
    params: {
      code,
    },
  });

export const sendSharecodeEmail = (email) => (dispatch, getState) =>
  axios({
    url: '/api/sharecode/email',
    method: 'post',
    params: {
      email,
    },
  });

export const createSharecode = ({ email, instanceIds, dashboardId = '', sendmail }) => (
  dispatch,
  getState
) =>
  axios({
    url: '/api/sharecode/',
    method: 'post',
    params: {
      email,
      instanceIds: _.join(_.map(instanceIds, (e) => e), ','),
      dashboardId,
      sendmail,
    },
  });

export const isAdmin = () => (dispatch, getState) => {
  const state = getState();
  const { profile } = state.api.client;
  if (profile.status !== C.STATUS_SUCCESS) {
    return false;
  }

  if (profile.data.roles.includes('ROLE_ADMIN')) {
    return true;
  }

  return false;
};

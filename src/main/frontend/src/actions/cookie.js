import C from '../constants';

export const cookieChooseOrganization = (organization) => (dispatch) => {
  dispatch({ type: C.COOKIE_CHOOSE_ORGANIZATION, payload: organization });
};

export const cookieChooseOrganizationNone = () => (dispatch) => {
  dispatch({ type: C.COOKIE_CHOOSE_ORGANIZATION_NONE });
};

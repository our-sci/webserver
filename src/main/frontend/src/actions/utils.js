import _ from 'lodash';

import C from '../constants';

export function foo() {
  return false;
}

export function createGetHeaders(auth) {
  const { token } = auth;

  const headers = {
    'Content-Type': 'application/json',
    'X-Authorization-Firebase': token,
  };

  return headers;
}

export function createPostHeaders(auth, contentType = false) {
  const { token } = auth;

  const headers = {
    'Content-Type': contentType || 'multipart/form-data',
    'X-Authorization-Firebase': token,
  };

  return headers;
}

export function createPostJsonHeaders(auth) {
  const { token } = auth;

  const headers = {
    'Content-Type': 'application/json',
    'X-Authorization-Firebase': token,
  };

  return headers;
}

export function basename(url) {
  if (url === null) {
    return null;
  }
  return url.split(/[\\/]/).pop();
}

export function createQueryString(params) {
  return _.join(_.map(_.pickBy(params, (v) => v), (v, k) => `${k}=${v}`), '&');
}

export const isTokenValid = (token) => {
  if (token) {
    // following line from official firebase fiddle http://jsfiddle.net/firebase/XDXu5/
    const data = JSON.parse(decodeURIComponent(escape(window.atob(token.split('.')[1]))));

    const currentMillis = new Date().getTime();
    const tokenExpMillis = data.exp * 1000;

    if (currentMillis < tokenExpMillis) return true;
  }

  return false;
};

export const showError = (error) => (dispatch) => {
  let code = ''; // eslint-disable-line
  let title = 'Error';
  let body = "I'm sorry, but I don't know exactly what happened :/";
  try {
    const { message } = error.response.data;
    code = message.status;
    title = message.key;
    body = message.description;
  } catch (e) {
    // that's ok
  }

  dispatch({
    type: C.FEEDBACK_SHOW,
    payload: {
      title,
      message: body,
      type: C.FEEDBACK_TYPE_DANGER,
    },
  });
};

export const showFeedback = ({ error, title, body, type }) => (dispatch) => {
  let mCode = ''; // eslint-disable-line
  let mTitle = 'Error';
  let mBody = "I'm sorry, but I don't know exactly what happened :/";
  let mType = C.FEEDBACK_TYPE_INFO;
  try {
    const { message } = error.response.data;
    console.log(message);
    mCode = message.status;
    mTitle = message.key;
    mBody = message.description;
  } catch (e) {
    // that's ok
  }

  if (title) {
    mTitle = title;
  }

  if (body) {
    mBody = body;
  }

  if (type) {
    mType = type;
  }

  dispatch({
    type: C.FEEDBACK_SHOW,
    payload: {
      title: mTitle,
      message: mBody,
      type: mType,
    },
  });
};

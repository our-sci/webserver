import C from '../constants';

export const invitationCodeChanged = (text) => ({
  type: C.INVITATION_CODE_CHANGED,
  payload: text,
});

import axios from 'axios';

import C from '../constants';
import { auth } from '../firebaseApp';
import { getProfile, claimInvitation } from './api';

const registerUserOnServer = (idToken) => (dispatch, getState) => {
  axios({
    url: '/api/signup',
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      firebaseToken: idToken,
    },
  })
    .then((response) => {
      dispatch({
        type: C.API_REGISTER_SUCCESS,
        payload: response.data,
      });
      dispatch(getProfile());
      dispatch(claimInvitation());
    })
    .catch((error) => {
      dispatch({
        type: C.API_REGISTER_ERROR,
        payload: error.message,
      });
      dispatch(getProfile());
    });
};

export const listenToTokenChange = () => (dispatch) => {
  auth.onIdTokenChanged((user) => {
    if (user) {
      // the id token changed
      user.getIdToken().then((token) => {
        dispatch({
          type: C.AUTH_TOKEN_CHANGED,
          payload: token,
        });
        dispatch(getProfile());
      });
    }
  });
};

export const listenToAuthChange = () => (dispatch) => {
  auth.onAuthStateChanged((user) => {
    console.log('onAuthStateChanged...');
    if (user) {
      console.log('a user signed in...');
      // a user signed in
      dispatch({
        type: C.AUTH_LOGIN,
        uid: user.uid,
        username: user.displayName || user.email,
      });
    } else {
      console.log('a user logged out.');
      dispatch({ type: C.AUTH_LOGOUT });
    }
  });
};

export const openAuthWithProvider = (provider) => (dispatch) => {
  dispatch({ type: C.AUTH_OPEN });

  auth
    .signInWithPopup(provider)
    .then((userCredential) => {
      const { user } = userCredential;
      if (user) {
        user.getIdToken().then((idToken) => {
          // should register here
          console.log('idToken from signInWithPopup:');
          console.log(idToken);
          dispatch(registerUserOnServer(idToken));
        });
      }
    })
    .catch((error) => {
      dispatch({
        type: C.FEEDBACK_SHOW,
        payload: { title: 'Login failed', message: error.message, type: C.FEEDBACK_TYPE_DANGER },
      });
      dispatch({ type: C.AUTH_LOGOUT });
    });
};

export const openAuthWithEmailAndPassword = (email, password) => (dispatch) => {
  dispatch({ type: C.AUTH_OPEN });

  auth
    .signInWithEmailAndPassword(email, password)
    .then((user) => {
      console.log('signInWithEmailAndPassword...');
      if (user) {
        user.getIdToken().then((idToken) => {
          // should register here
          console.log('idToken from signInWithEmailAndPassword:');
          console.log(idToken);
          dispatch(registerUserOnServer(idToken));
        });
      }
    })
    .catch((error) => {
      dispatch({
        type: C.FEEDBACK_SHOW,
        payload: { title: 'Login failed', message: error.message, type: C.FEEDBACK_TYPE_DANGER },
      });
      dispatch({ type: C.AUTH_LOGOUT });
    });
};

export const registerWithEmailAndPassword = (email, password) => (dispatch) => {
  auth
    .createUserWithEmailAndPassword(email, password)
    .then((user) => {
      if (user) {
        if (!user.displayName) {
          user
            .updateProfile({ displayName: email })
            .then(() => console.log(`updated displayName ${email}`))
            .catch(() => console.log('something went no bueno'));
        }

        user.getIdToken().then((idToken) => {
          // should register here
          console.log('idToken from createUserWithEmailAndPassword:');
          console.log(idToken);
          dispatch(registerUserOnServer(idToken));
        });
      }
    })
    .catch((error) => {
      dispatch({
        type: C.FEEDBACK_SHOW,
        payload: {
          title: 'Registration failed',
          message: error.message,
          type: C.FEEDBACK_TYPE_DANGER,
        },
      });
      dispatch({ type: C.AUTH_LOGOUT });
    });
};

export const sendPasswordResetEmail = (email) => (dispatch) => {
  auth
    .sendPasswordResetEmail(email)
    .then(() => {
      console.log('Password reset email was sent');
      dispatch({
        type: C.FEEDBACK_SHOW,
        payload: {
          title: 'Success',
          message: 'An email with instructions on how to reset your password has been sent.',
          type: C.FEEDBACK_TYPE_INFO,
        },
      });
    })
    .catch((error) => {
      console.log('Could not send password reset email');
      dispatch({
        type: C.FEEDBACK_SHOW,
        payload: {
          title: error.code,
          message: error.message,
          type: C.FEEDBACK_TYPE_DANGER,
        },
      });
    });
};

export const logoutUser = () => (dispatch) => {
  dispatch({ type: C.AUTH_LOGOUT });
  dispatch({ type: C.API_CLIENT_PROFILE_LOADING });
  auth.signOut();
};

export const emailChanged = (text) => ({
  type: C.AUTH_EMAIL_CHANGED,
  payload: text,
});

export const passwordChanged = (text) => ({
  type: C.AUTH_PASSWORD_CHANGED,
  payload: text,
});

export const hasValidToken = () => (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;

  if (token) {
    // following line from official firebase fiddle http://jsfiddle.net/firebase/XDXu5/
    const data = JSON.parse(decodeURIComponent(escape(window.atob(token.split('.')[1]))));

    const currentMillis = new Date().getTime();
    const tokenExpMillis = data.exp * 1000;

    if (currentMillis < tokenExpMillis) return true;
  }

  return false;
};

export const isLoggedIn = () => (dispatch, getState) => {
  const state = getState();
  const { status } = state.auth;

  if (status === C.AUTH_LOGGED_IN) {
    return true;
  }

  return false;
};

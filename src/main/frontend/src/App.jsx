import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { HashRouter } from 'react-router-dom';
import { PersistGate } from 'redux-persist/lib/integration/react';

import { persistor, store } from './store';

import { listenToAuthChange, listenToTokenChange } from './actions/auth';
import Page from './Page';

import './App.css';
import './Oursci.css';

export default class App extends Component {
  componentWillMount() {
    store.dispatch(listenToAuthChange());
    store.dispatch(listenToTokenChange());
  }

  render() {
    const LoadingScreen = () => 'loading...';

    return (
      <Provider store={store}>
        <PersistGate loading={<LoadingScreen />} persistor={persistor}>
          <HashRouter>
            <Page />
          </HashRouter>
        </PersistGate>
      </Provider>
    );
  }
}

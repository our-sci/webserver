import C from '../constants';

const INITIAL_STATE = {
  code: '',
};

export default (state, action) => {
  switch (action.type) {
    case C.INVITATION_CODE_CHANGED:
      return { code: action.payload };
    default:
      return state || INITIAL_STATE;
  }
};

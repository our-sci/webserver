import C from '../constants';

const initialState = [];

// payload is expected as { title, message, type }

export default (state, action) => {
  switch (action.type) {
    case C.FEEDBACK_SHOW:
      return [...state, { ...action.payload }];
    case C.FEEDBACK_DISMISS:
      return state.filter((i, n) => n !== action.num);
    default:
      return state || initialState;
  }
};

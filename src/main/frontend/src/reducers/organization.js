import C from '../constants';

const INITIAL_STATE = {
  list: {
    data: null,
    status: C.STATUS_LOADING,
  },
  find: {
    data: null,
    status: C.STATUS_LOADING,
  },
  current: {
    data: null,
    status: C.STATUS_LOADING,
  },
  myOrganizations: {
    data: null,
    status: C.STATUS_LOADING,
  },
  assets: {
    data: null,
    status: C.STATUS_LOADING,
  },
};

export default (state, action) => {
  switch (action.type) {
    case C.API_ORGANIZATION_LIST_LOADING:
      return { ...state, list: { data: null, status: C.STATUS_LOADING } };
    case C.API_ORGANIZATION_LIST_SUCCESS:
      return { ...state, list: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_ORGANIZATION_LIST_ERROR:
      return { ...state, list: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_ORGANIZATION_FIND_LOADING:
      return { ...state, find: { data: null, status: C.STATUS_LOADING } };
    case C.API_ORGANIZATION_FIND_SUCCESS:
      return { ...state, find: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_ORGANIZATION_FIND_ERROR:
      return { ...state, find: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_ORGANIZATION_MYORGANIZATIONS_LOADING:
      return { ...state, myOrganizations: { data: null, status: C.STATUS_LOADING } };
    case C.API_ORGANIZATION_MYORGANIZATIONS_SUCCESS:
      return { ...state, myOrganizations: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_ORGANIZATION_MYORGANIZATIONS_ERROR:
      return { ...state, myOrganizations: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_ORGANIZATION_ID_LOADING:
      return { ...state, current: { data: null, status: C.STATUS_LOADING } };
    case C.API_ORGANIZATION_ID_SUCCESS:
      return { ...state, current: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_ORGANIZATION_ID_ERROR:
      return { ...state, current: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_ORGANIZATION_ASSETS_LOADING:
      return { ...state, assets: { data: null, status: C.STATUS_LOADING } };
    case C.API_ORGANIZATION_ASSETS_SUCCESS:
      return { ...state, assets: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_ORGANIZATION_ASSETS_ERROR:
      return { ...state, assets: { data: action.payload, status: C.STATUS_ERROR } };

    default:
      return state || INITIAL_STATE;
  }
};

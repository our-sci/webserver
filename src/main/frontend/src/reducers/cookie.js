import C from '../constants';

const INITIAL_STATE = {
  organization: {
    selected: null,
  },
};

export default (state, action) => {
  switch (action.type) {
    case C.COOKIE_CHOOSE_ORGANIZATION:
      return {
        ...state,
        organization: {
          selected: action.payload,
        },
      };
    case C.COOKIE_CHOOSE_ORGANIZATION_NONE:
      return { ...state, organization: { ...INITIAL_STATE.organization } };
    case C.AUTH_LOGOUT:
      return { ...state, organization: { ...INITIAL_STATE.organization } };
    default:
      return state || INITIAL_STATE;
  }
};

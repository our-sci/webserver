import C from '../constants';

const INITIAL_STATE = {
  profile: {
    data: null,
    status: C.STATUS_LOADING,
  },
};

export default (state, action) => {
  switch (action.type) {
    /* api/client/profile */
    case C.API_CLIENT_PROFILE_LOADING:
      return { ...state, profile: { data: null, status: C.STATUS_LOADING } };
    case C.API_CLIENT_PROFILE_SUCCESS:
      return { ...state, profile: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_CLIENT_PROFILE_ERROR:
      return { ...state, profile: { data: action.payload, status: C.STATUS_ERROR } };
    default:
      return state || INITIAL_STATE;
  }
};

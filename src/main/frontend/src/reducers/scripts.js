import C from '../constants';

const INITIAL_STATE = {
  list: {
    data: null,
    status: C.STATUS_LOADING,
  },
  script: {
    data: null,
    status: C.STATUS_LOADING,
  },
  find: {
    data: null,
    status: C.STATUS_LOADING,
  },
};

export default (state, action) => {
  switch (action.type) {
    case C.API_SCRIPT_LIST_LOADING:
      return { ...state, list: { data: null, status: C.STATUS_LOADING } };
    case C.API_SCRIPT_LIST_SUCCESS:
      return { ...state, list: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_SCRIPT_LIST_ERROR:
      return { ...state, list: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_SCRIPT_FIND_LOADING:
      return { ...state, find: { data: null, status: C.STATUS_LOADING } };
    case C.API_SCRIPT_FIND_SUCCESS:
      return { ...state, find: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_SCRIPT_FIND_ERROR:
      return { ...state, find: { data: action.payload, status: C.STATUS_ERROR } };

    case C.API_SCRIPT_ID_LOADING:
      return { ...state, script: { data: null, status: C.STATUS_LOADING } };
    case C.API_SCRIPT_ID_SUCCESS:
      return { ...state, script: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_SCRIPT_ID_ERROR:
      return { ...state, script: { data: action.payload, status: C.STATUS_ERROR } };

    default:
      return state || INITIAL_STATE;
  }
};

import React from 'react';
import { Modal } from 'react-bootstrap';

import Button from './Button';

const MyModal = ({ show, onHide, onOK, title, children }) => (
  <Modal bsSize="lg" show={show} onHide={onHide}>
    <Modal.Header closeButton>
      <Modal.Title>{title}</Modal.Title>
    </Modal.Header>
    <Modal.Body>{children}</Modal.Body>
    <Modal.Footer>
      <Button type="default" onClick={onOK || onHide}>
        OK
      </Button>
    </Modal.Footer>
  </Modal>
);

export default MyModal;

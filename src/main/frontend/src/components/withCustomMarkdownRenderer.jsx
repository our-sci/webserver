import React from 'react';

const withCustomMarkdownRenderer = (WrappedComponent) => {
  class CustomMarkDown extends React.Component {
    // eslint-disable-next-line jsx-a11y/alt-text
    imageRenderer = (props) => <img {...props} style={{ maxWidth: '100%' }} />;

    render() {
      return <WrappedComponent {...this.props} renderers={{ image: this.imageRenderer }} />;
    }
  }

  CustomMarkDown.displayName = `withCustomMarkdownRenderer(${WrappedComponent.displayName ||
    WrappedComponent.name})`;
  return CustomMarkDown;
};

export default withCustomMarkdownRenderer;

import React from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';

class Tabs extends React.Component {
  onClick = (ev, index) => {
    ev.preventDefault();
    this.props.onSelect(index);
  };

  renderLink = (child, index) => {
    const { activeTab } = this.props;

    const className = activeTab === index ? 'nav-link active' : 'nav-link';

    if (child.props.link) {
      return (
        <Link
          to={child.props.link}
          className={className}
          onClick={() => this.props.onSelect(index)}
        >
          {child.props.title}
        </Link>
      );
    }

    return (
      <a
        href={`/tab/${child.props.title.toLowerCase()}`}
        className={className}
        onClick={(ev) => this.onClick(ev, index)}
      >
        {child.props.title}
      </a>
    );
  };

  renderHead = () => (
    <ul className="nav nav-tabs">
      {_.map(this.props.children, (child, index) => (
        <li key={index} className="nav-item">
          {this.renderLink(child, index)}
        </li>
      ))}
    </ul>
  );

  renderContent = () => {
    const { activeTab } = this.props;
    return (
      <div className="tab-container">
        {_.map(this.props.children, (child, index) => activeTab === index && child)}
      </div>
    );
  };

  render() {
    return (
      <div>
        {this.renderHead()}
        {this.renderContent()}
      </div>
    );
  }
}

export default Tabs;

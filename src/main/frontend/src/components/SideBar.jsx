import React from 'react';
import { Link, withRouter } from 'react-router-dom';

import SearchBar from './SearchBar';

export const SidebarLink = (props) => (
  <Link to={props.to} onClick={props.onClick} className="os-toc-link">
    {props.children}
  </Link>
);

class SideBar extends React.Component {
  constructor() {
    super();
    this.state = { collapsed: false };
  }

  collapse = (ev) => {
    ev.preventDefault();
    this.setState({ collapsed: true });
  };

  expand = (ev) => {
    ev.preventDefault();
    this.setState({ collapsed: false });
  };

  render() {
    if (this.state.collapsed) {
      return (
        <div className="sidebar-collapsed p-2">
          <ul className="list-unstyled">
            <li>
              <Link to="/survey">
                <strong>Su</strong>
              </Link>
            </li>
            <li>
              <Link to="/script">
                <strong>Sc</strong>
              </Link>
            </li>
            <li>
              <Link to="/dashboard">
                <strong>Da</strong>
              </Link>
            </li>
            <li>
              <Link to="/organization">
                <strong>Or</strong>
              </Link>
            </li>
            <li className="mt-2">
              <Link to="/share">
                <strong>Sh</strong>
              </Link>
            </li>
          </ul>

          <a href="/open" onClick={this.expand} style={{ color: 'rgba(0,0,0,0.3)' }}>
            <i className="fas fa-angle-double-right" />
          </a>
        </div>
      );
    }

    return (
      <div className="col-12 col-md-3 os-sidebar">
        <SearchBar />
        <nav className="collapse os-links d-md-block" id="os-docs-nav">
          <div className="dropdown mb-2" style={{ padding: '.25rem 1.5rem' }}>
            <button
              className="btn btn-primary w-100"
              type="button"
              id="dropdownMenuButton"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <i className="fas fa-plus mr-2" />
              Create new...
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <Link to="/survey/create" className="dropdown-item">
                Survey
              </Link>
              <Link to="/script/create" className="dropdown-item">
                Script
              </Link>
              <Link to="/dashboard/create" className="dropdown-item">
                Dashboard
              </Link>
              <Link to="/organization/create" className="dropdown-item">
                Organization
              </Link>
            </div>
          </div>
          <div className="os-toc-item">
            <SidebarLink to="/survey">Surveys</SidebarLink>
          </div>
          <div className="os-toc-item">
            <SidebarLink to="/script">Scripts</SidebarLink>
          </div>
          <div className="os-toc-item">
            <SidebarLink to="/dashboard">Dashboards</SidebarLink>
          </div>
          <div className="os-toc-item">
            <SidebarLink to="/organization">Organizations</SidebarLink>
          </div>
          <div className="os-toc-item mt-2">
            <SidebarLink to="/share">Shares</SidebarLink>
          </div>

          <div className="os-toc-item mt-3 d-none d-md-block">
            <a
              href="/collapse"
              className="os-toc-link"
              onClick={this.collapse}
              style={{ color: 'rgba(0,0,0,0.3)' }}
            >
              <i className="fas fa-angle-double-left" />
            </a>
          </div>

          {/* <div className="os-toc-item active">
      <a className="os-toc-link" href="/docs/4.1/about/overview/">
        About
      </a>

      <ul className="nav os-sidenav">
        <li className="active os-sidenav-active">
          <a href="/docs/4.1/about/overview/">Overview</a>
        </li>
        <li>
          <a href="/docs/4.1/about/brand/">Brand</a>
        </li>
        <li>
          <a href="/docs/4.1/about/license/">License</a>
        </li>
        <li>
          <a href="/docs/4.1/about/translations/">Translations</a>
        </li>
      </ul>
    </div> */}
        </nav>
      </div>
    );
  }
}

export default withRouter(SideBar);

import React from 'react';
import SockJsClient from 'react-stomp';

class LiveTracker extends React.Component {
  constructor(props) {
    super(props);
    this.state = { surveys: 0, surveyResults: 0 };
  }

  sendMessage = (msg) => {
    console.log('sending message now...');
    if (this.clientRef) this.clientRef.sendMessage('/app/hello', msg);
  };

  renderNumResults = () => {
    if (this.state.surveyResults === 0) {
      return null;
    }

    return (
      <h5>
        {this.state.surveyResults} surveys submitted
        <small className="text-secondary">... and counting!</small>
      </h5>
    );
  };

  render() {
    return (
      <div className={this.props.className}>
        {this.renderNumResults()}
        <SockJsClient
          url="/websocket"
          topics={['/topic/stats']}
          onMessage={(msg) => {
            console.log(msg);
            const { surveys, surveyResults } = msg;
            this.setState({ surveys, surveyResults });
          }}
          onConnect={() => this.sendMessage('{"name": "anybody"}')}
          ref={(client) => {
            this.clientRef = client;
          }}
        />
      </div>
    );
  }
}

export default LiveTracker;

import React from 'react';
import PropTypes from 'prop-types';

const ExternalWindowButton = ({ href, children, style, className }) => (
  <a href={href} target="_blank" rel="noopener noreferrer" className={className}>
    <button type="button" className="btn btn-outline-info" style={style}>
      <i className="fas fa-external-link-alt mr-2" />
      {children}
    </button>
  </a>
);

ExternalWindowButton.propTypes = {
  href: PropTypes.string,
  style: PropTypes.any,
  children: PropTypes.node,
  className: PropTypes.string,
};

ExternalWindowButton.defaultProps = {
  href: '',
  style: {},
  children: '',
  className: '',
};

export default ExternalWindowButton;

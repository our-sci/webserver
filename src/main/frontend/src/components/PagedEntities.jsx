import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import moment from 'moment';

import _ from 'lodash';

import C from '../constants';

const DEFAULT_SIZE = 5;

const ViewMode = {
  DEFAULT: 'default',
  MORE: 'more',
  LESS: 'less',
  ALL: 'all',
};

class PagedEntities extends Component {
  constructor(props) {
    super(props);
    this.state = { page: 0, size: props.size };
  }

  componentWillMount() {
    if (!this.props.entities || this.props.entities.status !== C.STATUS_SUCCESS) {
      this.props.recall(this.props.size);
    } else if (this.props.entities && this.props.entities.status === C.STATUS_SUCCESS) {
      const cachedSize = this.props.entities.data.content.length;
      if (cachedSize > this.state.size) {
        this.setState({ size: cachedSize });
      }
    }
  }

  componentDidMount() {
    this.fetchData(this.props.size);
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props;
    if (oldProps.id !== newProps.id) {
      this.props.recall(this.props.size);
    }
  }

  handleViewMode = (ev, mode) => {
    ev.preventDefault();

    const { totalElements } = this.props.entities.data;

    let newSize = this.props.size;

    switch (mode) {
      case ViewMode.DEFAULT:
        newSize = this.props.size;
        break;
      case ViewMode.MORE:
        newSize = this.state.size + this.props.size;
        if (newSize > totalElements) {
          newSize = totalElements;
        }
        break;
      case ViewMode.LESS:
        newSize = this.state.size - this.props.size;
        if (newSize < this.props.size) {
          newSize = this.props.size;
        }
        break;
      case ViewMode.ALL:
        newSize = totalElements;
        break;
      default:
        break;
    }

    this.fetchData(newSize);
  };

  fetchData = (newSize) => {
    this.setState({ size: newSize || this.props.size }, () => this.props.recall(this.state.size));
  };

  renderViewModeLink = (mode) => {
    let linkText = '';

    switch (mode) {
      case ViewMode.DEFAULT:
        linkText = 'View recent only';
        break;
      case ViewMode.MORE:
        linkText = 'more';
        break;
      case ViewMode.LESS:
        linkText = 'less';
        break;
      case ViewMode.ALL:
        linkText = `View all (${this.props.entities.data.totalElements})`;
        break;
      default:
        break;
    }

    return (
      <a href={mode} onClick={(ev) => this.handleViewMode(ev, mode)}>
        {linkText}
      </a>
    );
  };

  renderViewAllOrRecent = () => {
    const { size } = this.state;
    const { totalElements } = this.props.entities.data;

    if (size > totalElements || size === this.props.size) {
      return null;
    }

    if (size === totalElements) {
      return this.renderViewModeLink(ViewMode.DEFAULT);
    }

    return this.renderViewModeLink(ViewMode.ALL);
  };

  renderViewMoreOrLess = () => {
    const { size } = this.state;
    const { totalElements } = this.props.entities.data;

    if (size > totalElements || totalElements === this.props.size) {
      return null;
    }

    if (size === totalElements) {
      return this.renderViewModeLink(ViewMode.LESS);
    }

    if (size === DEFAULT_SIZE) {
      return this.renderViewModeLink(ViewMode.MORE);
    }

    return (
      <div>
        {this.renderViewModeLink(ViewMode.LESS)}
        &nbsp;|&nbsp;
        {this.renderViewModeLink(ViewMode.MORE)}
      </div>
    );
  };

  renderDate = (entity) => {
    if (!this.props.showDate) {
      return null;
    }

    const date = this.props.getDate(entity);

    if (!date) {
      return (
        <div className="float-right">
          <small>---</small>
        </div>
      );
    }

    return (
      <div className="float-right">
        <small>{moment(date).format('DD MMM YYYY, HH:mm')}</small>
        <br />
        <small className="float-right">{moment(date).fromNow()}</small>
      </div>
    );
  };

  renderFooter = () => {
    const { size } = this.state;
    const { totalElements } = this.props.entities.data;

    if (totalElements === 0) {
      return <div className="card-body">No entities so far...</div>;
    }

    if (size > totalElements || totalElements === this.props.size) {
      return null; // footer is not needed if initial size is greater than total elements
    }

    return (
      <div className="card-body clearfix" style={{ padding: '10px', paddingRight: '15px' }}>
        <span className="float-right">{this.renderViewMoreOrLess()}</span>
      </div>
    );
  };

  renderEntities = () => {
    const { entities, query } = this.props;

    return (
      <div className="card">
        <div className="card-header text-dark">
          {this.props.title || 'No title defined'}
          <span className="float-right">{this.renderViewAllOrRecent()}</span>
        </div>
        <ul className="list-group list-group-flush">
          {_.map(entities.data.content, (entity, index) => (
            <li className="list-group-item clearfix" key={entity.id || index}>
              {this.renderDate(entity)}
              {this.props.renderItem({ item: entity, query })}
            </li>
          ))}
        </ul>
        {this.renderFooter()}
      </div>
    );
  };

  render() {
    const { entities } = this.props;

    if (!entities) {
      return null;
    }

    if (entities.status !== C.STATUS_SUCCESS) {
      return null;
    }

    return <div className="mb-4">{this.renderEntities()}</div>;
  }
}

PagedEntities.defaultProps = {
  size: DEFAULT_SIZE,
  recall: () => console.log('What to do, what to do...'),
  title: 'Entities',
  showDate: false,
  getDate: (entity) => entity.date,
  id: 'default',
  renderItem: () => <span>Item</span>,
  query: null,
};

export default withRouter(PagedEntities);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import _ from 'lodash';

import { logoutUser } from '../actions/auth';
import { findMyOrganizations, getProfile, isAdmin } from '../actions/api';
import { cookieChooseOrganization, cookieChooseOrganizationNone } from '../actions/cookie';

import C from '../constants';

function headerStyle(strings, preferences) {
  const { colorTextPrimary, colorTextPrimaryHover, colorPrimary } = preferences;

  return `.os-navbar .navbar-nav .nav-link { color: ${colorTextPrimary}}
  .os-navbar .navbar-nav .nav-link:hover, .os-navbar .navbar-nav .nav-link:focus   { color: ${colorTextPrimaryHover}}
  .os-navbar { background-color: ${colorPrimary}}
  .navbar-nav>li>.dropdown-menu { background-color: ${colorPrimary}}
  .navbar-nav>li>.dropdown-menu>.dropdown-item { color: ${colorTextPrimary}}
  .navbar-nav>li>.dropdown-menu>.dropdown-item:hover { color: ${colorTextPrimaryHover}; background-color: ${colorPrimary}}
  .navbar-nav>li>.dropdown-menu>.dropdown-item:focus { color: ${colorTextPrimaryHover}; background-color: ${colorPrimary}}
  .os-navbar .navbar-brand { color: ${colorTextPrimary}}
  .os-navbar .navbar-brand:hover { color: ${colorTextPrimaryHover}}
  `;
}

const HeaderLink = ({ children, to, icon, onClick }) => (
  <Link to={to} className="nav-link" onClick={onClick}>
    {icon && <span className={icon} />} <span className="d-none d-lg-inline">{children}</span>
  </Link>
);

const ExternalLink = ({ children, to, icon, onClick }) => (
  <a href={to} className="nav-link" onClick={onClick} target="_blank" rel="noopener noreferrer">
    {icon && <span className={icon} />} <span className="d-none d-lg-inline">{children}</span>
  </a>
);

class Header extends Component {
  componentDidMount() {
    const selectedOrganization = this.props.cookie.organization.selected;
    this.applyOrganization(selectedOrganization);

    this.props.getProfile();
    this.props.findMyOrganizations();
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.auth.status !== this.props.auth.status &&
      nextProps.auth.status === C.AUTH_LOGGED_IN
    ) {
      this.props.findMyOrganizations();
    }

    const nextSelectedOrganization = nextProps.cookie.organization.selected;
    const currentSelectedOrganization = this.props.cookie.organization.selected;

    if (!_.isEqual(nextSelectedOrganization, currentSelectedOrganization)) {
      this.applyOrganization(nextSelectedOrganization);
    }
  }

  getBrandTextTag = (text) => `<span class="brand-text">${text}</span>`;

  applyOrganization = (organization) => {
    const styleId = 'style-preferences';
    const brandId = 'navbar-brand';

    // clear
    const existing = document.getElementById(styleId);
    if (existing) {
      document.getElementById(styleId).outerHTML = '';
    }
    document.getElementById(brandId).innerHTML = this.getBrandTextTag('Our-Sci');

    // apply
    if (organization) {
      document.getElementById(brandId).innerHTML = this.getBrandTextTag(organization.name);

      const { preferences } = organization;

      if (preferences) {
        const brandImage = preferences.showIcon
          ? `<img src='${organization.icon}' class="brand-image" />`
          : '';
        const brandText = this.getBrandTextTag(`${preferences.title || organization.name}`);

        document.getElementById(brandId).innerHTML = `${brandImage} ${brandText}`;

        const s = document.createElement('style');
        s.id = styleId;
        s.type = 'text/css';
        s.innerHTML = headerStyle`${preferences}`;
        document.head.appendChild(s);
      }
    }
  };

  renderNavBarNew = () => (
    <header className="navbar navbar-expand navbar-light flex-column flex-md-row os-navbar">
      <a className="navbar-brand mr-0 mr-md-2" href="/" aria-label="Our-Sci" id="navbar-brand">
        Our-Sci
      </a>

      <div className="navbar-nav">
        <ul className="navbar-nav flex-row">
          <li className="nav-item">
            <Link to="/" className="nav-link">
              Home
            </Link>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="https://our-sci.gitlab.io/documentation">
              Documentation
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link" href="http://blog.our-sci.net/" rel="noopener">
              Blog
            </a>
          </li>
        </ul>
      </div>

      <ul className="navbar-nav flex-row ml-md-auto d-flex">
        <li className="nav-item">
          <ExternalLink to="https://gitlab.com/our-sci" icon="fab fa-gitlab">
            Source
          </ExternalLink>
        </li>
        <li className="nav-item">
          <HeaderLink to="/mobile-app" icon="fab fa-android">
            App
          </HeaderLink>
        </li>
        {this.props.auth.status === C.AUTH_LOGGED_IN && (
          <li className="nav-item">
            <HeaderLink to="/profile" icon="fas fa-user">
              Profile
            </HeaderLink>
          </li>
        )}
        {this.props.auth.status === C.AUTH_LOGGED_IN &&
          this.props.isAdmin() && (
            <li className="nav-item">
              <HeaderLink to="/admin/users" icon="fas fa-hammer">
                Admin
              </HeaderLink>
            </li>
          )}
        <li className="nav-item">{this.renderLoginLogoutLink()}</li>
      </ul>
    </header>
  );

  renderLoginLogoutLink = () => {
    if (this.props.auth.status !== C.AUTH_LOGGED_IN) {
      return (
        <HeaderLink to="/login" icon="fas fa-sign-in-alt">
          Sign in
        </HeaderLink>
      );
    }

    return (
      <HeaderLink to="/" icon="fas fa-sign-out-alt" onClick={this.props.logoutUser}>
        Sign out
      </HeaderLink>
    );
  };

  render() {
    return this.renderNavBarNew();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  organizations: state.organizations,
  cookie: state.cookie,
});

const mapDispatchToProps = {
  logoutUser,
  getProfile,
  findMyOrganizations,
  cookieChooseOrganization,
  cookieChooseOrganizationNone,
  isAdmin,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);

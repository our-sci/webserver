import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const LinkButton = ({ to, children, icon, type, onClick }) => {
  const buttonClass = `btn btn-${type}`;

  return (
    <Link to={to} className={buttonClass} onClick={onClick}>
      <span className={icon} aria-hidden="true" />
      <span className="ml-2">{children}</span>
    </Link>
  );
};

LinkButton.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node,
  icon: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
};

LinkButton.defaultProps = {
  children: '',
  icon: 'fas fa-plus',
  type: 'primary',
  onClick: (ev) => ev,
};

export default LinkButton;

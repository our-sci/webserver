import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const ModalOrLink = ({
  showModal,
  onHide,
  onConfirmClicked,
  title,
  children,
  link,
  onLinkClicked,
  confirmText,
  confirmActive,
  cancelText,
  severity,
  className,
}) => {
  if (showModal) {
    return (
      <Modal size="lg" isOpen={showModal} onClosed={onHide} toggle={onHide}>
        <ModalHeader toggle={onHide}>{title}</ModalHeader>
        <ModalBody>{children}</ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={onHide}>
            {cancelText}
          </Button>
          <Button color={severity} onClick={onConfirmClicked || onHide} disabled={!confirmActive()}>
            {confirmText}
          </Button>
        </ModalFooter>
      </Modal>
    );
  }

  return (
    <a
      className={className}
      href="/confirm"
      onClick={(ev) => {
        ev.preventDefault();
        onLinkClicked();
      }}
    >
      {link}
    </a>
  );
};

ModalOrLink.defaultProps = {
  confirmText: 'OK',
  cancelText: 'Cancel',
  severity: 'default',
  confirmActive: () => true,
};

export default ModalOrLink;

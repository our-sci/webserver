import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';

import axios from 'axios';

import { updateProfile } from '../actions/api';

import { basename } from '../actions/utils';
import Spacer from './Spacer';

class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editing: false,
      displayName: '',
      picture: null, // URL to picture
      pictureFile: null, // local file handle for picture
      clearPicture: false,
      profile: null,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.uid !== prevProps.uid) {
      this.fetchData();
    }
  }

  fetchData = () => {
    const { uid } = this.props;

    axios({
      url: `/api/profile/uid/${uid}`,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'X-Authorization-Firebase': this.props.auth.token,
      },
    })
      .then((response) => this.setState({ profile: response.data }))
      .catch((error) => console.log(error));
  };

  startEditing = () => {
    const { displayName, picture } = this.state.profile;
    this.setState({ displayName, picture, clearPicture: false, editing: true });
  };

  cancelEditing = () => {
    this.setState({ editing: false });
  };

  updateProfileFromForm = () => {
    const { uid } = this.props;
    const { displayName, pictureFile, clearPicture } = this.state;

    this.props
      .updateProfile({ uid, displayName, picture: pictureFile, clearPicture })
      .then(() => this.fetchData());
    this.setState({ editing: false });
  };

  renderProfilePicture = () => {
    const { picture } = this.state.profile;

    if (!picture) {
      return (
        <div style={{ width: '100px', height: '100px', border: '1px solid #333' }}>No picture</div>
      );
    }

    return <img alt="profile" height="100" width="100" src={picture} />;
  };

  renderEditing = () => {
    if (!this.state.editing) return null;

    const { displayName, picture, clearPicture } = this.state;
    const pictureName = basename(picture);
    return (
      <div className="mt-2 clearfix">
        <h3>Edit Profile</h3>
        <form>
          <div className="form-group">
            <label htmlFor="displayName">Name</label>
            <input
              className="form-control"
              id="displayName"
              value={displayName}
              onChange={(ev) => this.setState({ displayName: ev.target.value })}
            />
          </div>

          <div className="form-row align-items-end">
            <div className="col">
              <div className="form-group">
                <label htmlFor="iconfile">
                  Icon{' '}
                  {picture && (
                    <a href={picture} target="_blank" rel="noopener noreferrer">
                      {pictureName}
                    </a>
                  )}
                </label>
                <input
                  type="file"
                  className="form-control-file"
                  id="iconfile"
                  onChange={(ev) =>
                    this.setState({ pictureFile: ev.target.files[0], picture: null })
                  }
                />
              </div>
            </div>
            <div className="col-auto">
              <div className="form-check mb-3">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="remove"
                  checked={clearPicture}
                  onChange={(ev) => this.setState({ clearPicture: ev.target.checked })}
                />
                <label htmlFor="remove" className="form-check-label">
                  Clear
                </label>
              </div>
            </div>
          </div>
        </form>
        <div className="btn-toolbar float-right">
          <Button outline color="secondary" onClick={this.cancelEditing} className="mr-1">
            Cancel
          </Button>
          <Button color="primary" onClick={this.updateProfileFromForm}>
            Update
          </Button>
        </div>
      </div>
    );
  };

  renderProfile = (profile) => (
    <div className="clearfix">
      <div style={{ float: 'left', marginRight: '10px' }}>{this.renderProfilePicture()}</div>
      <div>
        <h3 style={{ marginTop: '5px' }}>{profile.displayName}</h3>
        <h4>{profile.email}</h4>
        <h5 style={{ color: '#666' }}>{profile.username}</h5>
      </div>
    </div>
  );

  render() {
    const { profile } = this.state;

    if (!profile) {
      return <span>loading...</span>;
    }

    return (
      <div>
        {this.props.editable &&
          !this.state.editing && (
            <span className="float-right">
              <Button color="primary" onClick={this.startEditing}>
                <i className="fas fa-edit mr-2" />
                Edit
              </Button>
            </span>
          )}
        {this.renderProfile(profile)}
        <Spacer height={15} />
        {this.renderEditing()}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = {
  updateProfile,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import axios from 'axios';
import _ from 'lodash';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import * as Papa from 'papaparse';
import ReactTable from 'react-table';
import checkboxHOC from 'react-table/lib/hoc/selectTable';

import 'react-table/react-table.css';

import C from '../../constants';
import {
  getSurveyById,
  getSurveyResultCsvById,
  isAdmin,
  archiveSurveyResult,
} from '../../actions/api';
import { clearSurveyResultCsv } from '../../actions';
import DownloadButton from '../DownloadButton';
import Toggle from '../Toggle';
import ModalOrLink from '../ModalOrLink';
import SharecodeCreator from '../sharecode/SharecodeCreator';
import SurveyResubmitModal from './SurveyResubmitModal';

const CheckboxTable = checkboxHOC(ReactTable);

const column = {
  // Renderers
  Cell: undefined,
  Header: undefined,
  Footer: undefined,
  Aggregated: undefined,
  Pivot: undefined,
  PivotValue: undefined,
  Expander: undefined,
  Filter: undefined,
  // Standard options
  sortable: undefined, // use table default
  resizable: undefined, // use table default
  filterable: undefined, // use table default
  show: true,
  minWidth: 100,
  // Cells only
  className: '',
  style: { padding: '6px' },
  getProps: () => ({}),
  // Headers only
  headerClassName: '',
  headerStyle: { fontWeight: 'bold', padding: '10px', textAlign: 'left' },
  getHeaderProps: () => ({}),
  // Footers only
  footerClassName: '',
  footerStyle: {},
  getFooterProps: () => ({}),
  filterAll: false,
  filterMethod: undefined,
  sortMethod: undefined,
  defaultSortDesc: undefined,
};

class SurveyTable extends Component {
  constructor(props) {
    super(props);
    this.renderSurvey = this.renderSurvey.bind(this);
    this.renderTable = this.renderTable.bind(this);
    this.renderSelection = this.renderSelection.bind(this);
    this.downloadCSV = this.downloadCSV.bind(this);

    this.toggleArchived = this.toggleArchived.bind(this);
    this.fetchData = this.fetchData.bind(this);

    this.state = {
      dateFrom: moment().subtract(7, 'days'),
      dateTo: moment(),
      filterByDate: false,
      filterPersonal: false,
      selection: [],
      selectAll: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps, prevState) {
    const { filterByDate, dateFrom, dateTo, filterPersonal } = this.state;

    if (filterPersonal !== prevState.filterPersonal) {
      this.fetchData();
    }

    if (filterByDate !== prevState.filterByDate) {
      this.fetchData();
    }

    if (dateFrom !== prevState.dateFrom || dateTo !== prevState.dateTo) {
      this.fetchData();
    }

    if (prevProps.formId !== this.props.formId) {
      this.fetchData();
    }
  }

  componentWillUnmount() {
    this.props.clearSurveyResultCsv(this.props.formId);
  }

  toggleSelection = (key, shift, row) => {
    // start off with the existing state
    let selection = [...this.state.selection];
    const keyIndex = selection.indexOf(key);
    // check to see if the key exists
    if (keyIndex >= 0) {
      // it does exist so we will remove it using destructing
      selection = [...selection.slice(0, keyIndex), ...selection.slice(keyIndex + 1)];
    } else {
      // it does not exist so add it
      selection.push(key);
    }
    // update the state
    this.setState({ selection });
  };

  toggleAll = () => {
    const selectAll = !this.state.selectAll;
    const selection = [];
    if (selectAll) {
      // we need to get at the internals of ReactTable
      const wrappedInstance = this.surveyTable.getWrappedInstance();
      // the 'sortedData' property contains the currently accessible records based on the filter and sort
      const currentRecords = wrappedInstance.getResolvedState().sortedData;
      // we just push all the IDs onto the selection array
      currentRecords.forEach((item) => {
        selection.push(item._original._id);
      });
    }
    this.setState({ selectAll, selection });
  };

  isSelected = (key) => this.state.selection.includes(key);

  logSelection = () => {
    console.log('selection:', this.state.selection);
  };

  hasAdminRights = (survey) => {
    const { uid } = this.props.auth;

    if (uid === survey.data.user.username) {
      return true;
    }

    if (this.props.isAdmin()) {
      return true;
    }

    return false;
  };

  handleArchiveResults = (selection) => {
    this.props.archiveSurveyResult(selection).then(() => this.fetchData());
    this.setState({ confirmArchiving: false, selection: [] });
  };

  handleDateFromChange(dateFrom) {
    this.setState({ dateFrom });
  }

  handleDateToChange(dateTo) {
    this.setState({ dateTo });
  }

  toggleArchived() {
    const { formId } = this.props;

    const requestUrl = `/api/survey/toggle/archived/by-form-id/${formId}`;

    axios({
      url: requestUrl,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(() => {
        this.props.getSurveyById(formId);
      })
      .catch(() => {});
  }

  fetchData() {
    const { formId, sharecode } = this.props;
    const { filterByDate, dateFrom, dateTo, filterPersonal } = this.state;

    this.props.getSurveyById(formId);

    const params = { formId, from: null, to: null, size: this.props.size, user: null, sharecode };

    if (filterByDate) {
      try {
        params.from = dateFrom.format(C.DATE_FORMAT);
        params.to = dateTo.format(C.DATE_FORMAT);
      } catch (e) {
        // nop
      }
    }

    if (filterPersonal) {
      params.user = this.props.auth.uid;
    }

    this.props.getSurveyResultCsvById(params);
  }

  downloadCSV(ev) {
    ev.preventDefault();
    const { formId } = this.props;
    const result = this.props.surveyResults[formId];
    const filename = `${formId}.csv`;

    const dataset = _.filter(result.data.csv, (row, idx) => {
      console.log(idx);
      console.log(row);
      if (this.state.selection.length === 0) return true; // export everything when nothing is selected
      if (idx === 0) return true;
      if (_.includes(this.state.selection, row[2])) return true;
      return false;
    });

    const blob = new Blob([Papa.unparse(dataset)]);
    if (window.navigator.msSaveOrOpenBlob) {
      // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
      window.navigator.msSaveBlob(blob, filename);
    } else {
      const a = window.document.createElement('a');
      a.href = window.URL.createObjectURL(blob, { type: 'text/plain' });
      a.download = filename;
      document.body.appendChild(a);
      a.click(); // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
      document.body.removeChild(a);
    }
  }

  renderArchive = () => {
    const { survey } = this.props.surveys;
    const { selection } = this.state;

    if (selection.length > 0 && this.hasAdminRights(survey)) {
      return (
        <ModalOrLink
          showModal={this.state.confirmArchiving}
          onHide={() => this.setState({ confirmArchiving: false })}
          title="Confirm your action"
          onLinkClicked={() => this.setState({ confirmArchiving: true })}
          link="Archive..."
          severity="danger"
          confirmText="ARCHIVE"
          onConfirmClicked={() => this.handleArchiveResults(selection)}
        >
          Are you sure you want to archive the data sets with instance id
          <br />
          <ul>
            {_.map(selection, (iid) => (
              <li key={iid}>
                <strong>{iid}</strong>
              </li>
            ))}
          </ul>
        </ModalOrLink>
      );
    }

    return null;
  };

  renderResubmit = () => {
    // const { survey } = this.props.surveys;
    const { selection } = this.state;

    if (selection.length === 1 && this.props.isAdmin()) {
      return <SurveyResubmitModal instanceIds={selection} />;
    }

    return null;
  };

  renderShare = () => {
    const { selection } = this.state;
    if (selection.length > 0 && this.props.isAdmin()) {
      return <SharecodeCreator instanceIds={selection} />;
    }

    return null;
  };

  renderSelection() {
    if (!this.props.checkable) return null;
    const { selection } = this.state;

    return (
      <h6 className="float-right pt-2">
        {selection.length} rows selected {this.renderArchive()} {this.renderResubmit()}{' '}
        {this.renderShare()}
      </h6>
    );
  }

  renderTable() {
    const { formId } = this.props;
    const result = this.props.surveyResults[formId];
    if (!result) {
      return null; // result not yet defined
    }

    const { toggleSelection, toggleAll, isSelected } = this;
    const { selectAll } = this.state;

    if (result.status === C.STATUS_LOADING) {
      return <div>loading ...</div>;
    }

    if (result.status === C.STATUS_ERROR) {
      return <div>Error: {result.data}</div>;
    }

    const { table } = result.data;

    const checkboxProps = {
      selectAll,
      isSelected,
      toggleSelection,
      toggleAll,
      selectType: 'checkbox',
    };

    if (!this.props.checkable) {
      table.columns[0].show = false;
      table.columns[1].show = false;
      table.columns[2].show = false;
      table.columns[3].show = false;

      return (
        <ReactTable
          ref={(r) => {
            this.surveyTable = r;
          }}
          data={table.data}
          columns={table.columns}
          column={column}
          defaultPageSize={this.props.size}
          className="-striped -highlight mt-2"
          showPagination={false}
          filterable
        />
      );
    }

    return (
      <CheckboxTable
        ref={(r) => {
          this.surveyTable = r;
        }}
        data={table.data}
        columns={table.columns}
        column={column}
        defaultPageSize={10}
        className="-striped -highlight mt-2"
        {...checkboxProps}
        filterable
      />
    );
  }

  renderFormTitle = () => {
    const { formTitle, formId } = this.props.surveys.survey.data;

    const DELIMITER = ';';

    const url = `/survey/by-form-id/${formId}`;

    if (formTitle.includes(DELIMITER)) {
      const mainTitle = formTitle.substring(0, formTitle.indexOf(DELIMITER));
      const subTitle = formTitle.substring(formTitle.indexOf(DELIMITER) + 1);
      return (
        <div>
          <h3 className="mb-1">
            <Link to={url}>{mainTitle}</Link> <br />
            <small>{subTitle}</small>
          </h3>
          <span className="text-secondary">{formId}</span>
        </div>
      );
    }

    return (
      <div>
        <h3 className="mb-1">
          <Link to={url}>{formTitle}</Link>
        </h3>
        <span className="text-secondary">{formId}</span>
      </div>
    );
  };

  renderPersonalFilter = () => {
    const { filterPersonal } = this.state;

    if (!this.props.showDateFilter) {
      return null;
    }

    if (this.props.auth.status !== C.AUTH_LOGGED_IN) {
      return null;
    }

    return (
      <div className="d-inline">
        <div className="form-group form-check d-inline-block">
          <input
            type="checkbox"
            className="form-check-input"
            id="filterPersonal"
            checked={filterPersonal}
            onChange={() => this.setState({ filterPersonal: !filterPersonal })}
          />
          <label htmlFor="filterPersonal">Filter by me</label>
        </div>
      </div>
    );
  };

  renderDateFilter = () => {
    const { dateTo, dateFrom, filterByDate } = this.state;

    if (!this.props.showDateFilter) {
      return null;
    }

    return (
      <div className="mt-1 d-inline">
        <div className="form-group form-check d-inline-block">
          <input
            type="checkbox"
            className="form-check-input"
            id="filterByDate"
            onChange={() => this.setState({ filterByDate: !filterByDate })}
            checked={filterByDate}
          />
          <label className="form-check-label" htmlFor="filterByDate">
            Filter by date
          </label>
        </div>
        <div style={{ padding: '0.6em', display: 'inline-block' }}>
          <DatePicker
            disabled={!filterByDate}
            maxDate={dateTo}
            dateFormat={C.DATE_FORMAT}
            selected={dateFrom}
            onChange={(date) => this.setState({ dateFrom: date })}
          />
        </div>
        <span>-</span>
        <div style={{ padding: '0.6em', display: 'inline-block' }}>
          <DatePicker
            disabled={!filterByDate}
            minDate={dateFrom}
            dateFormat={C.DATE_FORMAT}
            selected={dateTo}
            onChange={(date) => this.setState({ dateTo: date })}
          />
        </div>
      </div>
    );
  };

  renderControls = (survey) => {
    if (!this.props.showControls) {
      return null;
    }

    return (
      <div className="float-right">
        {this.hasAdminRights(survey) && (
          <Toggle
            checked={survey.data.archived}
            onToggle={this.toggleArchived}
            textChecked="RESTORE"
            textUnchecked="ARCHIVE"
            className="mr-1"
          />
        )}
        <DownloadButton
          className="mr-1"
          href={`/api/survey/download/zip/by-form-id/${survey.data.formId}`}
        >
          Survey as ZIP
        </DownloadButton>
        <a
          role="button"
          className="btn btn-outline-primary"
          href={`/download/csv/${survey.data.formId}`}
          onClick={this.downloadCSV}
        >
          <i className="fas fa-download mr-2" />
          Data as CSV
        </a>
      </div>
    );
  };

  renderSurvey() {
    const { survey } = this.props.surveys;

    if (survey.status === C.STATUS_LOADING) {
      return <div>loading...</div>;
    }

    if (survey.status === C.STATUS_ERROR) {
      return <div>Error: {survey.data}</div>;
    }

    return (
      <div>
        {this.renderControls(survey)}
        {this.renderFormTitle()}
        {this.renderDateFilter()}
        {this.renderPersonalFilter()}
        {this.renderTable()}
        {this.renderSelection(survey)}
      </div>
    );
  }

  render() {
    return this.renderSurvey();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
  surveyResults: state.surveyResults,
  surveys: state.surveys,
});

const mapDispatchToProps = {
  getSurveyById,
  getSurveyResultCsvById,
  clearSurveyResultCsv,
  isAdmin,
  archiveSurveyResult,
};

SurveyTable.defaultProps = {
  checkable: true,
  sharecode: null,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SurveyTable);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import ModalOrLink from '../ModalOrLink';

import { getSurveyResult } from '../../actions/api';
import { showFeedback } from '../../actions/utils';

class SurveyResubmitModal extends Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, link: '' };
  }

  componentDidMount() {
    console.log(`getting info for instanceId ${this.props.instanceIds[0]}`);
    const instanceId = this.props.instanceIds[0];
    if (instanceId) {
      this.props.getSurveyResult(instanceId).then((response) => {
        console.log(response.data);
        const result = response.data;
        let link = `/storage/survey-results/${result.survey.formId}/${result.instanceId}/${
          result.instanceFile
        }?token=${this.props.auth.token}`;
        if (process.env.NODE_ENV === 'development') {
          // proxy does not work with html pages
          link = `http://localhost:5000${link}`;
        }
        this.setState({ link });
      });
    }
  }

  submit = () => {
    console.log('submitting');
  };

  render() {
    const { showModal } = this.state;
    const { instanceIds } = this.props;

    return (
      <ModalOrLink
        showModal={showModal}
        onHide={() => this.setState({ showModal: false })}
        title="Resubmit a survey"
        onLinkClicked={() => this.setState({ showModal: true })}
        link="Resubmit..."
        onConfirmClicked={this.submit}
      >
        <ol className="">
          <li className="p-1">
            Download current xml file <i>(right click and Save link as...)</i>
            <br />
            <a href={this.state.link} target="_blank" rel="noopener noreferrer">
              {instanceIds[0]}
            </a>
          </li>
          <li className="p-1">Edit xml file (and optionally rename it)</li>
          <li className="p-1">
            Go to <Link to="/admin/resubmissions">Resubmissions</Link> in the admin area and
            re-upload.
          </li>
        </ol>

        <br />
      </ModalOrLink>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = {
  showFeedback,
  getSurveyResult,
};

SurveyResubmitModal.defaultProps = {
  instanceIds: [],
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SurveyResubmitModal);

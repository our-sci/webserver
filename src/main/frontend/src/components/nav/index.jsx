import React from 'react';
import { Link } from 'react-router-dom';

import BrandToggle from './BrandToggle';
import Fluid from './Fluid';
import Collapse from './Collapse';

export const NavbarNav = (props) => {
  const alignmentClass = props.alignment ? `navbar-${props.alignment}` : '';
  return <ul className={`nav navbar-nav ${alignmentClass}`}>{props.children}</ul>;
};

export const ItemLink = (props) => (
  <li>
    <Link to={props.to} onClick={props.onClick}>
      {props.children}
    </Link>
  </li>
);

export const Dropdown = (props) => (
  <li className="dropdown">
    <a
      href="/"
      className="dropdown-toggle"
      data-toggle="dropdown"
      role="button"
      aria-haspopup="true"
      aria-expanded="false"
    >
      {props.icon} {props.title} <span className="caret" />
    </a>
    <ul className="dropdown-menu">{props.children}</ul>
  </li>
);

export const Separator = (props) => <li role="separator" className="divider" />;

/*
Example:
<Navbar title='Oursci Webinterface'>
  <NavbarNav>
    <ItemLink to='/projects'>Projects</ItemLink>
  </NavbarNav>
  <NavbarNav alignment='right'>
    <Dropdown title='More'>
      <ItemLink to='/resources/open'>Open</ItemLink>
      <ItemLink to='/resources/client'>Client</ItemLink>
      <ItemLink to='/resources/admin'>Admin</ItemLink>
      <Separator />
      <ItemLink to='/' onClick={this.props.logoutUser}>Sign out</ItemLink>
    </Dropdown>
  </NavbarNav>
</Navbar>
*/
const Navbar = (props) => (
  <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <Fluid>
      {/* Brand and toggle get grouped for better mobile display */}
      <BrandToggle>{props.title}</BrandToggle>

      {/* Collect the nav links, forms, and other content for toggling */}
      <Collapse>{props.children}</Collapse>
      {/* .navbar-collapse */}
    </Fluid>
    {/* .container-fluid */}
  </nav>
);

export default Navbar;

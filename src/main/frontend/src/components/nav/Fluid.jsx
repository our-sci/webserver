import React from 'react';
import PropTypes from 'prop-types';

const Fluid = (props) => {
  return (
    <div className="container-fluid">
      {props.children}
    </div>
  );
};

Fluid.propTypes = {
  children: PropTypes.node,
};

Fluid.defaultProps = {
  children: '',
};

export default Fluid;

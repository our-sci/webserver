import React from 'react';
import PropTypes from 'prop-types';

const Collapse = (props) => (
  <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    {props.children}
  </div>
);

Collapse.propTypes = {
  children: PropTypes.node,
};

Collapse.defaultProps = {
  children: 'Collapsible Item',
};

export default Collapse;

import React from 'react';
import PropTypes from 'prop-types';

const DownloadButton = ({ href, children, className }) => (
  <a className={`btn btn-outline-primary ${className}`} href={href} role="button">
    <i className="fas fa-file-archive mr-2" />
    {children}
  </a>
);

DownloadButton.propTypes = {
  href: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
};

DownloadButton.defaultProps = {
  href: '',
  children: '',
  className: '',
};

export default DownloadButton;

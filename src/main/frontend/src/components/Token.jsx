import React, { Component } from 'react';
import { connect } from 'react-redux';

class Token extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showToken: false,
    };
  }

  renderToken = () => {
    const { token } = this.props.auth;
    const { showToken } = this.state;

    if (!token) {
      return null;
    }

    if (showToken) {
      return (
        <div>
          <a
            href="/token/hide"
            onClick={(ev) => {
              ev.preventDefault();
              this.setState({ showToken: false });
            }}
          >
            Hide token
          </a>
          <br />
          <textarea value={token} cols="100" rows="10" readOnly />
        </div>
      );
    }

    return (
      <div>
        <a
          href="/token/show"
          onClick={(ev) => {
            ev.preventDefault();
            this.setState({ showToken: true });
          }}
        >
          Show token
        </a>
      </div>
    );
  };

  render() {
    return this.renderToken();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Token);

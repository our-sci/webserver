import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import _ from 'lodash';
import moment from 'moment';

import ModalOrLink from '../ModalOrLink';

import {
  createInvitation,
  deleteInvitation,
  resendInvitation,
  showInvitationCode,
  getOrganizationById,
} from '../../actions/api';

const FORMAT = 'DD MMM YYYY, HH:mm';
const UNKNOWN = '---';

class Invitations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emails: '',
      showDeletion: null,
      showCode: null,
      showEmail: null,
      code: '',
      subject: '',
      body: '',
    };
  }

  refresh = (ev) => {
    ev.preventDefault();
    const { organizationId } = this.props.organization;
    this.props.getOrganizationById(organizationId);
  };

  deleteInvitation = (id) => {
    const { organization } = this.props;
    const { organizationId } = organization;
    this.props.deleteInvitation(organizationId, id);
  };

  formatDate = (date) => (date ? moment(date).format(FORMAT) : UNKNOWN);

  getDefaultEmail = (invitation, code) => ({
    subject: `Invitation to organization ${invitation.organization.name}`,
    body: `Hi,\n\nYou are invited to join organization "${
      invitation.organization.name
    }" on the Our-Sci platform.\n\nYour invitation code is:\n${code}\n\nPlease use the following link to sign up:\nhttps://app.our-sci.net/#/login/invitation/${code}\n\nAll the best`,
  });

  getCodeForEmail = (id, invitation) => {
    this.props
      .showInvitationCode(id)
      .then((response) => {
        const code = response.data;
        const defaultEmail = this.getDefaultEmail(invitation, code);
        this.setState({
          showEmail: id,
          code,
          body: defaultEmail.body,
          subject: defaultEmail.subject,
        });
      })
      .catch((err) => console.log(err));
  };

  getCode = (id) => {
    this.props
      .showInvitationCode(id)
      .then((response) => this.setState({ showCode: id, code: response.data }))
      .catch((err) => console.log(err));
  };

  sendEmail = (id, subject, body) => {
    this.props.resendInvitation(id, subject, body).then(() => this.setState({ showEmail: null }));
  };

  renderShowEmailLink = (invitation) => {
    const { showEmail, subject, body } = this.state;
    const { id, email, status } = invitation;

    if (!email || status === 'ACCEPTED') return null;

    return (
      <ModalOrLink
        showModal={showEmail === id}
        onHide={() => this.setState({ showEmail: null })}
        title={`Resend email to ${email}`}
        onConfirmClicked={() => this.sendEmail(id, subject, body)}
        confirmText="Resend"
        severity="danger"
        onLinkClicked={() => this.getCodeForEmail(id, invitation)}
        link={<i className="far fa-envelope fa-lg" title="Resend email..." />}
        className="mr-2"
      >
        <form>
          <div className="form-group">
            <label htmlFor="subject">Subject</label>
            <input
              id="subject"
              className="form-control"
              value={subject}
              onChange={(ev) => this.setState({ subject: ev.target.value })}
            />
          </div>
          <div className="form-group">
            <label htmlFor="message">Message</label>
            <textarea
              id="message"
              className="form-control"
              rows="15"
              onChange={(ev) => this.setState({ body: ev.target.value })}
              value={body}
            />
          </div>
        </form>
      </ModalOrLink>
    );
  };

  renderShowCodeLink = (invitation) => {
    const { showCode, code } = this.state;
    const { id } = invitation;

    return (
      <ModalOrLink
        showModal={showCode === id}
        onHide={() => this.setState({ showCode: null })}
        title="Super Secret Invitation Code (SSIC)"
        onLinkClicked={() => this.getCode(id)}
        link={<i className="far fa-eye fa-lg" title="View code..." />}
        className="mr-2"
      >
        {code}
      </ModalOrLink>
    );
  };

  renderDeletionLink = (invitation) => {
    const { showDeletion } = this.state;
    const { id, claimed } = invitation;

    const body = claimed ? (
      <div>
        This invitation has been claimed on {this.formatDate(claimed)}, and is safe to delete.
      </div>
    ) : (
      <div>
        This invitation has <strong>not been claimed</strong>, are you sure you want to delete it
        regardless?
      </div>
    );

    return (
      <ModalOrLink
        showModal={showDeletion === id}
        onHide={() => this.setState({ showDeletion: null })}
        onConfirmClicked={() => this.deleteInvitation(id)}
        confirmText="Delete"
        severity="danger"
        title="Confirm your action"
        onLinkClicked={() => this.setState({ showDeletion: id })}
        link={<i className="far fa-trash-alt fa-lg" title="Delete..." />}
      >
        {body}
      </ModalOrLink>
    );
  };

  renderClaimedBy = (invitation) => {
    if (!invitation.claimed || !invitation.member) return UNKNOWN;

    const { id, user } = invitation.member;
    const { organizationId } = invitation.organization;
    return (
      <div>
        <Link to={`/organization/${organizationId}/members/edit?id=${id}`}>{user.email}</Link>
      </div>
    );
  };

  getEmails = () => {
    const { emails } = this.state;

    const array = [];
    _.split(emails, /\r?\n/).forEach((line) => {
      const splits = _.split(line, ';');
      splits.forEach((split) => {
        const email = _.trim(split);
        if (email.length !== 0) {
          array.push(email);
        }
      });
    });

    return array;
  };

  getEmailsString = () => _.join(this.getEmails(), ';');

  renderInvitationText = () => {
    const n = this.getEmails().length;
    if (n === 0 || n === 1) {
      return 'Create 1 Invitation';
    }

    return `Create ${n} Invitations`;
  };

  render = () => {
    const { organization } = this.props;
    const { organizationId } = organization;
    const { emails } = this.state;

    return (
      <div>
        <h4>
          <label htmlFor="invitation-email">Create new</label>
        </h4>

        <form>
          <div className="form-group">
            <textarea
              placeholder="email addresses..."
              id="invitation-email"
              aria-label="email"
              className="form-control"
              value={emails}
              onChange={(ev) => this.setState({ emails: ev.target.value })}
            />
            <small className="ml-1">
              May be left blank. Use semicolons or line breaks to separate multiple addresses.
            </small>
          </div>
          <div className="d-flex flex-row-reverse">
            <button
              className="btn btn-outline-primary"
              type="button"
              onClick={() => this.props.createInvitation(organizationId, this.getEmailsString())}
            >
              <i className="fas fa-user-plus mr-1" />
              {this.renderInvitationText()}
            </button>
          </div>
        </form>

        <h4 className="mt-5">
          Invitations{' '}
          <Link
            className="text-secondary float-right"
            to="/refresh"
            onClick={(ev) => this.refresh(ev)}
          >
            <i className="fas fa-sync-alt" />
          </Link>
        </h4>

        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>Status</th>
              <th>Issued</th>
              <th>Sent to</th>
              <th>Claimed by</th>
              <th style={{ width: '10em' }} className="text-right">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {_.map(organization.invitations, (invitation) => (
              <tr key={invitation.id}>
                <td
                  className={`text-${invitation.status === 'ACCEPTED' ? 'success' : 'secondary'}`}
                >
                  {invitation.status}
                </td>
                <td>{this.formatDate(invitation.issued)}</td>
                <td>{invitation.email || UNKNOWN}</td>
                <td>{this.renderClaimedBy(invitation)}</td>
                <td className="text-right">
                  {this.renderShowEmailLink(invitation)}
                  {this.renderShowCodeLink(invitation)}
                  {this.renderDeletionLink(invitation)}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  };
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  createInvitation,
  deleteInvitation,
  resendInvitation,
  showInvitationCode,
  getOrganizationById,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Invitations);

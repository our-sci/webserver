import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import C from '../../constants';
import { basename } from '../../actions/utils';

import {
  getOrganizationById,
  deleteOrganizationAsset,
  getOrganizationAssets,
} from '../../actions/api';

import ModalOrLink from '../ModalOrLink';
import Uploader from '../Uploader';

class Assets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      assetConfirmDeletion: null,
      assetConfirmInsertion: null,
    };
  }

  componentDidMount() {
    const { organizationId } = this.props;
    this.props.getOrganizationAssets(organizationId);
  }

  onAssetsUploaded = () => {
    const { organizationId } = this.props;
    this.props.getOrganizationAssets(organizationId);
  };

  removeAsset = (name) => {
    const { organizationId } = this.props;
    this.props.deleteOrganizationAsset(organizationId, name);
    this.setState({ assetConfirmDeletion: null });
  };

  renderAssetInsertionLink = (asset) => {
    const { assetConfirmInsertion } = this.state;
    const name = basename(asset);
    const standardMarkdown = `![${name}](${asset})`;
    const htmlMarkdown = `<img src="${asset}" alt="${name}" width="200" />`;

    const title = (
      <span>
        Copy the following content, and paste into <strong>Content</strong>
      </span>
    );

    return (
      <ModalOrLink
        showModal={assetConfirmInsertion === asset}
        onHide={() => this.setState({ assetConfirmInsertion: null })}
        title={title}
        onLinkClicked={() => this.setState({ assetConfirmInsertion: asset })}
        link={<span className="fas fa-paperclip fa-lg" />}
      >
        <p>
          <span className="small">{standardMarkdown}</span>
        </p>
        <p className="text-center">OR</p>
        <p>
          <span className="small">{htmlMarkdown}</span>
        </p>
      </ModalOrLink>
    );
  };

  renderAssetDeletionLink = (asset) => {
    const { assetConfirmDeletion } = this.state;
    const name = basename(asset);

    return (
      <ModalOrLink
        showModal={assetConfirmDeletion === asset}
        onHide={() => this.setState({ assetConfirmDeletion: null })}
        onConfirmClicked={() => this.removeAsset(asset)}
        confirmText="Delete"
        severity="danger"
        title="Confirm your action"
        onLinkClicked={() => this.setState({ assetConfirmDeletion: asset })}
        link="Delete"
      >
        Do you want to delete asset <strong>{name}</strong>?
      </ModalOrLink>
    );
  };

  renderAssets = () => {
    const { assets } = this.props.organizations;

    if (assets.status !== C.STATUS_SUCCESS) {
      return '...';
    }

    if (assets.data.length === 0) {
      return 'No assets uploaded yet...';
    }

    return (
      <table className="table">
        <thead>
          <tr className="border-top-0">
            <th />
            <th />
            <th />
          </tr>
        </thead>
        <tbody>
          {_.map(assets.data, (asset, index) => {
            const name = decodeURIComponent(asset.split('/').pop());

            return (
              <tr key={index}>
                <td className="align-middle">{this.renderAssetInsertionLink(asset, name)}</td>
                <td>
                  <a href={asset} target="_blank" rel="noopener noreferrer">
                    {name}
                  </a>
                </td>
                <td style={{ verticalAlign: 'middle' }}>{this.renderAssetDeletionLink(name)}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  };

  renderUploader = () => {
    const { organizationId } = this.props;

    return (
      <div>
        <h4>Create an Asset</h4>
        <Uploader
          name="Asset"
          url={`/api/organization/id/${organizationId}/assets`}
          onSuccess={this.onAssetsUploaded}
          showTitle={false}
        />
      </div>
    );
  };

  render = () => (
    <div className="row">
      <div className="col-md-6">
        <h3>Available Assets</h3>
        {this.renderAssets()}
      </div>
      <div className="col-md-6">{this.renderUploader()}</div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  getOrganizationById,
  getOrganizationAssets,
  deleteOrganizationAsset,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Assets);

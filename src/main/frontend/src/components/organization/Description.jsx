import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { post } from 'axios';

import { Button } from 'reactstrap';

import { getOrganizationById } from '../../actions/api';

import { basename } from '../../actions/utils';

class Description extends Component {
  constructor(props) {
    super(props);
    console.log('Description: construced');

    const { organizationId, name, description, icon, content, archived } = props.organization;

    this.state = {
      name,
      description,
      organizationId,
      content,
      icon, // URL to the uploaded icon
      iconFile: null, // local file handle for upload
      clearIcon: false,
      archived,
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log('Description: got new props');
    const { organization } = nextProps;
    if (organization !== null) {
      const { organizationId, name, description, icon, content, archived } = organization;
      this.setState({ organizationId, name, description, icon, content, archived });
    }
  }

  navigateBack = () => {
    this.props.history.push(`/organization/${this.state.organizationId}`);
  };

  updateOrganization = ({
    organizationId,
    name,
    icon,
    description,
    content,
    clearIcon,
    archived,
  }) => {
    const { token } = this.props.auth;

    const formData = new FormData();

    if (organizationId !== undefined) formData.append('id', organizationId);
    if (name !== undefined) formData.append('name', name);
    if (icon !== undefined) formData.append('icon', icon);
    if (description !== undefined) formData.append('description', description);
    if (content !== undefined) formData.append('content', content);
    if (archived !== undefined) formData.append('archived', archived);
    if (clearIcon !== undefined) formData.append('clearIcon', clearIcon);

    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
        'X-Authorization-Firebase': token,
      },
    };

    return post('/api/organization/update', formData, config);
  };

  updateOrganizationFromForm = () => {
    const {
      organizationId,
      name,
      description,
      iconFile,
      content,
      clearIcon,
      archived,
    } = this.state;
    this.updateOrganization({
      organizationId,
      name,
      description,
      icon: iconFile,
      content,
      clearIcon,
      archived,
    })
      .then(this.navigateBack)
      .catch((error) => console.log(error));
  };

  render = () => {
    const { name, description, icon, content, clearIcon } = this.state;
    const iconName = basename(icon);

    return (
      <div>
        <form>
          <div className="form-group">
            <label htmlFor="name">Organization Name</label>
            <input
              className="form-control"
              id="name"
              value={name}
              onChange={(ev) => this.setState({ name: ev.target.value })}
            />
          </div>

          <div className="form-group">
            <label htmlFor="description">Description</label>
            <input
              className="form-control"
              id="description"
              value={description}
              onChange={(ev) => this.setState({ description: ev.target.value })}
            />
          </div>

          <div className="form-row align-items-end">
            <div className="col">
              <div className="form-group">
                <label htmlFor="iconfile">
                  Icon{' '}
                  {icon && (
                    <a href={icon} target="_blank" rel="noopener noreferrer">
                      {iconName}
                    </a>
                  )}
                </label>
                <input
                  type="file"
                  className="form-control-file"
                  id="iconfile"
                  onChange={(ev) => this.setState({ iconFile: ev.target.files[0], icon: null })}
                />
              </div>
            </div>
            <div className="col-auto">
              <div className="form-check mb-3">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="remove"
                  checked={clearIcon}
                  onChange={(ev) => this.setState({ clearIcon: ev.target.checked })}
                />
                <label htmlFor="remove" className="form-check-label">
                  Clear
                </label>
              </div>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="content">Content</label>
            <textarea
              className="form-control"
              id="content"
              rows="20"
              value={content}
              onChange={(ev) => this.setState({ content: ev.target.value })}
            />
            <small>
              You can use{' '}
              <a
                href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet"
                target="_blank"
                rel="noopener noreferrer"
              >
                markdown
              </a>
            </small>
          </div>
        </form>
        <div className="btn-toolbar float-right">
          <Button outline color="secondary" onClick={this.navigateBack} className="mr-1">
            Cancel
          </Button>
          <Button color="primary" onClick={this.updateOrganizationFromForm}>
            Update
          </Button>
        </div>
      </div>
    );
  };
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  organizations: state.organizations,
});

const mapDispatchToProps = {
  getOrganizationById,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Description)
);

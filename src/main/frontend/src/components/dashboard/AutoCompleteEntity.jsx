import React, { Component } from 'react';
import axios from 'axios';
import moment from 'moment';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';

class AutoCompleteEntity extends Component {
  constructor(props) {
    super(props);
    this.state = { options: [], isLoading: false, selected: [], identifier: '' };
  }

  handleOption = (option) => {
    this.setState({
      selected: option,
      identifier: this.getIdentifier(option[0]),
    });
    this.props.onSelect(option[0]);
  };

  handleSearch = (query) => {
    this.setState({ isLoading: true });
    axios({
      url: `/api/${this.props.type}/find?search=${encodeURIComponent(query)}`,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      this.setState({ options: response.data.content, isLoading: false });
    });
  };

  getIdentifier = (option) => {
    if (!option) return '';

    switch (this.props.type) {
      case 'survey':
        return option.formId;
      case 'dashboard':
        return option.dashboardId;
      case 'script':
        return option.scriptId;
      default:
        return option.id;
    }
  };

  getLabelKey = (option) => {
    const DELIMITER = ';';

    switch (this.props.type) {
      case 'survey': {
        const label = option.formTitle;
        if (label.includes(DELIMITER)) {
          return label.substring(0, label.indexOf(DELIMITER));
        }
        return label;
      }
      case 'dashboard': {
        return option.name;
      }
      case 'script': {
        return option.name;
      }
      default:
        return option.label;
    }
  };

  renderMenuTitle = (option) => {
    const DELIMITER = ';';

    let title = '';
    let identifier = '';

    switch (this.props.type) {
      case 'survey':
        title = option.formTitle;
        identifier = option.formId;
        break;
      case 'dashboard':
        title = option.name;
        identifier = option.dashboardId;
        break;
      case 'script':
        title = option.name;
        identifier = option.scriptId;
        break;
      default:
        title = 'Subtitle';
        identifier = 'identifier';
    }

    if (title.includes(DELIMITER)) {
      const mainTitle = title.substring(0, title.indexOf(DELIMITER));
      const subTitle = title.substring(title.indexOf(DELIMITER) + 1);
      return (
        <div style={{ marginBottom: 5 }}>
          {mainTitle}
          <br />
          <span style={{ fontSize: '95%' }}>{subTitle}</span>
          <br />
          <small>{identifier}</small>
        </div>
      );
    }

    return (
      <div style={{ marginBottom: 5 }}>
        {title}
        <br />
        <small>{identifier}</small>
      </div>
    );
  };

  renderMenuItems = (option, props) => (
    <div key={option.id}>
      <span className="float-right">
        <small>{moment(option.date).format('YYYY-MM-DD, HH:mm')}</small>
      </span>
      {this.renderMenuTitle(option)}
    </div>
  );

  render() {
    return (
      <div className="form-group">
        <label>{this.props.type[0].toUpperCase() + this.props.type.slice(1)}</label>
        <AsyncTypeahead
          filterBy={['name', 'dashboardId']}
          onChange={this.handleOption}
          selected={this.state.selected}
          labelKey={this.getLabelKey}
          onSearch={this.handleSearch}
          isLoading={this.state.isLoading}
          placeholder={`Choose ${this.props.type}...`}
          options={this.state.options}
          renderMenuItemChildren={this.renderMenuItems}
          minLength={1}
        />
        <small>{this.state.identifier}</small>
      </div>
    );
  }
}

AutoCompleteEntity.defaultProps = {
  type: 'dashboard',
  onSelect: (option) => console.log('selected entity'),
};

export default AutoCompleteEntity;

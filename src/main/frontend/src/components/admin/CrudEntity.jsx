import React, { Component } from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';

import { isAdmin } from '../../actions/api';

class BrowsableEntity extends Component {
  constructor(props) {
    super(props);
    this.state = { search: '', page: 0 };
  }

  componentDidMount() {
    const { size } = this.props;
    const { search } = this.state;
    this.props.find({ search, size });
  }

  handleSearchTermChange = (search) => {
    const { size } = this.props;
    this.setState({ search, page: 0 });
    this.props.find({ search, page: 0, size });
  };

  handlePaginationChange = (ev, page) => {
    ev.preventDefault();
    const { size } = this.props;

    const { search } = this.state;
    this.setState({ page });
    this.props.find({ search, page, size });
  };

  renderPaginationBar = () => {
    const { data } = this.props.entity;

    const { totalPages } = data;

    if (totalPages === 1) {
      return null;
    }

    return (
      <ul className="pagination">
        {_.map(_.range(0, totalPages), (index) => (
          <li className={this.state.page === index ? 'page-item active' : 'page-item'} key={index}>
            <a
              className="page-link"
              href={`page/${index + 1}`}
              onClick={(ev) => this.handlePaginationChange(ev, index)}
            >
              {index + 1}
            </a>
          </li>
        ))}
      </ul>
    );
  };

  renderSearchBar = () => {
    const { data } = this.props.entity;
    if (!data) {
      return null;
    }
    const { totalElements } = data;

    return (
      <div className="row">
        <div className="col-sm-4">
          <form>
            <div className="form-group">
              <input
                className="form-control"
                id="search"
                value={this.state.search}
                onChange={(ev) => this.handleSearchTermChange(ev.target.value)}
              />
              <small>{totalElements} total</small>
            </div>
          </form>
        </div>
        <div className="col-sm-4">{this.renderPaginationBar()}</div>
      </div>
    );
  };

  renderList = () => {
    const { status, data } = this.props.entity;
    if (!data) {
      return null;
    }

    return (
      <ul className="list-group">
        {_.map(data.content, (entity) => (
          <li key={entity.id} className="list-group-item">
            {this.props.renderItem(entity)}
          </li>
        ))}
      </ul>
    );
  };

  renderBrowse = () => (
    <div className="mt-3">
      {this.renderSearchBar()}
      {this.renderList()}
    </div>
  );

  render() {
    return (
      <div className={this.props.className}>
        {this.props.title && <h3>{this.props.title}</h3>}
        {this.renderBrowse()}
      </div>
    );
  }
}

BrowsableEntity.defaultProps = {
  size: 20,
  onClick: (ev) => ev,
  title: '',
  find: ({ search }) => console.log(`Trying to find entities for search term: ${search}`),
  renderItem: (e) => `Data set ${e.id} (add renderItem prop)`,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  users: state.users,
});

const mapDispatchToProps = {
  isAdmin,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BrowsableEntity);

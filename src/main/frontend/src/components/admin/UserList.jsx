import React, { Component } from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';

import { findUsers, isAdmin } from '../../actions/api';

class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = { search: '', page: 0 };
  }

  componentDidMount() {
    const { search } = this.state;
    this.props.findUsers({ search });
  }

  handleSearchTermChange = (search) => {
    this.setState({ search, page: 0 });
    this.props.findUsers({ search, page: 0 });
  };

  handlePaginationChange = (ev, page) => {
    ev.preventDefault();
    const { search } = this.state;
    this.setState({ page });
    this.props.findUsers({ search, page });
  };

  renderPaginationBar = () => {
    const { data } = this.props.users.find;

    const { totalPages } = data;

    if (totalPages === 1) {
      return null;
    }

    return (
      <ul className="pagination">
        {_.map(_.range(0, totalPages), (index) => (
          <li className={this.state.page === index ? 'page-item active' : 'page-item'} key={index}>
            <a
              className="page-link"
              href={`page/${index + 1}`}
              onClick={(ev) => this.handlePaginationChange(ev, index)}
            >
              {index + 1}
            </a>
          </li>
        ))}
      </ul>
    );
  };

  renderSearchBar = () => {
    const { data } = this.props.users.find;
    if (!data) {
      return null;
    }
    const { totalElements } = data;

    return (
      <div className="row">
        <div className="col-sm-4">
          <form>
            <div className="form-group">
              <input
                className="form-control"
                id="search"
                value={this.state.search}
                onChange={(ev) => this.handleSearchTermChange(ev.target.value)}
              />
              <small>{totalElements} total</small>
            </div>
          </form>
        </div>
        <div className="col-sm-4">{this.renderPaginationBar()}</div>
      </div>
    );
  };

  renderBrowse = () => (
    <div className="mt-3">
      <h3>Browse Users</h3>
      {this.renderSearchBar()}
      {this.renderList()}
    </div>
  );

  renderList = () => {
    const { status, data } = this.props.users.find;
    if (!data) {
      return null;
    }

    return (
      <ul className="list-group">
        {_.map(data.content, (user) => {
          const { id, username, email, displayName, picture } = user;
          return (
            <li key={id} className="list-group-item">
              {`"${displayName || email}" <${email}>`}
            </li>
          );
        })}
      </ul>
    );
  };

  render() {
    return (
      <div>
        <h3>User List</h3>
        {this.renderBrowse()}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  users: state.users,
});

const mapDispatchToProps = {
  isAdmin,
  findUsers,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);

import React, { Component } from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';

import C from '../../constants';
import { getTpCredentialList, isAdmin } from '../../actions/api';

class TpCredentialList extends Component {
  constructor(props) {
    super(props);
    this.state = { isAdmin: false };
  }

  componentDidMount() {
    this.props.getTpCredentialList();
  }

  renderList = () => {
    const { data } = this.props.tpcredential.list;

    return (
      <ul>
        {_.map(data, (tcp) => {
          const { id, url, user } = tcp;
          return (
            <li key={id}>
              {user.email}
              {url}
            </li>
          );
        })}
      </ul>
    );
  };

  render() {
    const { status } = this.props.tpcredential.list;
    if (status !== C.STATUS_SUCCESS) {
      return <div>Loading...</div>;
    }

    return this.renderList();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  tpcredential: state.tpcredential,
});

const mapDispatchToProps = {
  isAdmin,
  getTpCredentialList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TpCredentialList);

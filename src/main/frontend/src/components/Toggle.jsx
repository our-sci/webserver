import React from 'react';
import PropTypes from 'prop-types';

const Toggle = ({ checked, onToggle, textChecked, textUnchecked, className }) => {
  if (checked) {
    return (
      <button className={`btn btn-outline-secondary ${className}`} type="button" onClick={onToggle}>
        <i className="far fa-eye-slash mr-2" />
        {textChecked}
      </button>
    );
  }

  return (
    <button className={`btn btn-outline-secondary ${className}`} type="button" onClick={onToggle}>
      <i className="far fa-eye mr-2" />
      {textUnchecked}
    </button>
  );
};

Toggle.propTypes = {
  checked: PropTypes.bool.isRequired,
  onToggle: PropTypes.func.isRequired,
  textChecked: PropTypes.string,
  textUnchecked: PropTypes.string,
  className: PropTypes.string,
};

Toggle.defaultProps = {
  textChecked: 'toggle checked',
  textUnchecked: 'toggle un-checked',
  className: '',
};

export default Toggle;

import React from 'react';

const Spacer = (props) => <div style={{ height: props.height }} />;

Spacer.defaultProps = {
  height: 30,
};

export default Spacer;

import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { dismissFeedback } from '../actions/feedback';

const Feedback = (props) => {
  const rows = props.feedback.map((f, i) => (
    <div key={i} className={`alert alert-dismissable alert-${f.type}`} role="alert">
      <button type="button" className="close ml-2" onClick={() => props.dismissFeedback(i)}>
        <span aria-hidden="true">&times;</span>
      </button>
      <strong>{f.title}</strong>
      <br />
      {f.message}
    </div>
  ));
  return <div style={{ display: 'inline-block', maxWidth: '40rem' }}>{rows}</div>;
};

Feedback.propTypes = {
  feedback: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const mapStateToProps = (state) => ({ feedback: state.feedback });

const mapDispatchToProps = { dismissFeedback };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Feedback);

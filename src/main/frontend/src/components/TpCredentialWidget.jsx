import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

import { getTpCredential, postTpCredential, deleteTpCredential } from '../actions/api';

import ModalOrLink from './ModalOrLink';

const TP_CREDENTIAL_TYPES = ['FARMOS', 'FARMOS_AGGREGATOR'];

class TpCredentialWidget extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.id,
      user: { id: this.props.uid },
      url: '',
      username: '',
      password: '',
      token: '',
      parameters: '',
      note: '',
      type: 'FARMOS',
      confirmDeletion: false,
      error: {},
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    const { mode } = this.props;
    const { id } = this.state;
    if (mode === 'EDIT') {
      this.props
        .getTpCredential(id)
        .then((response) => {
          this.setState({ ...response.data });
        })
        .catch(() => {});
    } else {
      const { uid } = this.props;

      axios({
        url: `/api/profile/uid/${uid}`,
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          'X-Authorization-Firebase': this.props.auth.token,
        },
      })
        .then((response) => this.setState({ user: response.data }))
        .catch((error) => console.log(error));
    }
  };

  fetchUser = () => {
    const { uid } = this.props;

    axios({
      url: `/api/profile/uid/${uid}`,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'X-Authorization-Firebase': this.props.auth.token,
      },
    })
      .then((response) => this.setState({ user: response.data }))
      .catch((error) => console.log(error));
  };

  navigateBack = () => {
    this.props.history.goBack();
  };

  handleDelete = () => {
    this.props
      .deleteTpCredential(this.state)
      .then(() => this.navigateBack())
      .catch((error) => console.log(error));
  };

  handlePost = () => {
    this.props
      .postTpCredential(this.state)
      .then(() => this.navigateBack())
      .catch((error) => console.log(error));
  };

  handleAdd = () => {
    if (this.validate()) {
      this.handlePost();
    }
  };

  handleUpdate = () => {
    if (this.validate()) {
      this.handlePost();
    }
  };

  validate = () => {
    const { url } = this.state;

    const prevError = this.state.error;
    if (url && !url.startsWith('https://') && !url.startsWith('http://')) {
      this.setState({
        error: {
          ...prevError,
          url: `Please provide the full URL, e.g. https://example.farmos.net`,
        },
      });
      return false;
    }

    return true;
  };

  handleUrlChange = (ev) => {
    const url = ev.target.value;
    this.setState({ url });

    const prevError = this.state.error;
    if (url && url.startsWith('http')) {
      this.setState({ error: { ...prevError, url: null } });
    }
  };

  renderSubmitButton = () => {
    if (this.props.mode === 'EDIT') {
      return (
        <button type="button" className="btn btn-primary" onClick={() => this.handleUpdate()}>
          Update
        </button>
      );
    }

    return (
      <button type="button" className="btn btn-primary" onClick={() => this.handleAdd()}>
        Submit
      </button>
    );
  };

  renderDeleteButton = () => {
    const { confirmDeletion, id, type, url } = this.state;

    if (this.props.mode === 'EDIT') {
      return (
        <ModalOrLink
          showModal={confirmDeletion}
          onHide={() => this.setState({ confirmDeletion: false })}
          onConfirmClicked={this.handleDelete}
          confirmText="Delete"
          severity="danger"
          title="Delete this for sure?"
          onLinkClicked={() => this.setState({ confirmDeletion: true })}
          link={<i className="far fa-trash-alt fa-lg" title="Delete..." />}
        >
          <div>
            {id}: {type} {url}
          </div>
        </ModalOrLink>
      );
    }

    return null;
  };

  renderForm = () => {
    const { user, url, username, password, token, parameters, note, type, error } = this.state;

    return (
      <form>
        <div className="btn-toolbar float-right">{this.renderDeleteButton()}</div>
        <h3>{this.props.mode === 'EDIT' ? 'Edit' : 'Add'}</h3>
        <div className="form-group">
          <label htmlFor="type">Type</label>
          <select
            className="form-control"
            id="type"
            onChange={(ev) => this.setState({ type: ev.target.value })}
            value={type}
          >
            {TP_CREDENTIAL_TYPES.map((t) => (
              <option value={t}>{t}</option>
            ))}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="user">User</label>
          <input
            disabled
            className="form-control"
            id="user"
            value={`${user.id}: ${user.displayName || user.email} <${user.email}>`}
          />
        </div>
        <div className="form-group">
          <label htmlFor="url">URL</label>
          <input
            className={`form-control ${error.url && 'is-invalid'}`}
            id="url"
            value={url}
            onChange={this.handleUrlChange}
          />
          {error.url && <div className="invalid-feedback">{error.url}</div>}
        </div>
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <input
            className="form-control"
            id="username"
            value={username}
            onChange={(ev) => this.setState({ username: ev.target.value })}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            className="form-control"
            id="password"
            value={password}
            onChange={(ev) => this.setState({ password: ev.target.value })}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Token</label>
          <input
            className="form-control"
            id="token"
            value={token}
            onChange={(ev) => this.setState({ token: ev.target.value })}
          />
        </div>

        <div className="form-group">
          <label htmlFor="parameters">Parameters</label>
          <input
            className="form-control"
            id="parameters"
            value={parameters}
            onChange={(ev) => this.setState({ parameters: ev.target.value })}
          />
        </div>

        <div className="form-group">
          <label htmlFor="notes">Note</label>
          <input
            className="form-control"
            id="note"
            value={note}
            onChange={(ev) => this.setState({ note: ev.target.value })}
          />
        </div>

        <div className="btn-toolbar float-right">
          <button type="button" className="btn btn-default mr-1" onClick={this.navigateBack}>
            Cancel
          </button>
          {this.renderSubmitButton()}
        </div>
      </form>
    );
  };

  renderEdit = () => <div className="content-medium">{this.renderForm()}</div>;

  render() {
    return this.renderEdit();
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  api: state.api,
});

const mapDispatchToProps = {
  getTpCredential,
  postTpCredential,
  deleteTpCredential,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TpCredentialWidget)
);

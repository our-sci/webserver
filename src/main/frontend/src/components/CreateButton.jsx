import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const CreateButton = ({ to, children }) => (
  <Link to={to} className="btn btn-primary">
    <i className="fas fa-plus mr-2" />
    {children}
  </Link>
);

CreateButton.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node,
};

CreateButton.defaultProps = {
  children: '',
};

export default CreateButton;

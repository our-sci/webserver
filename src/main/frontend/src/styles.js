export default {
  color: {
    primary: {},
    secondary: {},
    tertiary: {},
    light: { color: '#555' },
    lighter: { color: '#AAA' },
  },
};

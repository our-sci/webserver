package net.oursci.server.mappings;

import org.springframework.data.domain.Page;

import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.organization.Organization;
import net.oursci.server.script.Script;
import net.oursci.server.survey.Survey;

public class QuickAccess {
	Page<Survey> surveys;
	Page<Script> scripts;
	Page<Dashboard> dashboards;
	Page<Organization> organizations;
}

package net.oursci.server.sharecode;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.dashboard.DashboardService;
import net.oursci.server.exception.DashboardNotFound;
import net.oursci.server.exception.GenericNotFound;
import net.oursci.server.exception.SurveyResultNotFound;
import net.oursci.server.mail.MailService;
import net.oursci.server.surveyresult.SurveyResult;
import net.oursci.server.surveyresult.SurveyResultService;
import net.oursci.server.views.Views;

@RestController
@RequestMapping("/api/sharecode")
public class SharecodeController {

	@Autowired
	SharecodeService service;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	DashboardService dashboardService;
	
	@Autowired
	SurveyResultService surveyResultService;
	
	@PostMapping("/email")
	public void sendCodeToEmail(@RequestParam String email) {
		List<Sharecode> shareCodes = service.getByEmail(email);
		if(shareCodes.size() == 0) {
			// no shareCodes found.. exiting
			throw new GenericNotFound("No shares found for email '"+email+"'");
		}
		
		service.sendLink(email);
	}
	
	@PostMapping
	@JsonView(Views.Summary.class)
	public List<Sharecode> createSharecode(@RequestParam String[] instanceIds, @RequestParam String dashboardId, @RequestParam String email, @RequestParam(required = false, defaultValue = "false") boolean sendmail) {
		List<Sharecode> res = new ArrayList<>();
		
		Dashboard dashboard = null;
		try {
			dashboard = dashboardService.findByIdentifier(dashboardId);
		} catch(DashboardNotFound dnf) {
			// that is ok, we do not strictly need a dashboard
		}
		
		for(String instanceId: instanceIds) {
			SurveyResult result = surveyResultService.findByIdentifier(instanceId);
			if(result == null) {
				// no survey result found
				throw new SurveyResultNotFound();
			}
			res.add(service.add(result, dashboard, email));
		}
		
		if(sendmail) {
			service.sendLink(email);
		}
		
		return res;
	}
	
	@JsonView(Views.Summary.class)
	@GetMapping("/results")
	public List<SurveyResult> getSurveyResults(@RequestParam String code) {
		List<String> instanceIds = service.getInstanceIdsByCode(code);
		if(instanceIds.isEmpty()) {
			throw new GenericNotFound("No shares found for code "+code);
		}
		
		List<SurveyResult> results = surveyResultService.findByInstanceId(instanceIds);
		results.sort(Comparator.comparing(SurveyResult::getModified).reversed());
		return results;
	}
	
	
	@GetMapping
	@JsonView(Views.Summary.class)
	public List<Sharecode> getShareCodes(@RequestParam String code) {
		List<Sharecode> sharecodes = service.getByCode(code);
		if(sharecodes.isEmpty()) {
			throw new GenericNotFound("No shares found for code "+code);
		}
		return sharecodes;
	}
	
}

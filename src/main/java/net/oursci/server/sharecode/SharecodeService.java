package net.oursci.server.sharecode;

import java.util.List;

import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.surveyresult.SurveyResult;

public interface SharecodeService {

	Sharecode get(Long id);
	void archive(Long id);
	
	Sharecode add(SurveyResult sr, Dashboard db, String email);
	Sharecode add(SurveyResult sr, Dashboard db, String email, int type);
	
	List<Sharecode> getByCode(String code);
	List<Sharecode> getByEmail(String email);
	List<Sharecode> getBySurveyResult(SurveyResult surveyResult);
	
	Sharecode find(SurveyResult sr, Dashboard dashboard, String email);
	
	String email2code(String email);
	
	List<String> getInstanceIdsByCode(String code);
	
	void sendLink(String email);
	
}

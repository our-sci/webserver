package net.oursci.server.invitation;

import org.springframework.data.repository.PagingAndSortingRepository;

import net.oursci.server.invitation.Invitation;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.member.OrganizationMember;
import net.oursci.server.user.User;

import java.util.List;

public interface InvitationRepository extends PagingAndSortingRepository<Invitation, Long> {

	List<Invitation> findByOrganizationOrderByIssuedDesc(Organization organization);

	List<Invitation> findByOrganizationAndStatus(Organization organization, InvitationStatus status);
	
	List<Invitation> findByMember(OrganizationMember om);
	
	List<Invitation> findByUser(User user);

	Invitation findByCode(String code);

}

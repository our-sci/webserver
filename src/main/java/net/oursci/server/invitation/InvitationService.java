package net.oursci.server.invitation;

import java.util.List;

import net.oursci.server.organization.Organization;
import net.oursci.server.organization.member.OrganizationMember;
import net.oursci.server.user.User;

public interface InvitationService {

	Invitation getById(Long id);
	Invitation findById(Long id);
	List<Invitation> findAll();
	Invitation save(Invitation i);
	void delete(Invitation i);
	
	Invitation findByCode(String code);
	List<Invitation> findByUser(User user);
	List<Invitation> findByMember(OrganizationMember om);
	
	List<Invitation> getAll(Organization organization);
	List<Invitation> getIdle(Organization organization);
	List<Invitation> getDeclined(Organization organization);
	List<Invitation> getAccepted(Organization organization);	
	
}

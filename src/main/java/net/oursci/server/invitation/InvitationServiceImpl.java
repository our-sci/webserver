package net.oursci.server.invitation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.oursci.server.exception.InvitationNotFound;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.member.OrganizationMember;
import net.oursci.server.user.User;

import static net.oursci.server.invitation.InvitationStatus.*;

@Service
public class InvitationServiceImpl implements InvitationService {

	@Autowired
	InvitationRepository repo;

	@Override
	public Invitation getById(Long id) {
		return repo.findById(id).orElseThrow(() -> new InvitationNotFound());
	}
	
	@Override
	public Invitation findById(Long id) {
		return repo.findById(id).orElse(null);
	}
	
	@Override
	public List<Invitation> findAll() {
		return (List<Invitation>) repo.findAll();
	}
	
	@Override
	public Invitation save(Invitation i) {
		return repo.save(i);
	}

	@Override
	public void delete(Invitation i) {
		repo.delete(i);
	}

	@Override
	public Invitation findByCode(String code) {
		return repo.findByCode(code);
	}
	
	@Override
	public List<Invitation> findByUser(User user) {
		return repo.findByUser(user);
	}

	@Override
	public List<Invitation> getAll(Organization organization) {
		return repo.findByOrganizationOrderByIssuedDesc(organization);
	}

	@Override
	public List<Invitation> getIdle(Organization organization) {
		return repo.findByOrganizationAndStatus(organization, IDLE);
	}

	@Override
	public List<Invitation> getDeclined(Organization organization) {
		return repo.findByOrganizationAndStatus(organization, DECLINED);
	}

	@Override
	public List<Invitation> getAccepted(Organization organization) {
		return repo.findByOrganizationAndStatus(organization, ACCEPTED);
	}

	@Override
	public List<Invitation> findByMember(OrganizationMember om) {
		return repo.findByMember(om);
	}

}

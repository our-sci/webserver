package net.oursci.server.firebase;

public interface FirebaseService {

	FirebaseTokenHolder parseToken(String idToken);

	void updateDisplayName(String uid, String displayName);
	void updatePhotoUrl(String uid, String url);

	
}

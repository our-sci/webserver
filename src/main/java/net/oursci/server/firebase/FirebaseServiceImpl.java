package net.oursci.server.firebase;

import org.springframework.stereotype.Service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord.UpdateRequest;

import net.oursci.server.spring.conditionals.FirebaseCondition;



@Service
@FirebaseCondition
public class FirebaseServiceImpl implements FirebaseService {
	@Override
	public FirebaseTokenHolder parseToken(String firebaseToken) {
		return new FirebaseParser().parseToken(firebaseToken);
	}

	@Override
	public void updateDisplayName(String uid, String displayName) {
		try {
			UpdateRequest req = new UpdateRequest(uid).setDisplayName(displayName);
			FirebaseAuth.getInstance().updateUser(req);
			System.out.println("Successfully updated "+uid+" with new displayName "+displayName);
		} catch (FirebaseAuthException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void updatePhotoUrl(String uid, String url) {
		try {
			UpdateRequest req = new UpdateRequest(uid).setPhotoUrl(url);
			FirebaseAuth.getInstance().updateUser(req);
			System.out.println("Successfully updated "+uid+" with new photoUrl "+url);
		} catch (FirebaseAuthException e) {
			e.printStackTrace();
		}
	}
}

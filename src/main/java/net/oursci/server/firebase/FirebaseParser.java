package net.oursci.server.firebase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;

import net.oursci.server.exception.FirebaseTokenInvalidException;
import net.oursci.server.util.StringUtil;



public class FirebaseParser {
	public FirebaseTokenHolder parseToken(String idToken) {
		if (StringUtil.isBlank(idToken)) {
			throw new IllegalArgumentException("FirebaseTokenBlank");
		}
		try {
			FirebaseToken firebaseToken = FirebaseAuth.getInstance().verifyIdToken(idToken);
			return new FirebaseTokenHolder(firebaseToken);
		} catch (Exception e) {
			throw new FirebaseTokenInvalidException(e.getMessage());
		}
	}
}

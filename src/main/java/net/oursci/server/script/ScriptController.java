package net.oursci.server.script;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.javarosa.core.util.externalizable.DeserializationException;
import org.javarosa.xform.parse.XFormParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.oursci.server.config.SecurityConfig.Roles;
import net.oursci.server.exception.ScriptExistsException;
import net.oursci.server.exception.ScriptManifestNotFound;
import net.oursci.server.exception.ScriptNotFound;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.OrganizationService;
import net.oursci.server.s3.S3Wrapper;
import net.oursci.server.survey.SurveyRepository;
import net.oursci.server.util.OursciUtils;
import net.oursci.server.util.StringUtil;

@RestController
@RequestMapping(value = "/api/script")
public class ScriptController {

	@Autowired
	S3Wrapper s3Wrapper;

	@Autowired
	SurveyRepository surveyRepository;
	
	@Autowired
	ScriptRepository scriptRepository;

	@Autowired
	OursciUtils oursciUtils;
	
	@Autowired
	ScriptService scriptService;
	
	@Autowired
	OrganizationService organizationService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<Script> getSurveys() {
		return (List<Script>) scriptRepository.findAll();
	}
	
	@RequestMapping(value = "/by-script-id/{scriptId:.+}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Script getScriptById(@PathVariable("scriptId") String scriptId) {
		System.out.println("scriptId = "+scriptId);
		return scriptRepository.findByScriptId(scriptId);
	}
	
	@RequestMapping(value = "/by-database-id/{id}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Script getScriptById(@PathVariable("id") Long id) {
		return scriptService.findOne(id);
	}
	
	@RequestMapping(value = "/toggle/archived/by-script-id/{scriptId:.+}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Script toggleArchived(@PathVariable("scriptId") String scriptId) {
		Script script = scriptRepository.findByScriptId(scriptId);
		script.setArchived(!script.isArchived());
		return scriptRepository.save(script);
	}

	@Secured(value = {Roles.ROLE_ADMIN, Roles.ROLE_USER})
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	public Script create(@RequestParam MultipartFile[] files,
			@RequestParam(required = false, defaultValue = "false") Boolean override)
			throws XFormParseException, IOException, DeserializationException, InterruptedException {

		String scriptId = null;
		String scriptName = null;
		String scriptDescription = null;
		String scriptVersion = null;
		
		for (MultipartFile file : files) {
			System.out.println(file.getOriginalFilename());
			if (file.getOriginalFilename().equalsIgnoreCase("manifest.json")) {
				
				JSONObject json = OursciUtils.getJsonObject(file.getInputStream());
				try {
					scriptId = json.getString("id");
					scriptName = json.getString("name");
					scriptDescription = json.getString("description");
					scriptVersion = json.getString("version");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				break;
			}
		}

		if (scriptId == null) {
			throw new ScriptManifestNotFound("No manifest.json found in uploaded files, or script id can not be determined");
		}
		
		Script scriptEntity = scriptRepository.findByScriptId(scriptId);
		if(scriptEntity != null) {
			if(override == false){
				throw new ScriptExistsException("A script with id "+scriptId+" already exists.");
			} else {
				// TODO: delete any existing files
			}
		}
		
		String prefix = OursciUtils.getScriptDefinitionPrefix(scriptId);
		s3Wrapper.upload(files, prefix);

		if(scriptEntity == null) {
			scriptEntity = new Script();
		}
		
		scriptEntity.setDate(new Date());
		scriptEntity.setScriptId(scriptId);
		scriptEntity.setName(scriptName);
		scriptEntity.setDescription(scriptDescription);
		scriptEntity.setVersion(scriptVersion);
		scriptEntity.setUser(oursciUtils.getCurrentUser());
		scriptEntity.setArchived(false);

		return scriptRepository.save(scriptEntity);
	}
	
	@RequestMapping(value = "/download/listing/by-script-id/{scriptId:.+}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<String> getDownloadListByScriptId(@PathVariable("scriptId") String scriptId) {
		
		Script script = scriptRepository.findByScriptId(scriptId);
		if(script == null) {
			throw new ScriptNotFound("Script does not exist: "+scriptId);
		}
		
		return getDownloadList(script);
	}
	
	@RequestMapping(value = "/download/listing/by-database-id/{id}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<String> getDownloadListByDatabaseId(@PathVariable("id") Long id) {
		Script script = scriptService.findOne(id);
		return getDownloadList(script);
	}
	
	private List<String> getDownloadList(Script script) {
		List<String> downloadLinks = new ArrayList<>();
		
		String prefix = OursciUtils.getScriptDefinitionPrefix(script.getScriptId());
		List<String> keys = s3Wrapper.getKeys(prefix);
		
		for(String key : keys) {
			downloadLinks.add(s3Wrapper.getSignedDownloadUrl(key).toString());
		}
		
		return downloadLinks;
	} 
	
	@RequestMapping(value = "/download/zip/by-script-id/{scriptId:.+}", produces="application/zip")
    public byte[] downloadZipByScriptId(@PathVariable("scriptId") String scriptId, HttpServletResponse response) throws IOException{
		Script script = scriptRepository.findByScriptId(scriptId);
		if(script == null) {
			throw new ScriptNotFound("Script does not exist: "+scriptId);
		}
		
		return downloadZip(script, response);
    }
	
	@RequestMapping(value = "/download/zip/by-database-id/{id}", produces="application/zip")
    public byte[] downloadScriptByDatabaseId(@PathVariable("id") Long id, HttpServletResponse response) throws IOException{
		Script script = scriptService.findOne(id);
		return downloadZip(script, response);
    }
	
	private byte[] downloadZip(Script script, HttpServletResponse response) throws IOException {
        //setting headers
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-Disposition", String.format("attachment; filename=\"%s.zip\"", script.getScriptId()));

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
        ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);

        String prefix = OursciUtils.getScriptDefinitionPrefix(script.getScriptId());
		List<String> keys = s3Wrapper.getKeys(prefix);
        
        //packing files
        for (String key	: keys) {
            //new zip entry and copying inputstream with file to zipOutputStream, after all closing streams
        	String fileName = StringUtil.getBasename(key);
            zipOutputStream.putNextEntry(new ZipEntry(fileName));
            InputStream inputStream = s3Wrapper.get(key);

            IOUtils.copy(inputStream, zipOutputStream);

            inputStream.close();
            zipOutputStream.closeEntry();
        }

        if (zipOutputStream != null) {
            zipOutputStream.finish();
            zipOutputStream.flush();
            IOUtils.closeQuietly(zipOutputStream);
        }
        IOUtils.closeQuietly(bufferedOutputStream);
        IOUtils.closeQuietly(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
	}
	
	@GetMapping("/find")
	public Page<Script> findScripts(@RequestParam(required = false, defaultValue = "") String search, @RequestParam(required = false) String organization,
			@RequestParam(required = false, defaultValue = "false") boolean archived, Pageable pageable) {
		if (archived) {
			return scriptService.findArchived(search, pageable);
		}
		
		if(organization != null) {
			Organization org = organizationService.getByOrganizationId(organization);
			if(pageable.getSort() == null){
				pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.Direction.DESC, "priority");
			}
			return scriptService.find(search, org, pageable);
		}

		return scriptService.find(search, pageable);
	}

}
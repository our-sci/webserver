package net.oursci.server.script;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import net.oursci.server.user.User;

@Data
@Entity
@Table(name = "script")
public class Script {
	
	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "scriptid", unique=true)
	private String scriptId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "version")
	private String version;
	
	@Column(name = "picture")
	private String picture;
	
	@Column(name = "date")
	private Date date;
	
	@OneToOne
	private User user;
	
	private boolean archived = false;

}

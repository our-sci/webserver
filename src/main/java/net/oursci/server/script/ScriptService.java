package net.oursci.server.script;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import net.oursci.server.exception.BaseException;
import net.oursci.server.organization.Organization;

public interface ScriptService {
	
	Script findOne(Long id) throws BaseException;
	Script save(Script script);
	Script findByIdentifier(String identifier);
	
	Page<Script> find(String search, Pageable pageable);
	Page<Script> find(String search, Organization organization, Pageable pageable);
	Page<Script> findArchived(String search, Pageable pageable);
	
}

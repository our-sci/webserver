package net.oursci.server.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

	@Autowired
	JavaMailSender emailSender;
	
	@Value("${spring.mail.username}")
	private String username;
	
	@Value("${app.invitation.email.name}")
	private String name;
	
	
	@Override
	public boolean sendMail(String to, String subject, String body) {
		
		System.out.println("Sending email to: " + to);
		System.out.println("Subject: " + subject);
		System.out.println("Body: " + body);
		
		String from = String.format("%s <%s>", name, username);
		
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(from);
		message.setTo(to);
		message.setSubject(subject);
		message.setText(body);
		
		try {
			emailSender.send(message);
		} catch(MailException me) {
			me.printStackTrace();
			return false;
		}
		
		return true;
	}

}

package net.oursci.server.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.oursci.server.config.SecurityConfig.Roles;
import net.oursci.server.exception.NotAdmin;
import net.oursci.server.firebase.FirebaseService;
import net.oursci.server.mappings.UserProfile;
import net.oursci.server.s3.S3Wrapper;
import net.oursci.server.user.User;
import net.oursci.server.user.UserService;
import net.oursci.server.util.OursciUtils;

@RestController
@RequestMapping(value = "/api/profile")
public class ProfilesController {

	@Autowired
	OursciUtils utils;
	
	@Autowired
	UserService userService;
	
	@Autowired
	S3Wrapper s3Wrapper;
	
	@Autowired
	FirebaseService firebaseService;

	@Autowired
	private ModelMapper modelMapper;

	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	@RequestMapping(value = "/self", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public UserProfile apiClient() {
		return modelMapper.map(utils.getCurrentUser(), UserProfile.class);
	}

	@PostMapping
	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	public UserProfile updateProfile(@RequestParam String uid, @RequestParam(required = false) String displayName,
			@RequestParam(required = false) MultipartFile[] picture,
			@RequestParam(required = false) Boolean clearPicture) {
		
		User current = utils.getCurrentUser();		
		User user = userService.getByUsername(uid);
		
		if(current != user && !utils.isAdmin(current)) {
			throw new NotAdmin();
		}
		

		if(displayName != null) {
			user.setDisplayName(displayName);
			firebaseService.updateDisplayName(uid, displayName);
		}
		
		if(clearPicture != null && clearPicture) {
			user.setPicture(null);
			firebaseService.updatePhotoUrl(uid, null);
		}
		
		if (picture != null && picture.length == 1) {
			String filename = picture[0].getOriginalFilename();
			String prefix = OursciUtils.getUserProfilePrefix(user.getUsername());
			s3Wrapper.upload(picture, prefix);
			String key = prefix + filename;
			String pictureUrl = OursciUtils.convertToProfileIconLink(s3Wrapper.getURL(key));
			user.setPicture(pictureUrl);
			firebaseService.updatePhotoUrl(uid, pictureUrl);
		}
		
		User updated = userService.save(user);

		return modelMapper.map(updated, UserProfile.class);
	}
	
	@RequestMapping("/uid/{uid}")
	public UserProfile getProfileById(@PathVariable String uid) {
		User user = userService.getByUsername(uid);
		return modelMapper.map(user, UserProfile.class);
	}

}

package net.oursci.server.controller;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.oursci.server.mail.MailService;
import net.oursci.server.survey.Survey;
import net.oursci.server.survey.SurveyService;
import net.oursci.server.surveyresult.SurveyResult;
import net.oursci.server.surveyresult.SurveyResultRepository;
import net.oursci.server.surveyresult.SurveyResultService;
import net.oursci.server.user.Contribution;
import net.oursci.server.user.User;
import net.oursci.server.user.UserService;
import net.oursci.server.websocket.Statistics;

@RestController
@RequestMapping(value = "/api/debug")
public class DebugController {

	
	@Autowired
	SurveyResultRepository surveyResultRepository;
	
	@Autowired
	UserService userService;
	
	@Autowired
	SurveyResultService surveyResultService;
	
	@Autowired
	SurveyService surveyService;

	@Autowired
	SimpMessagingTemplate webSocket;
	
	@RequestMapping("/contribution/user/{username}")
	Page<Contribution> getContributionsByUser(@PathVariable String username, Pageable pageable) {
		User user = userService.getByUsername(username);
		Page<Contribution> ret = surveyResultRepository.findContributionsByUser(user, pageable);		
		return ret;
	}
	
	@RequestMapping("/latest")
	List<SurveyResult> getLatest(@RequestParam String survey, @RequestParam int size) {
		Survey s = surveyService.getByIdentifier(survey);
		Pageable pageable = PageRequest.of(0, size);
		return surveyResultService.find(s, pageable);
	}
	
	@RequestMapping("/up")
	void up() {
		Statistics stats = new Statistics();
		stats.setSurveys(surveyService.count());
		stats.setSurveyResults(surveyResultService.count() + 1);
		webSocket.convertAndSend("/topic/stats", stats);
	}
	
	@Autowired
	MailService mailService;
	
	@RequestMapping("/email/send")
	public boolean sendEmail(@RequestParam String to, @RequestParam String subject, @RequestParam String body) {
		System.out.println("Sending email to: "+to);
		System.out.println("Subject: "+subject);
		System.out.println("Body: "+body);
		
		return mailService.sendMail(to, subject, body);
	}
	
	@Value("${app.invitation.email.body}")
	String msg;
	
	@RequestMapping("/test")
	public String test() {
		return MessageFormat.format(msg, "Nexus-Computing", "133839813-41041412-4133");
	}
	
	
	@RequestMapping("/stats/survey")
	public Map<Survey, Integer> statsSurvey() {
		// most submitted surveys in
		// Survey(id=245, formTitle=Malawi Panel Soil Scan, formId=build_Malawi-Panel-Soil-Scan_1536765214 => 1515, took 3.62s
		// Survey(id=126, formTitle=Quick Carbon 2018 WYMT v9, formId=Quick Carbon 2018 WYMT v9 => 1483, took 3.994s
		// Survey(id=601, formTitle=2019 Soil Scan ONLY, formId=build_2019-Soil-Scan-ONLY_1557258399 => 1482, took 5.370s
		// Survey(id=417, formTitle=Panel Harvest Draft 7, formId=build_Panel-Harvest-Draft-7_1554030328 => 908, took 3.687s
		// Survey(id=110, formTitle=NLS Quick Carbon - Lab Survey v3, formId=NLS Quick Carbon - Lab Survey v3 => 593, took 1.826s
		// Survey(id=239, formTitle=Carrot Quality Survey v1, formId=build_Carrot-Quality-Survey-v1_1536173572 => 400, took 2.7s
		// Survey(id=229, formTitle=QC-2018-FortKeogh_SIG, formId=QC-2018-FortKeogh_SIG => 205, took 1.388s 
		// Survey(id=251, formTitle=QC 2018 Boulder, formId=QC 2018 Boulder => 100, took 1.351s
		
		
		
		List<Survey> surveys = (List<Survey>) surveyService.findAll();
		Map<Survey, Integer> unsortedMap = new HashMap<Survey, Integer>();
		
		for(Survey survey: surveys) {
			unsortedMap.put(survey, surveyResultService.find(survey).size());
		}
		
		Map<Survey, Integer> sortedMap = sortByValue(unsortedMap);
				
		return sortedMap;
	}
	
	private static Map<Survey, Integer> sortByValue(Map<Survey, Integer> unsortedMap) {
		
		// Convert Map to List of Map
		List<Map.Entry<Survey, Integer>> list = new LinkedList<>(unsortedMap.entrySet());
		
		// Sort list
		Collections.sort(list, new Comparator<Map.Entry<Survey, Integer>>() {
			@Override
			public int compare(Entry<Survey, Integer> o1, Entry<Survey, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});
		
		// Loop the sorted list
		Map<Survey, Integer> sortedMap = new LinkedHashMap<Survey, Integer>();
		for(Map.Entry<Survey, Integer> entry: list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}
				
		return sortedMap;
	}
	
	/*
	private static LinkedHashMap<Survey, Integer> sortHashMapByValues(
	        Map<Survey, Integer> passedMap) {
	    List<Survey> mapKeys = new ArrayList<>(passedMap.keySet());
	    List<Integer> mapValues = new ArrayList<>(passedMap.values());
	    Collections.sort(mapValues);
	    Collections.reverse(mapValues);
	    //Collections.sort(mapKeys);

	    LinkedHashMap<Survey, Integer> sortedMap =
	        new LinkedHashMap<>();

	    Iterator<Integer> valueIt = mapValues.iterator();
	    while (valueIt.hasNext()) {
	        Integer val = valueIt.next();
	        Iterator<Survey> keyIt = mapKeys.iterator();

	        while (keyIt.hasNext()) {
	            Survey key = keyIt.next();
	            Integer comp1 = passedMap.get(key);
	            Integer comp2 = val;

	            if (comp1.equals(comp2)) {
	                keyIt.remove();
	                sortedMap.put(key, val);
	                break;
	            }
	        }
	    }
	    return sortedMap;
	}
	*/
	
	
}

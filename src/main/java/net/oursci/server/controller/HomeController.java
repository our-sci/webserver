package net.oursci.server.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.oursci.server.config.OursciConfig;
import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.dashboard.DashboardRepository;
import net.oursci.server.dashboard.DashboardService;
import net.oursci.server.exception.DashboardNotFound;
import net.oursci.server.exception.ScriptNotFound;
import net.oursci.server.s3.S3Wrapper;
import net.oursci.server.script.Script;
import net.oursci.server.script.ScriptRepository;
import net.oursci.server.util.ApplicationInfo;
import net.oursci.server.util.OursciUtils;

@Controller
@RequestMapping(path = "/")
public class HomeController {

	@Value("${cloud.aws.s3.bucket}")
	private String bucket;
	
	@Autowired
	S3Wrapper s3Wrapper;
	
	@Autowired
	DashboardRepository dashboardRepository;
	
	@Autowired
	DashboardService dashboardService;
	
	@Autowired
	ScriptRepository scriptRepository;
		
	@RequestMapping(path = {"/dashboard/{dashboardId:.+}", "/api/dashboard/{dashboardId:.+}"})
	public String showDashboard(Model model, @PathVariable("dashboardId") String dashboardId, HttpServletRequest request) throws ParseException {
		Dashboard dashboard = dashboardService.findByIdentifier(dashboardId);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date conversionDate = sdf.parse("2018-10-16");
		
		// check for legacy dashboards
		if(dashboard.getDate().before(conversionDate)) {
			model.addAttribute("scriptUrl", getDashboardUrl(dashboardId));
			model.addAttribute("title", dashboard.getName());
			
			return "dashboard-legacy";
		}
		
		model.addAttribute("title", dashboard.getName());
		model.addAttribute("dashboardId", dashboardId);
		
		String customQuery = request.getQueryString();
		if(customQuery != null) {
			customQuery = "?" + customQuery;
		} else {
			customQuery = "";
		}
		model.addAttribute("customQuery", customQuery);
		
		return "dashboard";
	}
	
	
	@RequestMapping(path = "/dashboard/{dashboardId:.+}/src/{file:.+}")
	public void requestDashboardFile(HttpServletResponse response, @PathVariable("dashboardId") String dashboardId, @PathVariable("file") String file) {
		Dashboard dashboard = dashboardService.findByIdentifier(dashboardId);
		if(dashboard == null) {
			throw new DashboardNotFound();
		}

		String prefix = OursciUtils.getDashboardDefinitionPrefix(dashboardId);
		String key = String.format("%s%s", prefix, file);
		s3Wrapper.serveFile(key, response);
	}
	
	@RequestMapping(path = "/dashboard/{dashboardId:.+}/inner.html")
	public String showDashboardSandbox(@PathVariable("dashboardId") String dashboardId){
		return "dashboard-inner";
	}
	
	@RequestMapping(path = {"/processor/{scriptId:.+}"})
	public String showProcessor(Model model,
			@PathVariable("scriptId") String scriptId,
			@RequestParam("formId") String formId,
			@RequestParam("instanceId") String instanceId,
			@RequestParam String filename) {
		Script script = scriptRepository.findByScriptId(scriptId);
		if(script == null) {
			throw new ScriptNotFound("Script id not found: "+scriptId);
		}
		
		String key = OursciUtils.getSurveyResultPrefix(formId, instanceId) + filename;
		InputStream inputStream = s3Wrapper.get(key);
		
		String result = "";
		try {
			result = IOUtils.toString(inputStream, OursciConfig.CHARSET);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		result = String.format("result = %s;\nprocessor={};\nprocessor.getResult = function(){return JSON.stringify(result);}\n", result);
		
		model.addAttribute("title", script.getName());
		model.addAttribute("result", result);
		model.addAttribute("processorUrl", getProcessorUrl(scriptId));
		model.addAttribute("measurementFileUrl", getMeasurementFileUrl(formId, instanceId, filename));
		return "processor";
	}
	
	private String getDashboardUrl(String dashboardId) {
		String encDashboardId = dashboardId;
		String encFilename = "bundle.js";

		try {
			encDashboardId = URLEncoder.encode(dashboardId, OursciConfig.CHARSET);
			encFilename = URLEncoder.encode("bundle.js", OursciConfig.CHARSET);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		//String url = String.format("https://s3.%s.amazonaws.com/%s/dashboard-definitions/%s/%s", region, bucket,
		//		encDashboardId, encFilename);
		String url = String.format("%s/storage/dashboard-definitions/%s/%s", ApplicationInfo.URL_BASE, encDashboardId, encFilename);
		
		return url;
	}
	
	private String getProcessorUrl(String scriptId) {
		String encScriptId = scriptId;
		String encFilename = "processor.js";
		
		try {
			encScriptId = URLEncoder.encode(scriptId, OursciConfig.CHARSET);
			encFilename = URLEncoder.encode("processor.js", OursciConfig.CHARSET);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		//String url = String.format("https://s3.%s.amazonaws.com/%s/script-definitions/%s/%s", region, bucket,
		//		encScriptId, encFilename);
		String url = String.format("%s/storage/script-definitions/%s/%s", ApplicationInfo.URL_BASE, encScriptId, encFilename);
		
		return url;
	}
	
	private String getMeasurementFileUrl(String formId, String instanceId, String filename) {
		String encSurveyId = formId;
		String encInstanceId = instanceId;
		String encFilename = filename;

		try {
			encSurveyId = URLEncoder.encode(formId, OursciConfig.CHARSET);
			encInstanceId = URLEncoder.encode(instanceId, OursciConfig.CHARSET);
			encFilename = URLEncoder.encode(filename, OursciConfig.CHARSET);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String url = String.format("https://s3.%s.amazonaws.com/%s/survey-results/%s/%s/%s", region, bucket,
		//		encSurveyId, encInstanceId, encFilename);
		String url = String.format("%s/storage/survey-results/%s/%s/%s", ApplicationInfo.URL_BASE, encSurveyId, encInstanceId, encFilename);
		
		return url;
	}
	
	
}

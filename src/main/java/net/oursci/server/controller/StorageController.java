package net.oursci.server.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.oursci.server.dashboard.DashboardRepository;
import net.oursci.server.exception.NotAdmin;
import net.oursci.server.exception.OrganizationNotFound;
import net.oursci.server.firebase.FirebaseAuthenticationToken;
import net.oursci.server.firebase.FirebaseService;
import net.oursci.server.firebase.FirebaseTokenHolder;
import net.oursci.server.markdown.MarkdownService;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.OrganizationService;
import net.oursci.server.s3.S3Wrapper;
import net.oursci.server.script.ScriptRepository;
import net.oursci.server.surveyresult.SurveyResult;
import net.oursci.server.surveyresult.SurveyResultService;
import net.oursci.server.util.OursciUtils;

@Controller
@RequestMapping(path = "/storage")
public class StorageController {
	
	@Value("${cloud.aws.s3.bucket}")
	private String bucket;
	
	@Autowired
	S3Wrapper s3Wrapper;
	
	@Autowired
	DashboardRepository dashboardRepository;
	
	@Autowired
	OrganizationService organizationService;
	
	@Autowired
	ScriptRepository scriptRepository;
	
	@Autowired
	MarkdownService markdownService;
	
	@Autowired
	SurveyResultService surveyResultService;
	
	@Autowired
	FirebaseService firebaseService;
	
	@Autowired
	OursciUtils utils;
	
	@RequestMapping(path = "/organization/{organizationId}/assets/internal/{file:.+}")
	public void getInternalOrganizationAssets(HttpServletResponse response, @PathVariable("organizationId") String organizationId, @PathVariable("file") String file) {
		Organization organization = organizationService.findByOrganizationId(organizationId);
		if(organization == null) {
			throw new OrganizationNotFound();
		}

		String prefix = OursciUtils.getOrganizationAssetsInternalPrefix(organizationId);
		String key = String.format("%s%s", prefix, file);
		s3Wrapper.serveFile(key, response);
	}
	
	@RequestMapping(path = "/organization/{organizationId:.+}/assets/external/{file:.+}")
	public void getExternalOrganizationAssets(HttpServletResponse response, @PathVariable("organizationId") String organizationId, @PathVariable("file") String file) {
		Organization organization = organizationService.findByOrganizationId(organizationId);
		if(organization == null) {
			throw new OrganizationNotFound();
		}

		String prefix = OursciUtils.getOrganizationAssetsExternalPrefix(organizationId);
		String key = String.format("%s%s", prefix, file);
		s3Wrapper.serveFile(key, response);
	}
	
	@RequestMapping(path = "/survey-results/{formId:.+}/{instanceId}/{file:.+}")
	public void getSurveyResultAssets(HttpServletResponse response, @PathVariable("formId") String formId, @PathVariable("instanceId") String instanceId, @PathVariable("file") String file, @RequestParam(required = false, defaultValue = "") String token) {
		SurveyResult surveyResult = surveyResultService.findByIdentifier(instanceId);
		if(surveyResult.getInstanceFile().equalsIgnoreCase(file)) {
			// TODO: only allow admins to view instance file
			if(token.isEmpty()) {
				throw new NotAdmin();
			}
			FirebaseTokenHolder holder = firebaseService.parseToken(token);
			String userName = holder.getUid();
			Authentication auth = new FirebaseAuthenticationToken(userName, holder);
			SecurityContextHolder.getContext().setAuthentication(auth);
			if(!utils.isAdmin()) {
				throw new NotAdmin();
			}
		}
		String prefix = OursciUtils.getSurveyResultPrefix(formId, instanceId);		
		String key = String.format("%s%s", prefix, file);
		s3Wrapper.serveFile(key, response);
	}
	
	@RequestMapping(path = "/user/profile/{username:.+}/{file:.+}")
	public void getUserProfilePicture(HttpServletResponse response, @PathVariable("username") String username, @PathVariable("file") String file) {
		String prefix = OursciUtils.getUserProfilePrefix(username);;
		String key = String.format("%s%s", prefix, file);
		s3Wrapper.serveFile(key, response);
	}
	
	@RequestMapping(path = "/dashboard-definitions/{dashboardId:.+}/{file:.+}")
	public void getDashboardDefinitions(HttpServletResponse response, @PathVariable("dashboardId") String dashboardId, @PathVariable("file") String file) {
		String prefix = OursciUtils.getDashboardDefinitionPrefix(dashboardId);
		String key = String.format("%s%s", prefix, file);
		s3Wrapper.serveFile(key, response);
	}
	
	@RequestMapping(path = "/script-definitions/{scriptId:.+}/{file:.+}")
	public void getScriptDefinitions(HttpServletResponse response, @PathVariable("scriptId") String scriptId, @PathVariable("file") String file) {
		String prefix = OursciUtils.getScriptDefinitionPrefix(scriptId);
		String key = String.format("%s%s", prefix, file);
		s3Wrapper.serveFile(key, response);
	}
	
	@RequestMapping(path = "/survey-definitions/{formId:.+}/{file:.+}") 
	public void getSurveyDefinitions(HttpServletResponse response, @PathVariable("formId") String formId, @PathVariable("file") String file) throws IOException {
		String prefix = OursciUtils.getSurveyDefinitionPrefix(formId);
		String key = String.format("%s%s", prefix, file);
		
		if(file.toLowerCase().endsWith(".md")) {
			String markdown = IOUtils.toString(s3Wrapper.get(key), "UTF-8");
			String html = markdownService.render(markdown);
			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("text/html");
			response.getWriter().write(html);
			response.flushBuffer();
			return;
		}
		
		s3Wrapper.serveFile(key, response);
	}
	
	
}

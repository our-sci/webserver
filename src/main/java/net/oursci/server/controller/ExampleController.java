package net.oursci.server.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.oursci.server.config.SecurityConfig.Roles;

@RestController
public class ExampleController {

	@RequestMapping(value = "/api/open/example", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Object apiOpen() {
		Map<String, String> map = new HashMap<>();
		map.put("action", "/api/open/example");
		map.put("api", "open");
		
		return map;
	}

	@RequestMapping(value = "/api/client/example", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Object apiClient() {
		Map<String, String> map = new HashMap<>();
		map.put("action", "/api/client/example");
		map.put("api", "client");
		
		return map;
	}

	@RequestMapping(value = "/api/admin/example", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Object apiAdmin() {
		Map<String, String> map = new HashMap<>();
		map.put("action", "/api/admin/example");
		map.put("api", "admin");
		
		return map;
	}
	
	@Secured(value = {Roles.ROLE_ADMIN, Roles.ROLE_USER})
	@RequestMapping(value = "/api/open/closed", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Object openClosed() {
		Map<String, String> map = new HashMap<>();
		map.put("action", "/api/open/closed");
		map.put("api", "admin");
		
		return map;
	}
	
}

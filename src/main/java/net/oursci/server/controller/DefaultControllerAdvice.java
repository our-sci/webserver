package net.oursci.server.controller;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class DefaultControllerAdvice {

	@Autowired
	private Environment env;

	@Value("${app.version}")
	private String applicationVersion;

	@Value("${app.dashboard.sandbox.base}")
	private String dashboardBase;

	@ModelAttribute("applicationVersion")
	public String getApplicationVersion() {
		return applicationVersion;
	}

	@ModelAttribute("dashboardBase")
	private String getDashboardBase() {

		if (Arrays.stream(env.getActiveProfiles()).anyMatch(profile -> profile.equalsIgnoreCase("local"))) {
			try (final DatagramSocket socket = new DatagramSocket()) {
				socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
				String ip = socket.getLocalAddress().getHostAddress();
				return String.format("http://%s:5000/dashboard/", ip);

			} catch (SocketException e1) {
				e1.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}

		return dashboardBase;
	}

}

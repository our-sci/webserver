package net.oursci.server.websocket;

import lombok.Data;

@Data
public class Statistics {
	
	private Long surveys;
	private Long surveyResults; 
	
}

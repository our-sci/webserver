package net.oursci.server.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import net.oursci.server.survey.SurveyService;
import net.oursci.server.surveyresult.SurveyResultService;

@Controller
public class WebsocketController {

	@Autowired
	SurveyService surveyService;
	
	@Autowired
	SurveyResultService surveyResultService;
	
	@MessageMapping("/hello")
    @SendTo("/topic/stats")
    public Statistics greeting(HelloMessage message) throws Exception {
		
        // Thread.sleep(1000); // simulated delay
        
        Statistics stats = new Statistics();
        stats.setSurveys(surveyService.count());
        stats.setSurveyResults(surveyResultService.count());
        
        return stats;
    }
	
}

package net.oursci.server.survey;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import net.oursci.server.organization.Organization;
import net.oursci.server.user.User;

public interface SurveyRepository extends PagingAndSortingRepository<Survey, Long> {
	
	Collection<Survey> findAll();
	Survey findByFormId(String formId);
	
	Page<Survey> findByFormTitleContainingAndArchivedFalse(String formTitle, Pageable pageable); 
	Page<Survey> findByFormTitleContainingAndArchivedTrue(String formTitle, Pageable pageable);

	/*
	@Query("SELECT sr.survey, MAX(sr.modified)  as date FROM SurveyResult sr WHERE sr.survey.archived='N' GROUP BY sr.survey ORDER BY date DESC")
	Page<Survey> findLatestContributions(Pageable pageable);
	*/
	
	@Query("SELECT ofav.survey FROM OrganizationFavorite ofav WHERE ofav.survey.formTitle LIKE CONCAT('%',:searchTerm,'%') AND ofav.owner=:organization AND ofav.type='survey' ORDER BY priority DESC")
	Page<Survey> findByOrganization(@Param("searchTerm") String searchTerm, @Param("organization") Organization organization, Pageable pageable);
	
	Page<Survey> findByFormTitleContainingAndUserAndArchivedFalse(String formTitle, User user, Pageable pageable);
	
	@Query("SELECT s from Survey s WHERE (formTitle LIKE CONCAT('%',:search,'%') OR formId = :search) AND archived = :archived")
	Page<Survey> find(@Param("search") String search, @Param("archived") boolean archived, Pageable pageable);
	
}

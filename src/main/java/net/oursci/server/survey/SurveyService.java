package net.oursci.server.survey;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import net.oursci.server.exception.BaseException;
import net.oursci.server.organization.Organization;
import net.oursci.server.user.User;

public interface SurveyService {

	long count();
	
	Survey create(Survey se);
	Collection<Survey> findAll();
	
	Survey findOne(Long id) throws BaseException;
	Survey save(Survey survey);
	Survey getByIdentifier(String identifier);
	
	
	Page<Survey> find(String searchTerm, Pageable pageable);
	Page<Survey> findArchived(String searchTerm, Pageable pageable);
	
	Page<Survey> find(String searchTerm, Organization organization, Pageable pageable);
	Page<Survey> find(String searchTerm, User user, Pageable pageable);
	
}

package net.oursci.server.survey;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.javarosa.core.model.FormDef;
import org.javarosa.core.util.externalizable.DeserializationException;
import org.javarosa.xform.parse.XFormParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.oursci.server.config.SecurityConfig.Roles;
import net.oursci.server.exception.BadRequest;
import net.oursci.server.exception.SurveyExistsException;
import net.oursci.server.exception.SurveyNotFound;
import net.oursci.server.exception.XmlFormNotFound;
import net.oursci.server.javarosa.JavaRosaUtils;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.OrganizationService;
import net.oursci.server.s3.S3Wrapper;
import net.oursci.server.user.User;
import net.oursci.server.user.UserService;
import net.oursci.server.util.OursciUtils;
import net.oursci.server.util.StringUtil;

@RestController
@RequestMapping(value = "/api/survey")
@CrossOrigin
public class SurveyController {

	@Autowired
	S3Wrapper s3Wrapper;

	@Autowired
	SurveyService surveyService;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	SurveyRepository surveyRepository;

	@Autowired
	OursciUtils utils;

	@Autowired
	UserService userService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<Survey> getSurveys() {
		return (List<Survey>) surveyRepository.findAll();
	}

	@GetMapping("/find")
	public Page<Survey> findSurveys(@RequestParam(required = false, defaultValue = "") String search,
			@RequestParam(required = false) String organization, @RequestParam(required = false) String user,
			@RequestParam(required = false, defaultValue = "false") boolean archived, Pageable pageable) {
		if (archived) {
			return surveyService.findArchived(search, pageable);
		}

		if (organization != null) {
			Organization org = organizationService.getByOrganizationId(organization);
			return surveyService.find(search, org, pageable);
		}

		// add default date sort if search is empty, and no other sort was supplied
		if (search.isEmpty() && (pageable.getSort() == null || pageable.getSort().isUnsorted())) {
				pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.Direction.DESC,
						"date");
		}
		
		if (user != null) {
			User u = userService.getByUsername(user);
			return surveyService.find(search, u, pageable);
		}

		return surveyService.find(search, pageable);
	}

	@RequestMapping(value = "/by-database-id/{id}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Survey getSurveyByDatabaseId(@PathVariable("id") Long id) {
		return surveyService.findOne(id);
	}

	@RequestMapping(value = "/by-form-id/{formId}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Survey getSurveyByFormId(@PathVariable("formId") String formId) {
		return surveyRepository.findByFormId(formId);
	}

	@RequestMapping(value = "/toggle/archived/by-form-id/{formId}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Survey toggleArchived(@PathVariable("formId") String formId) {
		Survey survey = surveyRepository.findByFormId(formId);
		survey.setArchived(!survey.isArchived());
		return surveyRepository.save(survey);
	}

	@RequestMapping(value = "/keys/by-form-id/{formId}", method = RequestMethod.GET)
	public String getKeysByFormId(@PathVariable String formId) throws IOException {
		Survey survey = surveyRepository.findByFormId(formId);
		if (survey == null) {
			throw new SurveyNotFound("Survey does not exist: " + formId);
		}
		return getKeys(survey);
	}

	@RequestMapping(value = "/keys/by-database-id/{id}", method = RequestMethod.GET)
	public String getKeysByDatabaseId(@PathVariable Long id) throws IOException {
		Survey survey = surveyService.findOne(id);
		return getKeys(survey);
	}

	private String getKeys(Survey survey) {
		String formId = survey.getFormId();
		String formFile = survey.getFormFile();

		String key = OursciUtils.getSurveyDefinitionPrefix(formId) + formFile;
		InputStream is = s3Wrapper.get(key);

		return JavaRosaUtils.getJsonKeys(is);
	}

	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	public Survey create(@RequestParam MultipartFile[] files,
			@RequestParam(required = false, defaultValue = "false") Boolean override)
			throws XFormParseException, IOException, DeserializationException, InterruptedException {

		FormDef formDef = null;
		String formId = null;
		String formTitle = null;
		String formFile = null;

		for (MultipartFile file : files) {
			System.out.println(file.getOriginalFilename());
			if (file.getOriginalFilename().toLowerCase().endsWith(".xml")) {
				formFile = file.getOriginalFilename();
				formDef = JavaRosaUtils.getFormDef(file.getInputStream());
				formTitle = formDef.getTitle();
				formId = JavaRosaUtils.getFormId(formDef);
				break;
			}
		}

		if (formDef == null || formTitle == null || formId == null || formFile == null) {
			throw new XmlFormNotFound("No form found in uploaded files");
		}

		Survey surveyEntity = surveyRepository.findByFormId(formId);
		if (surveyEntity != null) {
			if (!override) {
				throw new SurveyExistsException("A survey with id " + formId + " already exists.");
			}

			if (!utils.isAdmin() && utils.getCurrentUser() != surveyEntity.getUser()) {
				throw new BadRequest("Only owner and admins can override surveys");
			}
		} else {
			surveyEntity = new Survey();
		}

		// persists on S3
		String prefix = OursciUtils.getSurveyDefinitionPrefix(formId);
		s3Wrapper.upload(files, prefix);

		// persist in datbase
		surveyEntity.setDate(new Date());
		surveyEntity.setFormFile(formFile);
		surveyEntity.setFormTitle(formTitle);
		surveyEntity.setFormId(formId);
		surveyEntity.setUser(utils.getCurrentUser());
		surveyEntity.setArchived(false);

		return surveyRepository.save(surveyEntity);
	}
	
	@RequestMapping(value = "/assets/{formId}")
	public List<String> listFiles(@PathVariable String formId) {
		Survey survey = surveyRepository.findByFormId(formId);
		if (survey == null) {
			throw new SurveyNotFound("Survey does not exist: " + formId);
		}
		
		String prefix = OursciUtils.getSurveyDefinitionPrefix(formId);

		return OursciUtils.transformToStorageLinks(s3Wrapper.getURLs(prefix), "/survey-definitions");
	}

	@RequestMapping(value = "/download/listing/by-form-id/{formId}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<String> getDownloadListByFormId(@PathVariable("formId") String formId) {

		Survey survey = surveyRepository.findByFormId(formId);
		if (survey == null) {
			throw new SurveyNotFound("Survey does not exist: " + formId);
		}

		return getDownloadList(survey);
	}

	@RequestMapping(value = "/download/listing/by-database-id/{formId}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<String> getDownloadListByFormId(@PathVariable("id") Long id) {
		Survey survey = surveyService.findOne(id);
		return getDownloadList(survey);
	}

	private List<String> getDownloadList(Survey survey) {
		List<String> downloadLinks = new ArrayList<>();

		String prefix = OursciUtils.getSurveyDefinitionPrefix(survey.getFormId());
		List<String> keys = s3Wrapper.getKeys(prefix);

		for (String key : keys) {
			downloadLinks.add(s3Wrapper.getSignedDownloadUrl(key).toString());
		}

		return downloadLinks;
	}

	@RequestMapping(value = "/download/zip/by-form-id/{formId}", produces = "application/zip")
	public byte[] downloadZipByFormId(@PathVariable("formId") String formId, HttpServletResponse response)
			throws IOException {
		Survey survey = surveyRepository.findByFormId(formId);
		if (survey == null) {
			throw new SurveyNotFound("Survey does not exist: " + formId);
		}
		return downloadZip(survey, response);
	}

	@RequestMapping(value = "/download/zip/by-database-id/{id}", produces = "application/zip")
	public byte[] downloadZipByDatabaseId(@PathVariable("id") Long id, HttpServletResponse response)
			throws IOException {
		Survey survey = surveyService.findOne(id);
		return downloadZip(survey, response);
	}

	@RequestMapping(value = "/download/xml/by-form-id/{formId}")
	public void serveXMLfromFormId(@PathVariable("formId") String formId, HttpServletResponse response) {
		Survey survey = surveyService.getByIdentifier(formId);
		serveXmlFile(survey, response);
	}

	@RequestMapping(value = "/download/xml/by-database-id/{id}")
	public void serveXMLfromDatabaseId(@PathVariable("id") Long id, HttpServletResponse response) {
		Survey survey = surveyService.findOne(id);
		serveXmlFile(survey, response);
	}

	private void serveXmlFile(Survey survey, HttpServletResponse response) {
		String formFile = survey.getFormFile();
		String prefix = OursciUtils.getSurveyDefinitionPrefix(survey.getFormId());
		String key = prefix + formFile;
		s3Wrapper.serveFile(key, response);
	}

	private byte[] downloadZip(Survey survey, HttpServletResponse response) throws IOException {

		// setting headers
		response.setStatus(HttpServletResponse.SC_OK);
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"%s.zip\"", survey.getFormId()));

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
		ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);

		String prefix = OursciUtils.getSurveyDefinitionPrefix(survey.getFormId());
		List<String> keys = s3Wrapper.getKeys(prefix);

		// packing files
		for (String key : keys) {
			// new zip entry and copying inputstream with file to
			// zipOutputStream, after all closing streams
			String fileName = StringUtil.getBasename(key);
			zipOutputStream.putNextEntry(new ZipEntry(fileName));
			InputStream inputStream = s3Wrapper.get(key);

			IOUtils.copy(inputStream, zipOutputStream);

			inputStream.close();
			zipOutputStream.closeEntry();
		}

		if (zipOutputStream != null) {
			zipOutputStream.finish();
			zipOutputStream.flush();
			IOUtils.closeQuietly(zipOutputStream);
		}
		IOUtils.closeQuietly(bufferedOutputStream);
		IOUtils.closeQuietly(byteArrayOutputStream);
		return byteArrayOutputStream.toByteArray();
	}

}
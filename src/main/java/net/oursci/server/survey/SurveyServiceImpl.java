package net.oursci.server.survey;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.oursci.server.exception.BaseException;
import net.oursci.server.exception.SurveyNotFound;
import net.oursci.server.organization.Organization;
import net.oursci.server.user.User;

@Service
public class SurveyServiceImpl implements SurveyService {

	@Autowired
	SurveyRepository repo;
	
	@Override
	public long count() {
		return repo.count();
	}
	
	@Override
	public Survey create(Survey se) {
		return repo.save(se);
	}

	@Override
	public Collection<Survey> findAll() {
		return repo.findAll();
	}

	@Override
	public Survey findOne(Long id) throws BaseException {
		return repo.findById(id).orElseThrow(() -> new SurveyNotFound());
	}

	@Override
	public Survey save(Survey survey) {
		return repo.save(survey);
	}

	@Override
	public Survey getByIdentifier(String identifier) {
		Survey survey = repo.findByFormId(identifier);
		if(survey == null) {
			throw new SurveyNotFound();
		}
		return survey;
	}

	@Override
	public Page<Survey> find(String search, Pageable pageable) {
		return repo.find(search, false, pageable);
	}
	
	@Override
	public Page<Survey> find(String searchTerm, Organization organization, Pageable pageable) {
		return repo.findByOrganization(searchTerm, organization, pageable);
	}

	@Override
	public Page<Survey> findArchived(String search, Pageable pageable) {
		return repo.find(search, true, pageable);
	}

	@Override
	public Page<Survey> find(String searchTerm, User user, Pageable pageable) {
		return repo.findByFormTitleContainingAndUserAndArchivedFalse(searchTerm, user, pageable);
	}





}

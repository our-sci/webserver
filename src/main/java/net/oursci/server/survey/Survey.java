package net.oursci.server.survey;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import net.oursci.server.user.User;
import net.oursci.server.views.Views;

@Data
@Entity
@Table(name = "survey")
public class Survey {
	
	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.Summary.class)
	private Long id;
	
	@Column(name = "formtitle")
	@JsonView(Views.Summary.class)
	private String formTitle;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "picture")
	private String picture;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "formid", unique=true)
	@JsonView(Views.Summary.class)
	private String formId;
	
	@Column(name = "formfile")
	private String formFile;
	
	@Column(name = "formVersion")
	private String formVersion;
	
	@Column(name = "latestContribution")
	private Date latestContribution;
	
	@NotNull
	@OneToOne
	private User user;

	private boolean archived = false;
	
	
	
}

package net.oursci.server.dashboard;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.javarosa.core.util.externalizable.DeserializationException;
import org.javarosa.xform.parse.XFormParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.oursci.server.config.SecurityConfig.Roles;
import net.oursci.server.exception.DashboardExistsException;
import net.oursci.server.exception.DashboardNotFound;
import net.oursci.server.exception.ScriptManifestNotFound;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.OrganizationService;
import net.oursci.server.s3.S3Wrapper;
import net.oursci.server.survey.SurveyRepository;
import net.oursci.server.util.OursciUtils;
import net.oursci.server.util.StringUtil;

@RestController
@RequestMapping(value = "/api/dashboard")
public class DashboardController {

	@Autowired
	S3Wrapper s3Wrapper;

	@Autowired
	SurveyRepository surveyRepository;
	
	@Autowired
	DashboardRepository dashboardRepository;
	
	@Autowired
	DashboardService dashboardService;
	
	@Autowired
	OrganizationService organizationService;

	@Autowired
	OursciUtils oursciUtils;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<Dashboard> getSurveys() {
		return (List<Dashboard>) dashboardRepository.findAll();
	}
	
	@RequestMapping(value = "/by-dashboard-id/{dashboardId:.+}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Dashboard getDashboardByIdentifier(@PathVariable("dashboardId") String dashboardId) {
		return dashboardRepository.findByDashboardId(dashboardId);
	}
	
	@RequestMapping(value = "/by-database-id/{id}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Dashboard getDashboardByDatabaseId(@PathVariable("id") Long id) {
		return dashboardRepository.findById(id).orElse(null);
	}
	
	@RequestMapping(value = "/toggle/archived/by-dashboard-id/{dashboardId:.+}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Dashboard toggleArchived(@PathVariable("dashboardId") String dashboardId) {
		Dashboard dashboard = dashboardRepository.findByDashboardId(dashboardId);
		dashboard.setArchived(!dashboard.isArchived());
		return dashboardRepository.save(dashboard);
	}

	@Secured(value = {Roles.ROLE_ADMIN, Roles.ROLE_USER})
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	public Dashboard create(@RequestParam MultipartFile[] files,
			@RequestParam(required = false, defaultValue = "false") Boolean override)
			throws XFormParseException, IOException, DeserializationException, InterruptedException {

		String dashboardId = null;
		String scriptName = null;
		String scriptDescription = null;
		String scriptVersion = null;
		
		for (MultipartFile file : files) {
			System.out.println(file.getOriginalFilename());
			if (file.getOriginalFilename().equalsIgnoreCase("manifest.json")) {
				
				JSONObject json = OursciUtils.getJsonObject(file.getInputStream());
				try {
					dashboardId = json.getString("id");
					scriptName = json.getString("name");
					scriptDescription = json.getString("description");
					scriptVersion = json.getString("version");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				break;
			}
		}

		if (dashboardId == null) {
			throw new ScriptManifestNotFound("No manifest.json found in uploaded files, or dashboard id can not be determined");
		}
		
		Dashboard dashboardEntity = dashboardRepository.findByDashboardId(dashboardId);
		if(dashboardEntity != null) {
			if(override == false){
				throw new DashboardExistsException("A dashboard with id already exists: "+dashboardId);
			} else {
				// TODO: delete any existing files
			}
		}
		
		String prefix = OursciUtils.getDashboardDefinitionPrefix(dashboardId);
		s3Wrapper.upload(files, prefix);

		if(dashboardEntity == null) {
			dashboardEntity = new Dashboard();
		}
		
		dashboardEntity.setDate(new Date());
		dashboardEntity.setDashboardId(dashboardId);
		dashboardEntity.setName(scriptName);
		dashboardEntity.setDescription(scriptDescription);
		dashboardEntity.setVersion(scriptVersion);
		dashboardEntity.setUser(oursciUtils.getCurrentUser());
		dashboardEntity.setArchived(false);

		return dashboardRepository.save(dashboardEntity);
	}
	
	@RequestMapping(value = "/download/listing/by-dashboard-id/{dashboardId:.+}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<String> getDownloadListByDashboardId(@PathVariable("dashboardId") String dashboardId) {
		
		Dashboard script = dashboardRepository.findByDashboardId(dashboardId);
		if(script == null) {
			throw new DashboardNotFound("Script does not exist: "+dashboardId);
		}
		
		return getDownloadList(script);
	}
	
	@RequestMapping(value = "/download/listing/by-database-id/{id}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<String> getDownloadListByDatabaseId(@PathVariable("id") Long id) {
		Dashboard dashboard = dashboardService.findOne(id);
		return getDownloadList(dashboard);
	}
	
	private List<String> getDownloadList(Dashboard script) {
		List<String> downloadLinks = new ArrayList<>();
		
		String prefix = OursciUtils.getDashboardDefinitionPrefix(script.getDashboardId());
		List<String> keys = s3Wrapper.getKeys(prefix);
		
		for(String key : keys) {
			downloadLinks.add(s3Wrapper.getSignedDownloadUrl(key).toString());
		}
		
		return downloadLinks;
	} 
	
	@RequestMapping(value = "/download/zip/by-dashboard-id/{dashboardId:.+}", produces="application/zip")
    public byte[] downloadZipByDashboardId(@PathVariable("dashboardId") String dashboardId, HttpServletResponse response) throws IOException{
		Dashboard dashboard = dashboardService.findByIdentifier(dashboardId);
		return downloadZip(dashboard, response);
    }
	
	@RequestMapping(value = "/download/zip/by-database-id/{id}", produces="application/zip")
    public byte[] downloadDashboardByDatabaseId(@PathVariable("id") Long id, HttpServletResponse response) throws IOException{
		Dashboard dashboard = dashboardService.findOne(id);
		return downloadZip(dashboard, response);
    }
	
	private byte[] downloadZip(Dashboard dashboard, HttpServletResponse response) throws IOException {
        //setting headers
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-Disposition", String.format("attachment; filename=\"%s.zip\"", dashboard.getDashboardId()));

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
        ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);

        String prefix = OursciUtils.getDashboardDefinitionPrefix(dashboard.getDashboardId());
		List<String> keys = s3Wrapper.getKeys(prefix);
        
        //packing files
        for (String key	: keys) {
            //new zip entry and copying inputstream with file to zipOutputStream, after all closing streams
        	String fileName = StringUtil.getBasename(key);
            zipOutputStream.putNextEntry(new ZipEntry(fileName));
            InputStream inputStream = s3Wrapper.get(key);

            IOUtils.copy(inputStream, zipOutputStream);

            inputStream.close();
            zipOutputStream.closeEntry();
        }

        if (zipOutputStream != null) {
            zipOutputStream.finish();
            zipOutputStream.flush();
            IOUtils.closeQuietly(zipOutputStream);
        }
        IOUtils.closeQuietly(bufferedOutputStream);
        IOUtils.closeQuietly(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
	}
	
	@GetMapping("/find")
	public Page<Dashboard> findDashboards(@RequestParam(required = false, defaultValue = "") String search, @RequestParam(required = false) String organization,
			@RequestParam(required = false, defaultValue = "false") boolean archived, Pageable pageable) {
		if (archived) {
			return dashboardService.findArchived(search, pageable);
		}
		
		if(organization != null) {
			Organization org = organizationService.getByOrganizationId(organization);
			if(pageable.getSort() == null){
				pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.Direction.DESC, "priority");
			}
			return dashboardService.find(search, org, pageable);
		}

		return dashboardService.find(search, pageable);
	}

}
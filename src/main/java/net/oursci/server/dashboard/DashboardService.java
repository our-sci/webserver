package net.oursci.server.dashboard;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import net.oursci.server.exception.BaseException;
import net.oursci.server.organization.Organization;

public interface DashboardService {

	Dashboard findOne(Long id) throws BaseException;
	Dashboard save(Dashboard dashboard);
	Dashboard findByIdentifier(String identifier);
	
	Page<Dashboard> find(String search, Pageable pageable);
	Page<Dashboard> find(String search, Organization organization, Pageable pageable);
	Page<Dashboard> findArchived(String search, Pageable pageable);
}

package net.oursci.server.dashboard;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import net.oursci.server.organization.Organization;


public interface DashboardRepository extends PagingAndSortingRepository<Dashboard, Long> {
	
	Collection<Dashboard> findAll();
	Dashboard findByDashboardId(String scriptId);
	
	Page<Dashboard> findByNameContainingAndArchivedFalse(String search, Pageable pageable);
	Page<Dashboard> findByNameContainingAndArchivedTrue(String search, Pageable pageable);
	
	//Page<Dashboard> findByDashboardIdOrNameContainingAndArchivedFalse(String search, Pageable pageable);
	Page<Dashboard> findByNameContainingOrDashboardIdIsAndArchivedFalse(String name, String dashboardId, Pageable pageable);
	Page<Dashboard> findByDashboardIdAndArchivedFalse(String search, Pageable pageable);
	
	
	@Query("SELECT ofav.dashboard FROM OrganizationFavorite ofav WHERE ofav.dashboard.name LIKE CONCAT('%',:searchTerm,'%') AND ofav.owner=:organization AND ofav.type='dashboard' ORDER BY priority DESC")
	Page<Dashboard> findByOrganization(@Param("searchTerm") String searchTerm, @Param("organization") Organization organization, Pageable pageable);
	
	@Query("SELECT d from Dashboard d WHERE (name LIKE CONCAT('%',:search,'%') OR dashboardId = :search) AND archived = :archived")
	Page<Dashboard> find(@Param("search") String search, @Param("archived") boolean archived, Pageable pageable);
	
	
}

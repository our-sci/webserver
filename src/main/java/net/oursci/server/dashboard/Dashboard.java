package net.oursci.server.dashboard;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import net.oursci.server.user.User;
import net.oursci.server.views.Views;

@Data
@Entity
@Table(name = "dashboard")
public class Dashboard {
	
	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.Summary.class)
	private Long id;
	
	@Column(name = "dashboardid", unique=true)
	@JsonView(Views.Summary.class)
	private String dashboardId;
	
	@Column(name = "name")
	@JsonView(Views.Summary.class)
	private String name;
	
	@Column(name = "description")
	@JsonView(Views.Summary.class)
	private String description;
	
	@Column(name = "version")
	private String version;
	
	@Column(name = "picture")
	private String picture;
	
	@Column(name = "date")
	private Date date;
	
	@OneToOne
	private User user;
	
	private boolean archived = false;

}

package net.oursci.server.scriptresult;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;
import net.oursci.server.script.Script;
import net.oursci.server.survey.Survey;
import net.oursci.server.user.User;

@Entity
@Table(name="scriptresult")
@Data
public class ScriptResult {
	
	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private Date date;
	
	@Column(name = "instanceid", unique = true)
	private String instanceId;
	
	@Column(name = "instancefile")
	private String instanceFile;
	
	@OneToOne
	@NotNull
	private Script script;
	
	@OneToOne
	private Survey survey;
	
	@OneToOne
	private User user;
		
}

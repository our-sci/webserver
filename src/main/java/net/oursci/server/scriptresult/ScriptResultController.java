package net.oursci.server.scriptresult;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.javarosa.xform.parse.XFormParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.oursci.server.exception.ScriptNotFound;
import net.oursci.server.s3.S3Wrapper;
import net.oursci.server.script.Script;
import net.oursci.server.script.ScriptRepository;
import net.oursci.server.util.OursciUtils;


@RestController
@RequestMapping(value = "/api/script/result")
public class ScriptResultController {

	@Autowired
	S3Wrapper s3Wrapper;
	
	@Autowired
	ScriptResultRepository scriptResultRepository;
	
	@Autowired
	ScriptRepository scriptRepository;
	
	@Autowired
	OursciUtils oursciUtils;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<ScriptResult> getResults() {
		return (List<ScriptResult>) scriptResultRepository.findAll();
	}
	
	@RequestMapping(value = "/list/by-script-id/{scriptId}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public List<ScriptResult> getResultsByScriptId(@PathVariable String scriptId) {
		Script script = scriptRepository.findByScriptId(scriptId);
		return (List<ScriptResult>) scriptResultRepository.findByScript(script);
	}

	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public ScriptResult getResultById(@PathVariable("id") Long id) {
		return scriptResultRepository.findById(id).orElse(null);
	}
	
	@RequestMapping(value = "/create/{scriptId}", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	public ScriptResult createResult(@RequestParam("files") MultipartFile[] files, @PathVariable String scriptId) throws XFormParseException, IOException {
		
		Script script = scriptRepository.findByScriptId(scriptId);
		if(script == null) {
			throw new ScriptNotFound("No script found with id: "+scriptId);
		}
		
		if(files.length > 1) {
			throw new RuntimeException("Script results must only have one uploaded file");
		}
		
		String filename = files[0].getOriginalFilename();
		
		String instanceId = UUID.randomUUID().toString();
				
		String prefix = OursciUtils.getScriptResultPrefix(scriptId, instanceId);
		s3Wrapper.upload(files, prefix);
		
		ScriptResult scriptResult = new ScriptResult();
		scriptResult.setDate(new Date());
		scriptResult.setInstanceId(instanceId);
		scriptResult.setInstanceFile(filename);
		scriptResult.setScript(script);
		scriptResult.setUser(oursciUtils.getCurrentUser());
		
		
		return scriptResultRepository.save(scriptResult);
	}
	
	
	
	

}
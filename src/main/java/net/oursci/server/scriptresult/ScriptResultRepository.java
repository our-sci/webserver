package net.oursci.server.scriptresult;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import net.oursci.server.script.Script;
import net.oursci.server.survey.Survey;
import net.oursci.server.user.User;

public interface ScriptResultRepository extends CrudRepository<ScriptResult, Long> {
	
	List<ScriptResult> findBySurvey(Survey survey);
	
	List<ScriptResult> findByScript(Script script);
	
	List<ScriptResult> findByUser(User user);
	
	List<ScriptResult> findByScriptId(String scriptId);

}

package net.oursci.server.javarosa;

public class JavaRosaInit {
	public static final String[] SERIALIZABLE_CLASSES = new String[] {
			"org.javarosa.core.services.locale.TableLocaleSource", // JavaRosaCoreModule
			"org.javarosa.core.model.FormDef", "org.javarosa.core.model.SubmissionProfile", // CoreModelModule
			"org.javarosa.core.model.QuestionDef", // CoreModelModule
			"org.javarosa.core.model.GroupDef", // CoreModelModule
			"org.javarosa.core.model.instance.FormInstance", // CoreModelModule
			"org.javarosa.core.model.data.BooleanData", // CoreModelModule
			"org.javarosa.core.model.data.DateData", // CoreModelModule
			"org.javarosa.core.model.data.DateTimeData", // CoreModelModule
			"org.javarosa.core.model.data.DecimalData", // CoreModelModule
			"org.javarosa.core.model.data.GeoPointData", // CoreModelModule
			"org.javarosa.core.model.data.GeoShapeData", // CoreModelModule
			"org.javarosa.core.model.data.GeoTraceData", // CoreModelModule
			"org.javarosa.core.model.data.IntegerData", // CoreModelModule
			"org.javarosa.core.model.data.LongData", // CoreModelModule
			"org.javarosa.core.model.data.MultiPointerAnswerData", // CoreModelModule
			"org.javarosa.core.model.data.PointerAnswerData", // CoreModelModule
			"org.javarosa.core.model.data.SelectMultiData", // CoreModelModule
			"org.javarosa.core.model.data.SelectOneData", // CoreModelModule
			"org.javarosa.core.model.data.StringData", // CoreModelModule
			"org.javarosa.core.model.data.TimeData", // CoreModelModule
			"org.javarosa.core.model.data.UncastData", // CoreModelModule
			"org.javarosa.core.model.data.helper.BasicDataPointer", // CoreModelModule
			"org.javarosa.core.model.Action", // CoreModelModule
			"org.javarosa.core.model.actions.SetValueAction" // CoreModelModule
	};
}

package net.oursci.server.javarosa;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.javarosa.core.model.Constants;
import org.javarosa.core.model.FormDef;
import org.javarosa.core.model.QuestionDef;
import org.javarosa.core.model.SelectChoice;
import org.javarosa.core.model.data.IAnswerData;
import org.javarosa.core.model.data.SelectMultiData;
import org.javarosa.core.model.data.SelectOneData;
import org.javarosa.core.model.data.helper.Selection;
import org.javarosa.core.model.instance.TreeElement;
import org.javarosa.core.model.instance.utils.DefaultAnswerResolver;
import org.javarosa.core.model.utils.DateUtils;
import org.javarosa.xform.parse.XFormParser;
import org.javarosa.xform.util.XFormAnswerDataSerializer;
import org.javarosa.xpath.XPathParseTool;
import org.javarosa.xpath.expr.XPathExpression;
import org.javarosa.xpath.expr.XPathFuncExpr;
import org.javarosa.xpath.parser.XPathSyntaxException;

// copied from https://github.com/opendatakit/

public class ExternalAnswerResolver extends DefaultAnswerResolver {

	@Override
	public IAnswerData resolveAnswer(String textVal, TreeElement treeElement, FormDef formDef) {
		QuestionDef questionDef = XFormParser.ghettoGetQuestionDef(treeElement.getDataType(), formDef,
				treeElement.getRef());
		if (questionDef != null && (questionDef.getControlType() == Constants.CONTROL_SELECT_ONE
				|| questionDef.getControlType() == Constants.CONTROL_SELECT_MULTI)) {
			boolean containsSearchExpression = false;

			XPathFuncExpr xpathExpression = null;
			try {
				xpathExpression = getSearchXPathExpression(questionDef.getAppearanceAttr());
			} catch (Exception e) {
				containsSearchExpression = true;
			}

			if (xpathExpression != null || containsSearchExpression) {

				List<SelectChoice> staticChoices = questionDef.getChoices();
				for (int index = 0; index < staticChoices.size(); index++) {
					SelectChoice selectChoice = staticChoices.get(index);
					String selectChoiceValue = selectChoice.getValue();
					if (isAnInteger(selectChoiceValue)) {

						Selection selection = selectChoice.selection();

						switch (questionDef.getControlType()) {
						case Constants.CONTROL_SELECT_ONE: {
							if (selectChoiceValue.equals(textVal)) {

								if (questionDef.getControlType() == Constants.CONTROL_SELECT_ONE) {

									return new SelectOneData(selection);
								}
							}
							break;
						}
						case Constants.CONTROL_SELECT_MULTI: {

							List<String> textValues = DateUtils.split(textVal, XFormAnswerDataSerializer.DELIMITER,
									true);
							if (textValues.contains(textVal)) {

								if (selectChoiceValue.equals(textVal)) {

									List<Selection> customSelections = new ArrayList<Selection>();
									customSelections.add(selection);
									return new SelectMultiData(customSelections);
								} else {

								}
							}
							break;
						}
						default: {

							throw createBugRuntimeException(treeElement, textVal);
						}
						}

					} else {
						switch (questionDef.getControlType()) {
						case Constants.CONTROL_SELECT_ONE: {

							SelectChoice customSelectChoice = new SelectChoice(textVal, textVal, false);
							customSelectChoice.setIndex(index);
							return new SelectOneData(customSelectChoice.selection());
						}
						case Constants.CONTROL_SELECT_MULTI: {

							List<SelectChoice> customSelectChoices = createCustomSelectChoices(textVal);
							List<Selection> customSelections = new ArrayList<Selection>();
							for (SelectChoice customSelectChoice : customSelectChoices) {
								customSelections.add(customSelectChoice.selection());
							}
							return new SelectMultiData(customSelections);
						}
						default: {

							throw createBugRuntimeException(treeElement, textVal);
						}
						}

					}
				}

				throw createBugRuntimeException(treeElement, textVal);
			}
		}
		return super.resolveAnswer(textVal, treeElement, formDef);
	}

	private RuntimeException createBugRuntimeException(TreeElement treeElement, String textVal) {
		return new RuntimeException("The appearance column of the field " + treeElement.getName()
				+ " contains a search() call and the field type is " + treeElement.getDataType()
				+ " and the saved answer is " + textVal);
	}

	protected List<SelectChoice> createCustomSelectChoices(String completeTextValue) {
		List<String> textValues = DateUtils.split(completeTextValue, XFormAnswerDataSerializer.DELIMITER, true);

		int index = 0;
		List<SelectChoice> customSelectChoices = new ArrayList<SelectChoice>();
		for (String textValue : textValues) {
			SelectChoice selectChoice = new SelectChoice(textValue, textValue, false);
			selectChoice.setIndex(index++);
			customSelectChoices.add(selectChoice);
		}

		return customSelectChoices;
	}

	public static boolean isAnInteger(String value) {
		if (value == null) {
			return false;
		}

		value = value.trim();

		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private static final Pattern SEARCH_FUNCTION_REGEX = Pattern.compile("search\\(.+\\)");

	public static XPathFuncExpr getSearchXPathExpression(String appearance) {
		if (appearance == null) {
			appearance = "";
		}
		appearance = appearance.trim();

		Matcher matcher = SEARCH_FUNCTION_REGEX.matcher(appearance);
		if (matcher.find()) {
			String function = matcher.group(0);
			try {
				XPathExpression xpathExpression = XPathParseTool.parseXPath(function);
				if (XPathFuncExpr.class.isAssignableFrom(xpathExpression.getClass())) {
					XPathFuncExpr xpathFuncExpr = (XPathFuncExpr) xpathExpression;
					if (xpathFuncExpr.id.name.equalsIgnoreCase("search")) {
						// also check that the args are either 1, 4 or 6.
						if (xpathFuncExpr.args.length == 1 || xpathFuncExpr.args.length == 4
								|| xpathFuncExpr.args.length == 6) {
							return xpathFuncExpr;
						} else {
							return null;
						}
					} else {
						return null;
					}
				} else {
					return null;
				}
			} catch (XPathSyntaxException e) {
				return null;
			}
		} else {
			return null;
		}
	}

}
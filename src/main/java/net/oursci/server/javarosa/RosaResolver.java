package net.oursci.server.javarosa;

public class RosaResolver {
	/**
	 
	public static OrderedJSONObject getJsonKeysAndValuesWithRosa(FormDef formDef, InputStream formInstanceStream) {
        PrototypeManager.registerPrototypes(JavaRosaInit.SERIALIZABLE_CLASSES);
        new XFormsModule().registerModule();

        final FormEntryController fec = new FormEntryController(new FormEntryModel(formDef));
		FormIndex formIndex = fec.getModel().getFormIndex();

		if (!initInstance(fec, formInstanceStream)) {
			return new OrderedJSONObject();
		}
		
		formDef.initialize(false, new InstanceInitializationFactory());
		FormInstance formInstance = fec.getModel().getForm().getInstance();
		
		OrderedJSONObject json = new OrderedJSONObject();
		TreeElement root = formInstance.getRoot();
		String formId = getFormId(formInstance);
		String instanceID = getInstanceID(formInstance);
		String date = getModifiedTimestamp(formInstance);
		String userID = getUserID(formInstance);

		ArrayList<String> anon = new ArrayList<>();

		for (int i = 0; i < root.getNumChildren(); i++) {
			TreeElement child = root.getChildAt(i);
			if (!child.getName().equalsIgnoreCase("anonymize")) {
				break;
			}

			for (int k = 0; k < child.getNumChildren(); k++) {
				TreeElement id = child.getChildAt(k);
				if (!id.getName().equalsIgnoreCase("id")) {
					break;
				}

				try {
					anon.add((String) id.getValue().getValue());
				} catch (Exception e) {
				}
			}
		}

		int event = -1;
		while ((event = fec.getModel().getEvent(formIndex)) != FormEntryController.EVENT_END_OF_FORM) {
			switch (event) {
			case FormEntryController.EVENT_QUESTION:
				String textID = fec.getModel().getQuestionPrompt().getQuestion().getTextID();
				String key = extractName(textID);
				String value = "";
				try {
					value = (String) fec.getModel().getQuestionPrompt().getAnswerText();
				} catch (Exception e) {
					// just go on
				}

				try {
					json.put(key, value);
				} catch (org.apache.wink.json4j.JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				try {
					OrderedJSONObject object = new OrderedJSONObject(value);

					OrderedJSONObject meta = (OrderedJSONObject) object.get("meta");

					String filename = meta.getString("filename");
					if (filename != null) {
						String encoding = "UTF-8";
						String region = ApplicationInfo.REGION;
						String bucket = ApplicationInfo.BUCKET;
						String encInstanceID = URLEncoder.encode(getInstanceID(formInstance));
						String encFormId = URLEncoder.encode(getFormId(formInstance));
						String encFilename = URLEncoder.encode(filename);

						String url = String.format("https://s3.%s.amazonaws.com/%s/survey-results/%s/%s/%s", region,
								bucket, encFormId, encInstanceID, encFilename);
						meta.put("filename", url);
					}

					json.put(key, object);
				} catch (Exception e) {
					// just go on
					// e.printStackTrace();
				}

				break;
			default:
				break;
			}

			formIndex = fec.getModel().incrementIndex(formIndex);
			fec.getModel().setQuestionIndex(formIndex);
		}

		try {
			json.put("metaDate", date);
			json.put("metaInstanceID", instanceID);
			json.put("metaUserID", userID);
		} catch (org.apache.wink.json4j.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return json;
	}


*/
}

package net.oursci.server.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.javarosa.core.model.FormDef;
import org.javarosa.core.model.FormIndex;
import org.javarosa.core.model.instance.InstanceInitializationFactory;
import org.javarosa.core.util.externalizable.DeserializationException;
import org.javarosa.form.api.FormEntryModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.model.PutObjectResult;

import net.oursci.server.config.SecurityConfig;
import net.oursci.server.exception.NotOrganizationAdmin;
import net.oursci.server.exception.UserNotFound;
import net.oursci.server.javarosa.Event;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.OrganizationService;
import net.oursci.server.organization.member.OrganizationMember;
import net.oursci.server.organization.member.OrganizationMemberService;
import net.oursci.server.s3.S3Wrapper;
import net.oursci.server.user.User;
import net.oursci.server.user.UserService;

@Service
public class OursciUtils {
	
	public static final String SURVEY_DEFINITIONS = "survey-definitions";
	public static final String SURVEY_RESULTS = "survey-results";
	public static final String JSON_HEADER_FILENAME = "header.json";
	
	public static final String SCRIPT_DEFINITIONS = "script-definitions";
	public static final String SCRIPT_RESULTS = "script-results";
	
	public static final String DASHBOARD_DEFINITIONS = "dashboard-definitions";

	public static final String ORGANIZATION = "organization";
	
	public static final String USER_PROFILE = "user/profile";
	
	public static final String OURSCI_SHARE_EMAIL = "oursci_share_email";
	public static final String OURSCI_SHARE_DASHBOARD = "oursci_share_dashboard";

	
	@Autowired
	S3Wrapper s3Wrapper;
	
	@Autowired
	UserService userService;
	
	@Autowired
	OrganizationService organizationService;
	
	@Autowired
	OrganizationMemberService organizationMemberService;
	
	public static String appUrlBase;
	
	@Value("${app.url.base}")
    public void setBaseUrl(String baseUrl) {
        appUrlBase = baseUrl;
    }
	
	public User getCurrentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		return userService.getByUsername(currentPrincipalName);
	}
	
	public User getCurrentUserOrNull() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();

		User current = null;
		try {
			current = userService.getByUsername(currentPrincipalName);
		} catch(UserNotFound unf) {
			return null;
		}
		
		return current;
	}
	
	public boolean isAdmin(User user) {
		if(user == null) {
			return false;
		}
		for(GrantedAuthority ga : user.getAuthorities()) {
			if(ga.getAuthority().equals(SecurityConfig.Roles.ROLE_ADMIN)) return true;
		}
		return false;
	}
	
	public boolean isAdmin() {
		User user = getCurrentUser();
		return isAdmin(user);
	}
	
	public void assertOrganizationAdmin(String organizationId) {
		Organization organization = organizationService.getByOrganizationId(organizationId);
		assertOrganizationAdmin(organization);
	}
	
	public void assertOrganizationAdmin(String organizationId, User user) {
		Organization organization = organizationService.getByOrganizationId(organizationId);
		assertOrganizationAdmin(organization, user);
	}
	
	public void assertOrganizationAdmin(Organization organization) {
		assertOrganizationAdmin(organization, getCurrentUser());
	}
	
	public void assertOrganizationAdmin(Organization organization, User user) {
		if(user == null || organization == null) {
			throw new NotOrganizationAdmin();
		}
		
		if (isAdmin(user)) {
			return;
		}
		
		if(user == organization.getCreator()) {
			return;
		}

		OrganizationMember member = organizationMemberService.findOneByUserAndOrganization(user,
				organization);

		if(member == null) {
			throw new NotOrganizationAdmin();
		}
		
		if (member.isAdmin()) {
			return;
		}
		
		throw new NotOrganizationAdmin();
	}
	
	
	public ArrayList<String> getSurveyColumns(FormDef def) throws IOException, DeserializationException, InterruptedException {
        ArrayList<String> output = new ArrayList<>();

        FormEntryModel fem = new FormEntryModel(def);
        def.initialize(true, new InstanceInitializationFactory());

        FormIndex idx = fem.getFormIndex();

        eventLoop:
        for (; ; ) {
            Event e = Event.fromEvent(fem.getEvent(idx));
            switch (e) {
                case EVENT_BEGINNING_OF_FORM:
                    break;
                case EVENT_END_OF_FORM:
                    break eventLoop;
                case EVENT_PROMPT_NEW_REPEAT:
                    break;
                case EVENT_QUESTION:
                    output.add(fem.getQuestionPrompt().getQuestionText());
                    break;
                case EVENT_GROUP:
                    break;
                case EVENT_REPEAT:
                    break;
                case EVENT_REPEAT_JUNCTURE:
                    break;
                case EVENT_UNKNOWN:
                    break;
            }
            idx = fem.incrementIndex(idx);
            fem.setQuestionIndex(idx);
        }
        return output;
    }
	
	public PutObjectResult uploadSurveyColumns(ArrayList<String> columns, String prefix) {
		prefix = addTrailingDelimiter(prefix);
		JSONArray json = new JSONArray(columns);
		InputStream is = new ByteArrayInputStream(json.toString().getBytes());
		return s3Wrapper.upload(is, prefix + "columns.json");
	}
	
	public PutObjectResult uploadSurveyHeader(String string, String prefix) {
		InputStream is = new ByteArrayInputStream(string.toString().getBytes());
		return s3Wrapper.upload(is, prefix, JSON_HEADER_FILENAME);
	}
	
	public static String addTrailingDelimiter(String prefix) {
		if(prefix != null && !prefix.isEmpty() && !prefix.endsWith(S3Wrapper.DELIMITER)) {
			prefix += S3Wrapper.DELIMITER;
		}
		return prefix;
	}
	
	public static String getSurveyDefinitionPrefix(String formId) {
		return String.format("%s/%s/", SURVEY_DEFINITIONS, formId);
	}
	
	public static String getScriptDefinitionPrefix(String scriptId) {
		return String.format("%s/%s/", SCRIPT_DEFINITIONS, scriptId);
	}
	
	public static String getOrganizationAssetsInternalPrefix(String organizationId) {
		return String.format("%s/%s/assets/internal/", ORGANIZATION, organizationId);
	}
	
	public static String getOrganizationAssetsExternalPrefix(String organizationId) {
		return String.format("%s/%s/assets/external/", ORGANIZATION, organizationId);
	}
	
	public static String getDashboardDefinitionPrefix(String dashboardId) {
		return String.format("%s/%s/", DASHBOARD_DEFINITIONS, dashboardId);
	}
	
	public static String getSurveyResultPrefix(String formId, String instanceId) {
		return String.format("%s/%s/%s/", SURVEY_RESULTS, formId, instanceId);
	}
	
	public static String getScriptResultPrefix(String scriptId, String uid) {
		return String.format("%s/%s/%s/", SCRIPT_RESULTS, scriptId, uid);
	}
	
	public static String getUserProfilePrefix(String username) {
		return String.format("%s/%s/", USER_PROFILE, username);
	}
	
	
	public static JSONObject getJsonObject(InputStream inputStream){

	   try {
	       BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
	       StringBuilder responseStrBuilder = new StringBuilder();

	       String inputStr;
	       while ((inputStr = streamReader.readLine()) != null)
	           responseStrBuilder.append(inputStr);

	       JSONObject jsonObject = new JSONObject(responseStrBuilder.toString());

	       //returns the json object
	       return jsonObject;

	   } catch (IOException e) {
	       e.printStackTrace();
	   } catch (JSONException e) {
	       e.printStackTrace();
	   }

	    //if something went wrong, return null
	    return null;
	}
	
	
	public static List<String> convertToInternalOrganizationAssetLinks(List<String> s3Links) {
		for(int i=0; i<s3Links.size();i++) {
			String current = s3Links.get(i);
			int index = current.indexOf("/organization");
			if(index < 0) {
				continue;
			}
			String internalLink = String.format("%s/storage%s", appUrlBase, current.substring(index));
			s3Links.set(i, internalLink);
		}
		
		return s3Links;
	}
	
	/**
	 * Transforms links from Amazon S3 buckets to links pointing to this webserver's storage directory.
	 *
	 * @param s3Links Links to files inside an S3 bucket
	 * @param prefix Prefix inside bucket, e.g. "/organization" or "/survey-definitions"
	 * @return Links pointing to this webserver's storage directory
	 */
	public static List<String> transformToStorageLinks(List<String> s3Links, String prefix) {
		for(int i=0; i<s3Links.size();i++) {
			String current = s3Links.get(i);
			int index = current.indexOf(prefix);
			if(index < 0) {
				continue;
			}
			String internalLink = String.format("%s/storage%s", appUrlBase, current.substring(index));
			s3Links.set(i, internalLink);
		}
		
		return s3Links;
	}
	
	public static String convertToProfileIconLink(String s3Link) {
		String current = s3Link;
		int index = current.indexOf("/user/profile");
		if(index < 0) {
			return s3Link;
		}
		String internalLink = String.format("%s/storage%s", appUrlBase, current.substring(index));
		return internalLink;
	}
	
}

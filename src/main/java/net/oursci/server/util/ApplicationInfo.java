package net.oursci.server.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationInfo {
	public static String BUCKET;
	public static String REGION;
	public static String URL_BASE;
	
	@Value("${cloud.aws.s3.bucket}")
	public void setBucket(String bucket) {
		BUCKET = bucket;
	}
	
	@Value("${cloud.aws.region.static}")
	public void setRegion(String region) {
		REGION = region;
	}
	
	@Value("${app.url.base}")
	public void setUrlBase(String urlBase) {
		URL_BASE = urlBase;
	}
}

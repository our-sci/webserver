package net.oursci.server.util;

import org.apache.commons.lang.StringUtils;

import net.oursci.server.s3.S3Wrapper;


public class StringUtil {
	public static Boolean isBlank(String string) {
		return StringUtils.isBlank(string);
	}
	
	public static String getBasename(String key)  {
        String[] splits = key.split(S3Wrapper.DELIMITER);
        String basename = splits[splits.length - 1];
        return basename;
	}
}

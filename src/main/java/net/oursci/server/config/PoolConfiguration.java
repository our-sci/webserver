package net.oursci.server.config;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cloud.aws.jdbc.datasource.TomcatJdbcDataSourceFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PoolConfiguration implements BeanPostProcessor {

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof TomcatJdbcDataSourceFactory) {
			TomcatJdbcDataSourceFactory tomcatJdbcDataSourceFactory = (TomcatJdbcDataSourceFactory) bean;
			tomcatJdbcDataSourceFactory.setTestOnBorrow(true);
			tomcatJdbcDataSourceFactory.setTestWhileIdle(true);
			tomcatJdbcDataSourceFactory.setValidationQuery("SELECT 1");
		}
		return bean;
	}

	@Override
	public Object postProcessBeforeInitialization(Object arg0, String arg1) throws BeansException {
		// TODO Auto-generated method stub
		return arg0;
	}

	/* Might try these values as well
	 * spring.datasource.primary.max-idle=10
	 * spring.datasource.primary.max-wait=10000
	 * spring.datasource.primary.min-idle=5
	 * spring.datasource.primary.initial-size=5
	 * spring.datasource.primary.validation-query=SELECT 1
	 * spring.datasource.primary.test-on-borrow=false
	 * spring.datasource.primary.test-while-idle=true
	 * spring.datasource.primary.time-between-eviction-runs-millis=18800
	 */

}

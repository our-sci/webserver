package net.oursci.server.config;

import org.springframework.cloud.aws.jdbc.config.annotation.EnableRdsInstance;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("!local")
@Configuration
@EnableRdsInstance(dbInstanceIdentifier="${app.db.instance}", databaseName="${app.db.name}", username="${app.db.user}",password="${app.db.password}")
public class RdsConfiguration {

}

package net.oursci.server.surveyresult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;


import net.oursci.server.survey.Survey;
import net.oursci.server.user.User;

public class SurveyResultSpec {
	
	public static Specification<SurveyResult> withSurvey(Survey survey) {
		return (root, query, cb) -> {
			return cb.equal(root.get("survey"), survey);
		};
	}
	
	public static Specification<SurveyResult> withArchived(boolean archived) {
		return (root, query, cb) -> {
			return cb.equal(root.get("archived"), archived);
		};
	}
	
	public static Specification<SurveyResult> withUser(User user) {
		return (root, query, cb) -> {
			if(user == null) {
				return cb.isTrue(cb.literal(true)); // always true => no filtering
			}
			return cb.equal(root.get("user"), user);
		};
	}
	
	public static Specification<SurveyResult> withDateBetween(Date from, Date to) {
		return (root, query, cb) -> {
			final List<Predicate> predicates = new ArrayList<>();

			  if (from != null) {
			    predicates.add(cb.greaterThan(root.get("modified"), from));
			  }
			  if (to != null) {
			    predicates.add(cb.lessThan(root.get("modified"), to));
			  }

			  return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}
	
	public static Specification<SurveyResult> withInstanceIds(List<String> instanceIds) {
		return (root, query, cb) -> {
			if(instanceIds.size() == 0) {
				return cb.isTrue(cb.literal(true)); // always true => no filtering
			}
			return root.get("instanceId").in(instanceIds);
		};
	}
}

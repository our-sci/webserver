package net.oursci.server.surveyresult;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import net.oursci.server.survey.Survey;
import net.oursci.server.user.Contribution;
import net.oursci.server.user.User;

public interface SurveyResultRepository extends PagingAndSortingRepository<SurveyResult, Long>, JpaSpecificationExecutor<SurveyResult> {
	
	List<SurveyResult> findByFormId(String formId);
	List<SurveyResult> findByFormIdOrderByModifiedDesc(String formId);	
	List<SurveyResult> findByFormIdAndModifiedBetween(String formId, Date from, Date to);
	List<SurveyResult> findByFormIdAndModifiedBetweenOrderByModifiedDesc(String formId, Date from, Date to);

	List<SurveyResult> findBySurveyAndArchivedFalseOrderByModifiedDesc(Survey survey);
	List<SurveyResult> findBySurveyAndArchivedFalseOrderByModifiedDesc(Survey survey, Pageable pageable);
	
	List<SurveyResult> findBySurveyAndModifiedBetweenAndArchivedFalseOrderByModifiedDesc(Survey survey, Date from, Date to);
	List<SurveyResult> findBySurveyAndModifiedBetweenAndArchivedFalseOrderByModifiedDesc(Survey survey, Date from, Date to, Pageable pageable);
	

	List<SurveyResult> findByModifiedBetween(Date from, Date to);
	
	SurveyResult findByInstanceId(String instanceId);
	List<SurveyResult> findByInstanceIdIn(List<String> instanceIds);

	
	@Query("SELECT DISTINCT s.user FROM SurveyResult s WHERE s.formId = :formId")
	List<User> getUserByFormId(@Param("formId") String formId);
	
	@Query(	"SELECT " +
			"	new net.oursci.server.user.Contribution(sr.user, sr.survey, COUNT(sr), MAX(sr.modified) as modified) " +
			"FROM " +
			"	SurveyResult sr " +
			"LEFT JOIN " +
			"	sr.survey s " +
			"WHERE " +
			"	sr.user=:user AND s.archived=0 " +
			"GROUP BY " +
			"	sr.survey " +
			"ORDER BY " +
			"	modified DESC"
	)
	Page<Contribution> findContributionsByUser(@Param("user") User user, Pageable pageable);
	
	@Query(	"SELECT " +
			"	new net.oursci.server.user.Contribution(sr.user, sr.survey, COUNT(sr), MAX(sr.modified) as modified) " +
			"FROM " +
			"	SurveyResult sr " +
			"LEFT JOIN " +
			"	sr.survey s " +
			"WHERE " +
			"	sr.user=:user AND s.archived=0 " +
			"GROUP BY " +
			"	sr.survey " +
			"ORDER BY " +
			"	modified DESC"
	)
	List<Contribution> findContributionsByUser(@Param("user") User user);
	
	List<SurveyResult> findBySurveyAndUserAndArchivedFalseOrderByModifiedDesc(Survey survey, User user);
	List<SurveyResult> findBySurveyAndUserAndArchivedFalseOrderByModifiedDesc(Survey survey, User user, Pageable pageable);
	
	List<SurveyResult> findBySurveyAndUserAndModifiedBetweenAndArchivedFalseOrderByModifiedDesc(Survey survey, User user, Date from, Date to);
	List<SurveyResult> findBySurveyAndUserAndModifiedBetweenAndArchivedFalseOrderByModifiedDesc(Survey survey, User user, Date from, Date to, Pageable pageable);
	
	SurveyResult findTopByUserOrderByModifiedDesc(User user);
	
	
}

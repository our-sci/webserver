package net.oursci.server.organization;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonView;

import net.oursci.server.config.SecurityConfig.Roles;
import net.oursci.server.exception.BadRequest;
import net.oursci.server.exception.OrganizationAlreadyExists;
import net.oursci.server.exception.UserNotFound;
import net.oursci.server.invitation.Invitation;
import net.oursci.server.invitation.InvitationService;
import net.oursci.server.mappings.OrganizationWithFavorites;
import net.oursci.server.organization.favorite.Favorite;
import net.oursci.server.organization.favorite.OrganizationFavorite;
import net.oursci.server.organization.favorite.OrganizationFavoriteService;
import net.oursci.server.organization.member.OrganizationMember;
import net.oursci.server.organization.member.OrganizationMemberService;
import net.oursci.server.organization.preferences.OrganizationPreferences;
import net.oursci.server.organization.preferences.OrganizationPreferencesService;
import net.oursci.server.s3.S3Wrapper;
import net.oursci.server.user.User;
import net.oursci.server.user.UserService;
import net.oursci.server.util.OursciUtils;
import net.oursci.server.views.Views;

@RestController
@RequestMapping(value = "/api/organization")
public class OrganizationController {

	@Autowired
	S3Wrapper s3Wrapper;

	@Autowired
	OursciUtils oursciUtils;

	@Autowired
	OrganizationFavoriteService organizationFavoriteService;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	OrganizationMemberService organizationMemberService;

	@Autowired
	OrganizationPreferencesService organizationPreferencesService;

	@Autowired
	UserService userService;
	
	@Autowired
	InvitationService invitationService;

	@Autowired
	private ModelMapper modelMapper;

	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Organization createOrganization(@RequestParam String id, @RequestParam String name,
			@RequestParam String description, @RequestParam String content) {

		if (organizationService.exists(id)) {
			Organization existing = organizationService.getByOrganizationId(id);
			if (existing.isArchived()) {
				throw new OrganizationAlreadyExists("An organization with ID " + id
						+ " already exists. You can find it under archived organizations.");
			}
			throw new OrganizationAlreadyExists();
		}

		User currentUser = oursciUtils.getCurrentUser();

		Organization organization = new Organization();
		organization.setOrganizationId(id);
		organization.setName(name);
		organization.setDescription(description);
		organization.setContent(content);
		organization.setDate(new Date());
		organization.setCreator(currentUser);

		organization = organizationService.save(organization);

		OrganizationMember member = new OrganizationMember();
		member.setAdmin(true);
		member.setUser(currentUser);
		member.setOrganization(organization);
		organizationMemberService.save(member);

		return organization;
	}

	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Organization updateOrganization(@RequestParam String id, @RequestParam(required = false) String name,
			@RequestParam(required = false) String description, @RequestParam(required = false) String content,
			@RequestParam(required = false) MultipartFile[] icon, @RequestParam(required = false) Boolean clearIcon,
			@RequestParam(required = false) Boolean archived) {

		Organization organization = organizationService.getByOrganizationId(id);

		oursciUtils.assertOrganizationAdmin(organization);

		// set new values
		if (name != null)
			organization.setName(name);
		if (description != null)
			organization.setDescription(description);
		if (content != null)
			organization.setContent(content);
		if (archived != null)
			organization.setArchived(archived);

		if (icon != null && icon.length == 1) {
			String filename = icon[0].getOriginalFilename();
			System.out.println("There is a file with name " + filename);
			String prefix = OursciUtils.getOrganizationAssetsInternalPrefix(id);
			s3Wrapper.upload(icon, prefix);
			String key = prefix + filename;
			String iconURL = s3Wrapper.getURL(key);
			organization.setIcon(iconURL);
		}

		if (clearIcon != null && clearIcon == true) {
			// TODO: delete icon on S3
			organization.setIcon(null);
		}

		return organizationService.save(organization);
	}

	@RequestMapping(value = "/id/{organizationId}/assets", method = RequestMethod.POST)
	public List<String> postAssets(@PathVariable String organizationId, @RequestParam MultipartFile[] files,
			@RequestParam(required = false, defaultValue = "false") Boolean override) {
		oursciUtils.assertOrganizationAdmin(organizationId);

		String prefix = OursciUtils.getOrganizationAssetsExternalPrefix(organizationId);

		List<String> uploadedAssets = s3Wrapper.getBaseNames(prefix);

		for (MultipartFile file : files) {
			System.out.println(file.getOriginalFilename());
			if (uploadedAssets.contains(file.getOriginalFilename()) && !override) {
				throw new BadRequest("Asset already exists: " + file.getOriginalFilename());
			}
		}

		s3Wrapper.upload(files, prefix);

		return OursciUtils.convertToInternalOrganizationAssetLinks(s3Wrapper.getURLs(prefix));
	}

	@RequestMapping(value = "/id/{organizationId}/assets", method = RequestMethod.GET)
	public List<String> getAssets(@PathVariable String organizationId) {
		String prefix = OursciUtils.getOrganizationAssetsExternalPrefix(organizationId);
		return OursciUtils.convertToInternalOrganizationAssetLinks(s3Wrapper.getURLs(prefix));
	}

	@RequestMapping(value = "/id/{organizationId}/assets/remove", method = RequestMethod.GET)
	public List<String> removeAsset(@PathVariable String organizationId, String name) {
		oursciUtils.assertOrganizationAdmin(organizationId);

		String prefix = OursciUtils.getOrganizationAssetsExternalPrefix(organizationId);
		s3Wrapper.deleteObject(prefix, name);

		return OursciUtils.convertToInternalOrganizationAssetLinks(s3Wrapper.getURLs(prefix));
	}

	@RequestMapping(value = "/id/{organizationId}/preferences", method = RequestMethod.POST)
	public OrganizationPreferences postPreferences(@PathVariable String organizationId, @RequestParam(required = false, defaultValue = "false") Boolean clear,
			@RequestBody OrganizationPreferences preferences) {
		Organization organization = organizationService.getByOrganizationId(organizationId);

		oursciUtils.assertOrganizationAdmin(organization);

		if(clear) {
			System.out.println("Unsetting preferences for "+organization.getOrganizationId());
			organization.setPreferences(null);
			organizationService.save(organization);
			return null;
		}

		OrganizationPreferences np = organizationPreferencesService.save(preferences);
		organization.setPreferences(np);
		organizationService.save(organization);
		return np;

	}

	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	@RequestMapping(value = "/removeMember", method = RequestMethod.POST)
	public Organization removeMember(@RequestParam Long id, @RequestParam String organizationId) {
		Organization organization = organizationService.getByOrganizationId(organizationId);

		OrganizationMember member = organizationMemberService.find(id);
		if (member == null) {
			throw new BadRequest("no organization member found with id " + id);
		}

		// any user should be able to remove self, otherwise check for admin rights
		if (member.getUser() != oursciUtils.getCurrentUser()) {
			oursciUtils.assertOrganizationAdmin(organizationId);
		}
		
		List<Invitation> invitations = invitationService.findByMember(member);
		for(Invitation invitation: invitations) {
			invitation.setMember(null);
			invitationService.save(invitation);
		}

		if (member.getUser() == organization.getCreator()) {
			throw new BadRequest("One does not simply remove this organization's creator.");
		}
		
		

		organizationMemberService.delete(member);

		return organizationService.save(organization);
	}

	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	@RequestMapping(value = "/addMember", method = RequestMethod.POST)
	public OrganizationMember addMember(@RequestParam String id, @RequestParam String email,
			@RequestParam String description, @RequestParam(defaultValue = "0") Long role,
			@RequestParam boolean admin) {
		Organization organization = organizationService.getByOrganizationId(id);
		
		email = email.trim();

		oursciUtils.assertOrganizationAdmin(organization);

		User user = userService.findOneByEmail(email);
		if (user == null) {
			throw new UserNotFound("No user found with email " + email);
		}

		OrganizationMember member = new OrganizationMember();
		member.setUser(user);
		member.setOrganization(organization);
		member.setDescription(description);
		member.setRole(role);
		member.setAdmin(admin);

		return organizationMemberService.add(member);
	}

	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	@RequestMapping(value = "/updateMember", method = RequestMethod.POST)
	public OrganizationMember updateMember(@RequestParam Long id, @RequestParam String description,
			@RequestParam(defaultValue = "0") Long role, @RequestParam Boolean admin) {

		OrganizationMember member = organizationMemberService.find(id);
		Organization organization = member.getOrganization();

		oursciUtils.assertOrganizationAdmin(organization);

		member.setDescription(description);
		member.setRole(role);
		if (!admin && (member.getUser() == organization.getCreator())) {
			throw new BadRequest("One does not simply revoke admin rights to this organization's creator.");
		}
		member.setAdmin(admin);

		return organizationMemberService.save(member);
	}

	@RequestMapping("/list")
	public Iterable<Organization> list(@RequestParam(required = false) String username) {
		if (username != null) {
			User user = userService.getByUsername(username);
			return organizationService.findByUser(user);
		}
		return organizationService.findAll();
	}

	@RequestMapping("/id/{organizationId}")
	public OrganizationWithFavorites getOneWithFavorites(@PathVariable String organizationId) {
		Organization organization = organizationService.getByOrganizationId(organizationId);
		OrganizationWithFavorites organizationWithFavorites = modelMapper.map(organization,
				OrganizationWithFavorites.class);
		organizationWithFavorites.setFavorites(createFavoriteMap(organization));

		return organizationWithFavorites;
	}

	private HashMap<String, List<OrganizationFavorite>> createFavoriteMap(Organization organization) {
		HashMap<String, List<OrganizationFavorite>> map = new HashMap<>();

		for (Favorite.TYPE type : Favorite.TYPE.values()) {
			List<OrganizationFavorite> list = organizationFavoriteService.find(organization, type);
			map.put(type.value(), list);
		}

		return map;
	}

	/* Not sure if this is needed anymore */
	/* => Seems to be used inside Android application!! */
	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	@JsonView(Views.Summary.class)
	@RequestMapping("/by-user")
	public Organization[] organizationByMember() {
		
		User user = oursciUtils.getCurrentUser();

		List<OrganizationMember> members = organizationMemberService.findByUser(user);

		final Set<Organization> res = new HashSet<Organization>();

		for (OrganizationMember e : members) {
			res.add(e.getOrganization());
		}

		return res.toArray(new Organization[] {});
	}


	@GetMapping("/find")
	public Page<Organization> findOrganizations(@RequestParam(required = false, defaultValue = "") String search, @RequestParam(required = false) String organization,
			@RequestParam(required = false, defaultValue = "false") boolean archived, Pageable pageable) {
		if (archived) {
			return organizationService.findArchived(search, pageable);
		}
		
		return organizationService.find(search, pageable);
	}
	

}
package net.oursci.server.organization.favorite;

import java.util.List;

import net.oursci.server.organization.Organization;

public interface OrganizationFavoriteService {
	List<OrganizationFavorite> findAll();

	OrganizationFavorite save(OrganizationFavorite favorite);
	void delete(OrganizationFavorite favorite);
	void delete(Long id);
	
	OrganizationFavorite findOne(Long id);
	
	List<OrganizationFavorite> find(Organization owner);
	List<OrganizationFavorite> find(Organization owner, Favorite.TYPE type);
	
	OrganizationFavorite add(Organization owner, Favorite.TYPE type, String typeIdentifier, String description, Integer priority);
}

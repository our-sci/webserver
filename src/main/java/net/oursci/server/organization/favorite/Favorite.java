package net.oursci.server.organization.favorite;

public interface Favorite {
	// do not edit existing enum values! just add new ones if needed
	public enum TYPE {
		SURVEY("survey"), SURVEYRESULT("surveyresult"), SCRIPT("script"), DASHBOARD("dashboard");

		private String type;

		TYPE(String type) {
			this.type = type;
		}

		public static TYPE fromString(String string) {
			for (TYPE type : TYPE.values()) {
				if (string.equalsIgnoreCase(type.value()))
					return type;
			}
			return null;
		}

		public String value() {
			return type;
		}
	}
}

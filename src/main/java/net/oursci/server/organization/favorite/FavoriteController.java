package net.oursci.server.organization.favorite;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.oursci.server.config.SecurityConfig.Roles;
import net.oursci.server.exception.BadRequest;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.OrganizationService;
import net.oursci.server.util.OursciUtils;

@RestController
@RequestMapping(value = "/api/favorite")
public class FavoriteController {

	@Autowired
	OursciUtils oursciUtils;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	OrganizationFavoriteService organizationFavoriteService;

	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	@PostMapping("/organization")
	public OrganizationFavorite postOrganizationFavorite(@RequestParam String organizationId, @RequestParam String type,
			@RequestParam String typeIdentifier, @RequestParam(required = false) String description,
			@RequestParam(required = false) Integer priority) {

		Favorite.TYPE resolvedType = Favorite.TYPE.fromString(type);
		if (resolvedType == null) {
			throw new BadRequest("Not a known type: " + type);
		}

		Organization organization = organizationService.getByOrganizationId(organizationId);

		return organizationFavoriteService.add(organization, resolvedType, typeIdentifier, description, priority);
	}

	@GetMapping("/organization")
	public List<OrganizationFavorite> organizationFavoriteList() {
		return organizationFavoriteService.findAll();
	}
	
	@PostMapping("/organization/remove")
	public void removeOrganizationFavorite(@RequestParam String organizationId, @RequestParam Long id) {
		// TODO: check privileges
		organizationFavoriteService.delete(id);
	}
	
	@Secured(value = { Roles.ROLE_ADMIN, Roles.ROLE_USER })
	@PostMapping(value = "/organization/update")
	public OrganizationFavorite updateOrganizationFavorite(@RequestParam Long id,
			@RequestParam String description, @RequestParam(defaultValue = "0") Integer priority) {
		
		// TODO: check privileges
		OrganizationFavorite favorite = organizationFavoriteService.findOne(id);
		favorite.setDescription(description);
		favorite.setPriority(priority);
		
		return organizationFavoriteService.save(favorite);
	}

	@RequestMapping("/organization/id/{id}")
	public OrganizationFavorite showDetail(@PathVariable Long id) {
		return organizationFavoriteService.findOne(id);
	}

}
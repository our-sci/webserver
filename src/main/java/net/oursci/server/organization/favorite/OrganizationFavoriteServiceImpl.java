package net.oursci.server.organization.favorite;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.dashboard.DashboardService;
import net.oursci.server.exception.BadRequest;
import net.oursci.server.exception.FavoriteNotFound;
import net.oursci.server.organization.Organization;
import net.oursci.server.organization.favorite.Favorite.TYPE;
import net.oursci.server.script.Script;
import net.oursci.server.script.ScriptService;
import net.oursci.server.survey.Survey;
import net.oursci.server.survey.SurveyService;
import net.oursci.server.surveyresult.SurveyResult;
import net.oursci.server.surveyresult.SurveyResultService;

@Service
public class OrganizationFavoriteServiceImpl implements OrganizationFavoriteService {

	@Autowired
	OrganizationFavoriteRepository organizationFavoriteRepository;

	@Autowired
	DashboardService dashboardService;

	@Autowired
	ScriptService scriptService;

	@Autowired
	SurveyService surveyService;

	@Autowired
	SurveyResultService surveyResultService;

	@Override
	public List<OrganizationFavorite> find(Organization organization) {
		return organizationFavoriteRepository.findByOwner(organization);
	}

	@Override
	public List<OrganizationFavorite> find(Organization organization, TYPE type) {
		return organizationFavoriteRepository.findByOwnerAndTypeOrderByPriorityDesc(organization, type.value());
	}

	@Override
	public OrganizationFavorite add(Organization owner, Favorite.TYPE type, String typeIdentifier, String description,
			Integer priority) {
		OrganizationFavorite favorite = new OrganizationFavorite();

		switch (type) {
		case DASHBOARD:
			Dashboard dashboard = dashboardService.findByIdentifier(typeIdentifier);
			favorite.setDashboard(dashboard);
			break;
		case SCRIPT:
			Script script = scriptService.findByIdentifier(typeIdentifier);
			favorite.setScript(script);
			break;
		case SURVEY:
			Survey survey = surveyService.getByIdentifier(typeIdentifier);
			favorite.setSurvey(survey);
			break;
		case SURVEYRESULT:
			SurveyResult surveyResult = surveyResultService.findByIdentifier(typeIdentifier);
			favorite.setSurveyResult(surveyResult);
			break;
		default:
			throw new BadRequest("Missing switch case for Favorite.TYPE: " + type.value());
		}

		favorite.setOwner(owner);
		favorite.setType(type.value());
		favorite.setTypeIdentifier(typeIdentifier);
		favorite.setPriority(priority);
		favorite.setDescription(description);

		return organizationFavoriteRepository.save(favorite);
	}

	@Override
	public OrganizationFavorite findOne(Long id) {
		return organizationFavoriteRepository.findById(id).orElseThrow(() -> new FavoriteNotFound());
	}

	@Override
	public OrganizationFavorite save(OrganizationFavorite favorite) {
		return organizationFavoriteRepository.save(favorite);
	}

	@Override
	public List<OrganizationFavorite> findAll() {
		return organizationFavoriteRepository.findAll();
	}

	@Override
	public void delete(OrganizationFavorite favorite) {
		organizationFavoriteRepository.delete(favorite);
	}

	@Override
	public void delete(Long id) {
		organizationFavoriteRepository.deleteById(id);
	}

}

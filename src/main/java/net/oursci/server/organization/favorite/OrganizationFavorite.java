package net.oursci.server.organization.favorite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import net.oursci.server.dashboard.Dashboard;
import net.oursci.server.organization.Organization;
import net.oursci.server.script.Script;
import net.oursci.server.survey.Survey;
import net.oursci.server.surveyresult.SurveyResult;


@Data
@Entity
@Table(name = "organizationfavorite")
public class OrganizationFavorite {

	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToOne
	private Organization owner;
	
	private String description;
	
	private Integer priority;

	private String type;
	
	private String typeIdentifier;

	@OneToOne
	private Survey survey;

	@OneToOne
	private SurveyResult surveyResult;

	@OneToOne
	private Script script;

	@OneToOne
	private Dashboard dashboard;

	private boolean archived = false;

	
}
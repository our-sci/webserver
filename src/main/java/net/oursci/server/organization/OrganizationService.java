package net.oursci.server.organization;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import net.oursci.server.user.User;

public interface OrganizationService {
	
	/**
	 * Get the organization from the id, throws a runtime {@link net.oursci.server.exception.OrganizationNotFound} error if not found
	 * 
	 * @param organizationId
	 * @return The organization with corresponding organization id
	 */
	Organization getByOrganizationId(String organizationId);
	
	Organization save(Organization organization);
	List<Organization> findAll();
	Organization findOne(Long id);
	
	
	/**
	 * Try to find an organization by organizationId. May return null if not found.
	 * 
	 * @param organizationId
	 * @return The organization or null
	 */
	Organization findByOrganizationId(String organizationId);
	List<Organization> findByUser(User user);
	boolean exists(Long id);
	boolean exists(String organizationId);
	
	Page<Organization> find(String searchTerm, Pageable pageable);
	Page<Organization> findArchived(String searchTerm, Pageable pageable);
}

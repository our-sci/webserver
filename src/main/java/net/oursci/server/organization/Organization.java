package net.oursci.server.organization;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import net.oursci.server.invitation.Invitation;
import net.oursci.server.organization.member.OrganizationMember;
import net.oursci.server.organization.preferences.OrganizationPreferences;
import net.oursci.server.user.User;
import net.oursci.server.views.Views;

@Data
@Entity
@Table(name = "organization")
public class Organization {

	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.Summary.class)
	private Long id;

	@Column(name = "organizationid", unique = true)
	@JsonView(Views.Summary.class)
	private String organizationId;

	@Column(name = "name")
	@JsonView(Views.Summary.class)
	private String name;

	@Column(name = "description")
	@JsonView(Views.Summary.class)
	private String description;

	@Column(name = "content")
	@JsonView(Views.Detail.class)
	@Lob
	private String content;

	@Column(name = "date")
	@JsonView(Views.Summary.class)
	private Date date;
	
	@Column(name = "icon")
	@JsonView(Views.Summary.class)
	private String icon;

	@OneToMany(mappedBy = "organization")
	@JsonView(Views.Detail.class)
	private List<OrganizationMember> members = new ArrayList<OrganizationMember>();

	@OneToMany(mappedBy = "organization")
	@OrderBy("issued DESC")
	@JsonView(Views.Detail.class)
	@JsonBackReference
	private List<Invitation> invitations = new ArrayList<Invitation>();
	
	@OneToOne
	@JsonView(Views.Summary.class)
	private User creator;
	
	@OneToOne
	@JsonView(Views.Detail.class)
	private OrganizationPreferences preferences;
	
	@JsonView(Views.Summary.class)
	private boolean archived = false;
}

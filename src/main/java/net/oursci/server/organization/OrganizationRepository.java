package net.oursci.server.organization;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import net.oursci.server.user.User;

public interface OrganizationRepository extends CrudRepository<Organization, Long> {

	List<Organization> findAll();
	Organization findOneByOrganizationId(String organizationId);
	
	@Query("SELECT DISTINCT om.organization FROM OrganizationMember om WHERE om.user = :user")
	List<Organization> findByUser(@Param("user") User user);
	
	Page<Organization> findByNameContainingAndArchivedFalse(String name, Pageable pageable);
	Page<Organization> findByNameContainingAndArchivedTrue(String name, Pageable pageable);
	
	Page<Organization> findByNameContainingOrOrganizationIdIsAndArchivedFalse(String search, String organizationId, Pageable pageable);
	
	@Query("SELECT o from Organization o WHERE (name LIKE CONCAT('%',:search,'%') OR organizationId = :search) AND archived = :archived")
	Page<Organization> find(@Param("search") String search, @Param("archived") boolean archived, Pageable pageable);


}

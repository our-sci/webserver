package net.oursci.server.organization.member;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import net.oursci.server.organization.Organization;
import net.oursci.server.user.User;

public interface OrganizationMemberRepository extends CrudRepository<OrganizationMember, Long> {
	List<OrganizationMember> findByUser(User user);
	OrganizationMember findOneByUserAndOrganization(User user, Organization organzation);
}

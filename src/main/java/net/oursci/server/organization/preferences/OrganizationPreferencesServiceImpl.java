package net.oursci.server.organization.preferences;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganizationPreferencesServiceImpl implements OrganizationPreferencesService {
	
	@Autowired
	OrganizationPreferencesRepository repo;

	@Override
	public OrganizationPreferences save(OrganizationPreferences preferences) {
		return repo.save(preferences);
	}

	@Override
	public void delete(Long id) {
		repo.deleteById(id);
	}

	@Override
	public void delete(OrganizationPreferences preferences) {
		repo.delete(preferences);
	}

}

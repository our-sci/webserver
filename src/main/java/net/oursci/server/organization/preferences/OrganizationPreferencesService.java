package net.oursci.server.organization.preferences;

public interface OrganizationPreferencesService {
	
	OrganizationPreferences save(OrganizationPreferences preferences);
	
	void delete(Long id);
	void delete(OrganizationPreferences preferences);
	
}

package net.oursci.server.organization.preferences;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import net.oursci.server.views.Views;

@Data
@Entity
@Table(name = "organization_preferences")
public class OrganizationPreferences {
	
	@Id()
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.Summary.class)
	private Long id;
	
	@Column
	@JsonView(Views.Summary.class)
	private String colorPrimary;
	
	@Column
	@JsonView(Views.Summary.class)
	private String colorSecondary;
	
	@Column
	@JsonView(Views.Summary.class)
	private String colorTertiary;
	
	@Column
	@JsonView(Views.Summary.class)
	private String colorTextPrimary;
	
	@Column
	@JsonView(Views.Summary.class)
	private String colorTextPrimaryHover;
	
	@Column
	@JsonView(Views.Summary.class)
	private String colorTextSecondary;
	
	@Column
	@JsonView(Views.Summary.class)
	private String colorTextSecondaryHover;
	
	@Column
	@JsonView(Views.Summary.class)
	private String borderWidth;
	
	@Column
	@JsonView(Views.Summary.class)
	private String title;
	
	@Column
	@JsonView(Views.Summary.class)
	private String shortTitle;
	
	@Column
	@JsonView(Views.Summary.class)
	private String app;
	
	
	@Column
	@JsonView(Views.Summary.class)
	private Boolean showIcon;
	

}

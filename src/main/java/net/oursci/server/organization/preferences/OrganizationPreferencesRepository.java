package net.oursci.server.organization.preferences;

import org.springframework.data.repository.CrudRepository;

public interface OrganizationPreferencesRepository extends CrudRepository<OrganizationPreferences, Long> {

}

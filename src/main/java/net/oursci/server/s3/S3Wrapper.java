package net.oursci.server.s3;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;

@Service
public class S3Wrapper {

	public static final String DELIMITER = "/";

	@Autowired
	private AmazonS3Client amazonS3Client;

	@Value("${cloud.aws.s3.bucket}")
	private String bucket;

	public PutObjectResult upload(String filePath, String uploadKey) throws FileNotFoundException {
		return upload(new FileInputStream(filePath), uploadKey);
	}

	private String getContentType(String uploadKey) {
		String key = uploadKey.toLowerCase();
		
		if (key.endsWith("xml")) {
			return "text/xml";
		}
		if (key.endsWith("json")) {
			return "application/json";
		}

		if (key.endsWith("jpg") || key.endsWith("jpeg") || key.endsWith("jpe")) {
			return "image/jpeg";
		}
		if (key.endsWith("png")) {
			return "image/png";
		}

		if (key.endsWith("bmp")) {
			return "image/bmp";
		}

		if (key.endsWith("js")) {
			return "application/javascript";
		}
		
		if(key.endsWith("md")) {
			return "text/markdown";
		}

		return "application/octet-stream";
	}

	public void serveFile(String key, HttpServletResponse response) {
		InputStream is = get(key);
		try {
			String contentType = getContentType(key);
			response.setContentType(contentType);
			IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public PutObjectResult upload(InputStream inputStream, String uploadKey) {
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, uploadKey, inputStream, new ObjectMetadata());

		// putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead); // with S3
		// private buckets, this would generate an error
		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentType(getContentType(uploadKey));
		putObjectRequest.setMetadata(meta);

		PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);
		IOUtils.closeQuietly(inputStream);

		return putObjectResult;
	}

	public PutObjectResult upload(InputStream inputStream, String prefix, String name) {
		prefix = addTrailingDelimiter(prefix);
		String uploadKey = prefix + name;

		return upload(inputStream, uploadKey);
	}

	public List<PutObjectResult> upload(MultipartFile[] multipartFiles, String prefix) {
		String prefixAndDelimiter = addTrailingDelimiter(prefix);

		List<PutObjectResult> putObjectResults = new ArrayList<>();

		Arrays.stream(multipartFiles).filter(multipartFile -> !StringUtils.isEmpty(multipartFile.getOriginalFilename()))
				.forEach(multipartFile -> {
					try {
						putObjectResults.add(upload(multipartFile.getInputStream(),
								prefixAndDelimiter + multipartFile.getOriginalFilename()));
					} catch (IOException e) {
						e.printStackTrace();
					}
				});

		return putObjectResults;
	}

	public ResponseEntity<byte[]> download(String key) throws IOException {
		GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);

		S3Object s3Object = amazonS3Client.getObject(getObjectRequest);

		S3ObjectInputStream objectInputStream = s3Object.getObjectContent();

		byte[] bytes = IOUtils.toByteArray(objectInputStream);

		String fileName = URLEncoder.encode(key, "UTF-8").replaceAll("\\+", "%20");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		httpHeaders.setContentLength(bytes.length);
		httpHeaders.setContentDispositionFormData("attachment", fileName);

		return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.OK);
	}

	public InputStream get(String key) {
		GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);

		// System.out.println("Trying to get S3 key: "+key);

		S3Object s3Object = amazonS3Client.getObject(getObjectRequest); // this takes around 100-200ms
		S3ObjectInputStream objectInputStream = s3Object.getObjectContent();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		byte[] buffer = new byte[1024];
		int len;
		try {
			while ((len = objectInputStream.read(buffer)) > -1) {
				baos.write(buffer, 0, len);
			}
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			baos.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Open new InputStreams using the recorded bytes
		// Can be repeated as many times as you wish
		InputStream inputStream = new ByteArrayInputStream(baos.toByteArray());
		try {
			s3Object.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return inputStream;
	}

	public List<InputStream> getMultiple(List<String> keys) throws InterruptedException, ExecutionException {
		ExecutorService executor = Executors.newFixedThreadPool(32);

		List<Callable<InputStream>> callables = new ArrayList<>();

		for (final String key : keys) {
			Callable<InputStream> callable = () -> {
				InputStream inputStream = get(key);
				return inputStream;
			};
			callables.add(callable);
		}

		List<Future<InputStream>> futures = executor.invokeAll(callables);

		// wait for execution
		executor.shutdown();
		executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);

		List<InputStream> streams = new ArrayList<>();
		for (Future<InputStream> future : futures) {
			streams.add(future.get());
		}

		return streams;
	}

	public List<InputStream> downloadList(List<String> keys) {
		Long t0 = System.currentTimeMillis();
		// TransferManager tx =
		// TransferManagerBuilder.standard().withS3Client(amazonS3Client).build();
		TransferManager tx = TransferManagerBuilder.standard().build();

		List<InputStream> streams = new ArrayList<>();
		List<Download> downloads = new ArrayList<>();

		for (final String key : keys) {
			GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);
			Download d = tx.download(getObjectRequest, new File(key));
			System.out.println("adding download for " + key);
			downloads.add(d);
		}

		for (Download d : downloads) {
			try {
				System.out.println("wait for " + d.getKey());
				d.waitForCompletion();
			} catch (AmazonClientException | InterruptedException e) {
				e.printStackTrace();
			}
		}

		for (Download d : downloads) {
			File file = new File(d.getKey());
			try {
				InputStream inputStream = new FileInputStream(file);
				streams.add(inputStream);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		Long t1 = System.currentTimeMillis();
		System.out.println("downloadList took " + (t1 - t0));

		return streams;
	}

	public ListingInfo list(String prefix) {
		prefix = addTrailingDelimiter(prefix);

		ListObjectsRequest lor = new ListObjectsRequest().withBucketName(bucket).withPrefix(prefix)
				.withDelimiter(DELIMITER);

		ObjectListing objectListing = amazonS3Client.listObjects(lor);

		List<S3ObjectSummary> s3ObjectSummaries = objectListing.getObjectSummaries();
		List<String> commonPrefixes = objectListing.getCommonPrefixes();

		return new ListingInfo(s3ObjectSummaries, commonPrefixes);
	}

	public List<String> getKeys(String prefix) {
		ArrayList<String> keys = new ArrayList<>();

		ListObjectsRequest lor = new ListObjectsRequest().withBucketName(bucket).withPrefix(prefix)
				.withDelimiter(DELIMITER);
		ObjectListing objectListing = amazonS3Client.listObjects(lor);
		List<S3ObjectSummary> s3ObjectSummaries = objectListing.getObjectSummaries();

		for (S3ObjectSummary summary : s3ObjectSummaries) {
			keys.add(summary.getKey());
		}

		return keys;
	}

	public List<String> getBaseNames(String prefix) {
		List<String> fullKeys = getKeys(prefix);

		ArrayList<String> baseNames = new ArrayList<>();
		for (String fullKey : fullKeys) {
			String[] splits = fullKey.split(DELIMITER);
			baseNames.add(splits[splits.length - 1]);
		}

		return baseNames;
	}

	public URL getSignedDownloadUrl(String key) {
		GeneratePresignedUrlRequest gpur = new GeneratePresignedUrlRequest(bucket, key);
		gpur.setMethod(HttpMethod.GET);
		gpur.setExpiration(DateTime.now().plusDays(1).toDate());
		URL signedUrl = amazonS3Client.generatePresignedUrl(gpur);
		return signedUrl;
	}

	/*
	 * public List<DownloadListing> listLinks() { List<DownloadListing> links = new
	 * LinkedList<>();
	 * 
	 * ObjectListing objectListing = amazonS3Client.listObjects(new
	 * ListObjectsRequest().withBucketName(bucket)); List<S3ObjectSummary>
	 * s3ObjectSummaries = objectListing.getObjectSummaries();
	 * 
	 * for (S3ObjectSummary summary : s3ObjectSummaries) {
	 * GeneratePresignedUrlRequest gpur = new GeneratePresignedUrlRequest(bucket,
	 * summary.getKey()); gpur.setMethod(HttpMethod.GET);
	 * gpur.setExpiration(DateTime.now().plusDays(1).toDate()); URL signedUrl =
	 * amazonS3Client.generatePresignedUrl(gpur); links.add(new
	 * DownloadListing(summary.getKey(), signedUrl.toString())); }
	 * 
	 * return links; }
	 */

	private String addTrailingDelimiter(String prefix) {
		if (prefix != null && !prefix.isEmpty() && !prefix.endsWith(DELIMITER)) {
			prefix += DELIMITER;
		}
		return prefix;
	}

	public void deleteKey(String key) {
		System.out.println("Deleting object with key " + key);
		amazonS3Client.deleteObject(bucket, key);
	}

	public void deleteObject(String prefix, String name) {
		String key = addTrailingDelimiter(prefix) + name;
		System.out.println("Deleting object with key " + key);
		amazonS3Client.deleteObject(bucket, key);
	}

	public String getURL(String key) {
		return amazonS3Client.getResourceUrl(bucket, key);
	}

	public List<String> getURLs(String prefix) {
		ArrayList<String> urls = new ArrayList<>();

		ListObjectsRequest lor = new ListObjectsRequest().withBucketName(bucket).withPrefix(prefix)
				.withDelimiter(DELIMITER);
		ObjectListing objectListing = amazonS3Client.listObjects(lor);
		List<S3ObjectSummary> s3ObjectSummaries = objectListing.getObjectSummaries();

		for (S3ObjectSummary summary : s3ObjectSummaries) {
			urls.add(amazonS3Client.getResourceUrl(bucket, summary.getKey()));
		}

		return urls;
	}

}
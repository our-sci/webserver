package net.oursci.server.s3;

import java.util.List;

import com.amazonaws.services.s3.model.S3ObjectSummary;

public class ListingInfo {
	public final List<S3ObjectSummary> s3ObjectSummaries;
	public final List<String> commonPrefixes;
	
	public ListingInfo(List<S3ObjectSummary> s3ObjectSummaries, List<String> commonPrefixes) {
		super();
		this.s3ObjectSummaries = s3ObjectSummaries;
		this.commonPrefixes = commonPrefixes;
	}
	
	
}

package net.oursci.server.s3;

public class DownloadListing {
	
	public final String key;
	public final String link;
	
	public DownloadListing(String key, String link) {
		super();
		this.key = key;
		this.link = link;
	}
	
}

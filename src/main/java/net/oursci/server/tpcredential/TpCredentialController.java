package net.oursci.server.tpcredential;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.oursci.server.exception.BadRequest;
import net.oursci.server.exception.NotAdmin;
import net.oursci.server.user.User;
import net.oursci.server.user.UserService;
import net.oursci.server.util.OursciUtils;

@RestController
@RequestMapping(value = "/api/tpcredential")
public class TpCredentialController {

	@Autowired
	TpCredentialService service;

	@Autowired
	UserService userService;

	@Autowired
	OursciUtils utils;

	@GetMapping("/find")
	Page<TpCredential> find(@RequestParam(defaultValue = "") String search, @RequestParam(required = false) String uid,
			Pageable pageable) {
		User current = utils.getCurrentUser();

		if (uid != null) {
			User user = userService.getByUsername(uid);
			if (current == user || utils.isAdmin(current)) {
				return service.find(search, user, pageable);
			}
		}

		if (utils.isAdmin(current)) {
			return service.find(search, pageable);
		}

		throw new NotAdmin();
	}

	@GetMapping
	public TpCredential getOne(@RequestParam Long id) {
		TpCredential tpc = service.getById(id);
		User current = utils.getCurrentUser();
		if (current == tpc.getUser()) {
			return tpc;
		}

		if (utils.isAdmin(current)) {
			return tpc;
		}

		throw new NotAdmin();
	}

	@Value("${app.farmos.default.user}")
	private String FARMOS_DEFAULT_USER;
	
	@Value("${app.farmos.default.password}")
	private String FARMOS_DEFAULT_PASSWORD;
	
	@GetMapping("/self/list")
	public List<TpCredential> getPersonalList() {
		User user = utils.getCurrentUser();
		List<TpCredential> list = service.findByUser(user);
		list.forEach(credential -> {
			if(credential.getType() == TpCredentialType.FARMOS) {
				if(credential.getUsername().equals(FARMOS_DEFAULT_USER) && credential.getPassword().isEmpty()) {
					credential.setPassword(FARMOS_DEFAULT_PASSWORD);
				}
			}
		});
		return list;
	}
	
	@PostMapping
	public TpCredential post(@RequestBody TpCredential tpc) {
		User current = utils.getCurrentUser();

		final Long id = tpc.getId();
		if (id != null && service.exists(id)) {
			TpCredential existing = service.getById(id);
			if ((existing.getUser() == current && current.getId().equals(tpc.getUser().getId())) || utils.isAdmin(current)) {
				return service.save(tpc);
			}
		}

		if (current.getId().equals(tpc.getUser().getId()) || utils.isAdmin(current)) {
			return service.save(tpc);
		}

		throw new BadRequest();
	}

	@DeleteMapping
	public void delete(@RequestBody TpCredential tpc) {
		User current = utils.getCurrentUser();

		if (current.getId().equals(tpc.getUser().getId()) || utils.isAdmin(current)) {
			service.delete(tpc.getId());
			return;
		}
		throw new NotAdmin();
	}

}

package net.oursci.server.tpcredential;

public enum TpCredentialType {
	DEFAULT("DEFAULT"),
	FARMOS("FARMOS"),
	FARMOS_AGGREGATOR("FARMOS_AGGREGATOR");
	
	String type;
	
	TpCredentialType(String type) {
		this.type = type;
	}
	
	public static TpCredentialType fromString(String type) {
		for(TpCredentialType tpct: TpCredentialType.values()) {
			if(tpct.type.equalsIgnoreCase(type)) {
				return tpct;
			}
		}		
		return TpCredentialType.DEFAULT;
	}
	
}

package net.oursci.server.tpcredential;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.oursci.server.exception.BadRequest;
import net.oursci.server.user.User;

@Service
public class TpCredentialServiceImpl implements TpCredentialService {

	@Autowired
	TpCredentialRepository repo;
	
	@Override
	public TpCredential save(TpCredential a) {
		return repo.save(a);
	}

	@Override
	public void delete(Long id) {
		repo.deleteById(id);
	}

	@Override
	public TpCredential getById(Long id) {
		return repo.findById(id).orElseThrow(() -> new BadRequest("Credentials not found"));
	}

	@Override
	public boolean exists(Long id) {
		return repo.existsById(id);
	}
	
	@Override
	public List<TpCredential> list() {
		return repo.findAll();
	}
	
	@Override
	public List<TpCredential> list(User user) {
		return repo.findByUser(user);
	}
	
	@Override
	public List<TpCredential> findByUser(User user) {
		return repo.findByUser(user);
	}

	@Override
	public List<TpCredential> findByUserAndType(User user, TpCredentialType type) {
		return repo.findByUserAndType(user, type);
	}

	@Override
	public Page<TpCredential> find(String search, Pageable pageable) {
		return repo.find(search, pageable);
	}

	@Override
	public Page<TpCredential> find(String search, User user, Pageable pageable) {
		return repo.find(search, user, pageable);
	}
}

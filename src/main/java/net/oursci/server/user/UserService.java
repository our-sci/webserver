package net.oursci.server.user;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

	List<User> list();
	
	User registerUser(RegisterUserInit init);
	User getByUsername(String username);
	User findOneByEmail(String email);
	
	User getById(Long id);
	User get(Long id);
	User save(User user);
	Page<User> find(String search, Pageable pageable);

}


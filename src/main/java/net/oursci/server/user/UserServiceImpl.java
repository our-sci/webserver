package net.oursci.server.user;


import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import net.oursci.server.config.SecurityConfig.Roles;
import net.oursci.server.exception.UserNotFound;


@Service(value = UserServiceImpl.NAME)
public class UserServiceImpl implements UserService {

	public final static String NAME = "UserService";

	@Autowired
	private UserRepository repo;

	@Autowired
	private RoleRepository roleRepository;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetails userDetails = repo.findOneByUsername(username);
		if (userDetails == null)
			return null;

		Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
		for (GrantedAuthority role : userDetails.getAuthorities()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(role.getAuthority()));
		}

		return new org.springframework.security.core.userdetails.User(userDetails.getUsername(),
				userDetails.getPassword(), userDetails.getAuthorities());
	}

	@Override
	@Transactional
	@Secured(value = Roles.ROLE_ANONYMOUS)
	public User registerUser(RegisterUserInit init) {

		User userLoaded = repo.findOneByUsername(init.getUserName());

		if (userLoaded == null) {
			User userEntity = new User();
			userEntity.setUsername(init.getUserName());
			userEntity.setEmail(init.getEmail());
			String displayName = init.getDisplayName() != null ? init.getDisplayName() : init.getEmail();
			userEntity.setDisplayName(displayName);
			userEntity.setPicture(init.getPicture());

			if(init.getEmail().equalsIgnoreCase("andreas.rudolf@nexus-computing.ch")){
				userEntity.setAuthorities(getAdminRoles());
			} else {
				userEntity.setAuthorities(getUserRoles());
			}
			// TODO firebase users should not be able to login via username and
			// password so for now generation of password is OK
			userEntity.setPassword(UUID.randomUUID().toString());
			repo.save(userEntity);
			return userEntity;
		} else {
			return userLoaded;
		}
	}

	@PostConstruct
	public void init() {

		if (repo.count() == 0) {
			// create initial users here if needed
		}
	}

	private Set<Role> getAdminRoles() {
		return Collections.singleton(getRole(Roles.ROLE_ADMIN));
	}

	private Set<Role> getUserRoles() {
		return Collections.singleton(getRole(Roles.ROLE_USER));
	}

	/**
	 * Get or create role
	 * @param authority
	 * @return
	 */
	private Role getRole(String authority) {
		Role adminRole = roleRepository.findByAuthority(authority);
		if (adminRole == null) {
			return new Role(authority);
		} else {
			return adminRole;
		}
	}

	@Override
	public User getByUsername(String username) {
		User user = repo.findOneByUsername(username);
		if(user == null) {
			throw new UserNotFound();
		}
		return user;
	}

	@Override
	public User getById(Long id) {
		return repo.findById(id).orElseThrow(() -> new UserNotFound());
	}

	@Override
	public User get(Long id) {
		return repo.findById(id).orElseThrow(() -> new UserNotFound());
	}

	@Override
	public User save(User user) {
		return repo.save(user);
	}

	@Override
	public Page<User> find(String search, Pageable pageable) {
		return repo.find(search, pageable);
	}

	@Override
	public User findOneByEmail(String email) {
		return repo.findOneByEmail(email);
	}

	@Override
	public List<User> list() {
		return repo.findAll();
	}

}
package net.oursci.server.user;

public class RegisterUserInit {
	private final String userName;
	private final String email;
	private final String displayName;
	private final String picture;

	public RegisterUserInit(String userName, String email, String displayName, String picture) {
		super();
		this.userName = userName;
		this.email = email;
		this.displayName = displayName;
		this.picture = picture;
	}

	public String getUserName() {
		return userName;
	}

	public String getEmail() {
		return email;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getPicture() {
		return picture;
	}
}
package net.oursci.server.user;

import java.util.Date;

import lombok.Data;
import net.oursci.server.survey.Survey;

@Data
public class Contribution {

	private User user;
	private Survey survey;
	private Long total;
	private Date latest;
	
	public Contribution(User user, Survey survey, Long total, Date latest) {
		this.user = user;
		this.survey = survey;
		this.total = total;
		this.latest = latest;
	}

	
}

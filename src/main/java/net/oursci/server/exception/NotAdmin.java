package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class NotAdmin extends BaseException {

	private static final long serialVersionUID = 3325289083733896992L;

	public NotAdmin(String message) {
		super(message);
	}

	public NotAdmin() {
		super("You are not an admin.");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.FORBIDDEN.value();
	}
}

package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class UserNotFound extends BaseException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6152125131526544587L;

	public UserNotFound(String message) {
		super(message);
	}

	public UserNotFound() {
		super("User not found");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.NOT_FOUND.value();
	}
}


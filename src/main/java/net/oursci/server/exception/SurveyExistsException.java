package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class SurveyExistsException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6874561911099675543L;

	public SurveyExistsException(String message) {
		super(message);
	}

	public SurveyExistsException() {
		super("Survey already exists");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.CONFLICT.value();
	}
}

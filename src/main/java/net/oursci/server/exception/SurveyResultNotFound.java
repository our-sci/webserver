package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class SurveyResultNotFound extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 94724988872297916L;

	public SurveyResultNotFound(String message) {
		super(message);
	}

	public SurveyResultNotFound() {
		super("SurveyResult not found");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.NOT_FOUND.value();
	}
}


package net.oursci.server.exception;

public abstract class BaseException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4738278602000354784L;
	
	public abstract int getErrorCode();
	
	public String getKey() {
		return this.getClass().getSimpleName();
	}

	public BaseException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public BaseException(String message) {
		super(message, null);
	}

}

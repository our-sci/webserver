package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class ScriptNotFound extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1840324412186085417L;

	public ScriptNotFound(String message) {
		super(message);
	}

	public ScriptNotFound() {
		super("Script not found");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.NOT_FOUND.value();
	}
}


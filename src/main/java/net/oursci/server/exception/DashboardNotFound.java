package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class DashboardNotFound extends BaseException {
	private static final long serialVersionUID = -6017327909560747974L;

	public DashboardNotFound(String message) {
		super(message);
	}

	public DashboardNotFound() {
		super("Dashboard not found");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.NOT_FOUND.value();
	}
}

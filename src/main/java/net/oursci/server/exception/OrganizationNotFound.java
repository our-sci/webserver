package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class OrganizationNotFound extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5970951265489130157L;

	public OrganizationNotFound(String message) {
		super(message);
	}

	public OrganizationNotFound() {
		super("Can not find this organization");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.NOT_FOUND.value();
	}
}

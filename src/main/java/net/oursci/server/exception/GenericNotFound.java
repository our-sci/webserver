package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class GenericNotFound extends BaseException {
	private static final long serialVersionUID = -653496573481697803L;

	public GenericNotFound(String message) {
		super(message);
	}
	
	public GenericNotFound() {
		super("Not found");
	}

	@Override
	public int getErrorCode() {
		return HttpStatus.NOT_FOUND.value();
	}

}

package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class XmlFormNotFound extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8989163319277487629L;

	public XmlFormNotFound(String message) {
		super(message);
	}

	public XmlFormNotFound() {
		super("Dashboard not found");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.NOT_FOUND.value();
	}
}


package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class ScriptExistsException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2435970492564178231L;

	public ScriptExistsException(String message) {
		super(message);
	}

	public ScriptExistsException() {
		super("This script already exists");
	}
	
	@Override
	public int getErrorCode() {
		
		return HttpStatus.CONFLICT.value();
	}
}


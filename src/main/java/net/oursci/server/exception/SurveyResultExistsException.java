package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class SurveyResultExistsException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9179503521689961667L;

	public SurveyResultExistsException(String message) {
		super(message);
	}

	public SurveyResultExistsException() {
		super("Dashboard not found");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.CONFLICT.value();
	}
}


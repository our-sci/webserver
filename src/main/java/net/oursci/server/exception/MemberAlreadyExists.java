package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class MemberAlreadyExists extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8499457776784179132L;

	public MemberAlreadyExists(String message) {
		super(message);
	}

	public MemberAlreadyExists() {
		super("Organization member already exists");
	}
	
	@Override
	public int getErrorCode() {
		
		return HttpStatus.CONFLICT.value();
	}
}


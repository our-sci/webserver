package net.oursci.server.exception;


import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.javarosa.xform.parse.XFormParseException;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

@ControllerAdvice
public class RestExceptionHandler {

	@Bean
    public ErrorAttributes errorAttributes() {
		
        return new DefaultErrorAttributes() {
        	@Override
        	public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {        		
        		Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, includeStackTrace);
        		
        		Object errorMessage = webRequest.getAttribute(RequestDispatcher.ERROR_MESSAGE, RequestAttributes.SCOPE_REQUEST);
        		Object errorStatus = webRequest.getAttribute(RequestDispatcher.ERROR_STATUS_CODE, RequestAttributes.SCOPE_REQUEST);
        		
                if (errorMessage != null) {
                	ObjectMapper mapper = new ObjectMapper();
                	
                	ExceptionMessage message = null;
                	try {
						message = mapper.readValue((String)errorMessage, ExceptionMessage.class);
					} catch (IOException e) {
						message = new ExceptionMessage((int)errorStatus, "Generic", (String)errorMessage);
					}
                	errorAttributes.put("message", message);
                    
                }
                return errorAttributes;
        	}
        };
    }
	
	
	// Our custom defined exceptions
	@ExceptionHandler({BaseException.class})
	public void handleBaseExceptions(BaseException e, HttpServletResponse r) throws IOException {
		ExceptionMessage em = new ExceptionMessage(e.getErrorCode(), e.getKey(), e.getMessage());
		r.sendError(e.getErrorCode(), em.toString());
	}
	
	
	// Third party & runtime exceptions
	@ExceptionHandler({XFormParseException.class})
	public void handleXFormParseException(XFormParseException e, HttpServletResponse r) throws IOException {
		int status = HttpStatus.BAD_REQUEST.value();
		ExceptionMessage em = new ExceptionMessage(status, e.getClass().getSimpleName(), e.getMessage());
		r.sendError(status, em.toString());
	}
	
	@ExceptionHandler({ConstraintViolationException.class})
	public void handleConstraints(ConstraintViolationException e, HttpServletResponse r) throws IOException {
		String description = "";
		Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
		
		for (ConstraintViolation<?> violation : violations) {
			description += violation.getMessage();
			if(violations.size()>1){
				description.concat(". ");
			}
		}
		
		int status = HttpStatus.BAD_REQUEST.value();
		
		ExceptionMessage em = new ExceptionMessage(status, e.getClass().getSimpleName(), description);
		r.sendError(status, em.toString());
	}
	
}


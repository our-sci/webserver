package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class BadRequest extends BaseException {

	private static final long serialVersionUID = 4338716704918981719L;

	public BadRequest(String message) {
		super(message);
	}

	public BadRequest() {
		super("Generic bad request");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.BAD_REQUEST.value();
	}
}

package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class ScriptManifestNotFound extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8104263423098768907L;

	public ScriptManifestNotFound(String message) {
		super(message);
	}

	public ScriptManifestNotFound() {
		super("No script manifest found");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.BAD_REQUEST.value();
	}
}


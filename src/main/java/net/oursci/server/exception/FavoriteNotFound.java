package net.oursci.server.exception;

import org.springframework.http.HttpStatus;

public class FavoriteNotFound extends BaseException {

	private static final long serialVersionUID = 6939930130840236215L;

	public FavoriteNotFound(String message) {
		super(message);
	}

	public FavoriteNotFound() {
		super("Favorite not found");
	}
	
	@Override
	public int getErrorCode() {
		return HttpStatus.NOT_FOUND.value();
	}
}


package net.oursci.server;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import net.oursci.server.util.OursciUtils;
import net.oursci.server.util.StringUtil;



@RunWith(SpringRunner.class)
public class StringTest {

	@Test
	public void getBasename()  {
        String key = "survey-definitions/build_supermax_12321/form.xml";
        String[] splits = key.split("/");
        String basename = splits[splits.length - 1];
        System.out.println(basename);
        basename = StringUtil.getBasename(key);
        System.out.println(basename);

	}
	
	@Test
	public void readIdFromManifest() throws FileNotFoundException, JSONException {
		File file = new File("/home/rudi/Dropbox/Nexus-Computing/oursci/reflectometer/out/manifest.json");
		
		JSONObject json = OursciUtils.getJsonObject(new FileInputStream(file));
		
		System.out.println(json.getString("id"));
		System.out.println(json.getString("version"));

	}
	
	@Test
	public void testMatcher() {
		Pattern pattern = Pattern.compile("\"(.*)\": \"(.*)\"");
		String line = "\"filename\": \"measurement_2017-11-01 21:13:25.json\", ";
		
		//Pattern pattern = Pattern.compile("(.*): (.*)");
		//String line = "key: value";

		
		Matcher m = pattern.matcher(line);
		if(m.find()){
			System.out.println("matcher found something");
			System.out.println(m.group(0));
			System.out.println(m.group(1));
			System.out.println(m.group(2));
		}
	}
	


    
}


package net.oursci.server;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.javarosa.core.model.FormDef;
import org.javarosa.core.model.FormIndex;
import org.javarosa.core.model.IFormElement;
import org.javarosa.core.model.data.StringData;
import org.javarosa.core.model.instance.FormInstance;
import org.javarosa.core.model.instance.InstanceInitializationFactory;
import org.javarosa.core.model.instance.TreeElement;
import org.javarosa.core.services.transport.payload.ByteArrayPayload;
import org.javarosa.core.util.externalizable.DeserializationException;
import org.javarosa.form.api.FormEntryController;
import org.javarosa.form.api.FormEntryModel;
import org.javarosa.model.xform.XFormSerializingVisitor;
import org.javarosa.xform.parse.XFormParseException;
import org.javarosa.xform.parse.XFormParser;
import org.javarosa.xform.util.XFormUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import net.oursci.server.javarosa.Event;
import net.oursci.server.javarosa.JavaRosaUtils;



@RunWith(SpringRunner.class)
public class FormTest {

	
	@SuppressWarnings("unused")
	@Test
    public void formDef() throws IOException, DeserializationException, InterruptedException {
        System.out.println("running formDef");

        File xmlFile = new File("/home/rudi/Dropbox/Nexus-Computing/oursci/My first survey.xml");
        System.out.println("xml file: " + xmlFile.getAbsolutePath());

        FileInputStream fis = new FileInputStream(xmlFile);
        FormDef def = XFormUtils.getFormFromInputStream(fis);

        for (IFormElement e : def.getChildren()) {
            System.out.printf("id, title: %s, %s\n", e.getID(), e.getLabelInnerText());
        }

        FormEntryModel fem = new FormEntryModel(def);
        FormEntryController fec = new FormEntryController(fem);

        def.initialize(true, new InstanceInitializationFactory());
        String id = fem.getForm().getMainInstance().getRoot().getAttributeValue(null, "id");

        FormIndex idx = fem.getFormIndex();
        IFormElement element = fec.getModel().getForm().getChild(idx);

        System.out.printf("id of element is %s\n", element.getClass());

        eventLoop:
        for (; ; ) {

            Event e = Event.fromEvent(fem.getEvent(idx));
            System.out.println(e);

            switch (e) {
                case EVENT_BEGINNING_OF_FORM:
                    break;
                case EVENT_END_OF_FORM:
                    break eventLoop;
                case EVENT_PROMPT_NEW_REPEAT:
                    break;
                case EVENT_QUESTION:
                    String drivletId = fem.getQuestionPrompt().getSpecialFormQuestionText("drivlet");
                    fec.answerQuestion(idx, new StringData("hello"), true);
                    System.out.println("drivlet is: " + drivletId);
                    System.out.println(fem.getQuestionPrompt().getQuestionText());
                    System.out.println(fem.getQuestionPrompt().getHelpText());
                    System.out.println(fem.getQuestionPrompt().getAnswerText());
                    System.out.println(fem.getQuestionPrompt().getFormElement().getTextID());
                    
                    
                    
                    List<TreeElement> addAttributes = 
                    		fem.getQuestionPrompt().getBindAttributes();
                    System.out.println("# additional attributes: "+addAttributes.size());
                    for(TreeElement ele : addAttributes) {
                    	System.out.println(ele.getName());
                    }

                    
                    System.out.println();
                    break;
                case EVENT_GROUP:
                    break;
                case EVENT_REPEAT:
                    break;
                case EVENT_REPEAT_JUNCTURE:
                    break;
                case EVENT_UNKNOWN:
                    break;
            }

            idx = fem.incrementIndex(idx);
            fem.setQuestionIndex(idx);
        }

        XFormSerializingVisitor serializer = new XFormSerializingVisitor();
        ByteArrayPayload payload = (ByteArrayPayload) serializer.createSerializedPayload(fem.getForm().getInstance());
        //exportXmlFile(payload, "./output.xml");


        System.out.printf("question event: %d\n", fec.getModel().getEvent());

    }
	
	
	@Test
	@Ignore
	public void loadFormWithInstance() throws IOException {
		
        File formDefinitionFile = new File("/home/rudi/Dropbox/Nexus-Computing/oursci/form.xml"); 
        File formInstanceFile = new File("/home/rudi/Dropbox/Nexus-Computing/oursci/output.xml");
        
        FormDef formDefinition = XFormUtils.getFormFromInputStream(new FileInputStream(formDefinitionFile));
		FormEntryModel fem = new FormEntryModel(formDefinition);
		
		@SuppressWarnings("unused")
		FormEntryController fec = new FormEntryController(fem);
                
        FormInstance formInstance = null;
		
		try {
            formInstance = XFormParser.restoreDataModel(new FileInputStream(formInstanceFile), null);
        } catch (Exception e) {
        	System.out.println("Error when running restoreDataModel");
        	return;
        }

		String formId = JavaRosaUtils.getFormId(formInstance);
		String instanceID = JavaRosaUtils.getInstanceID(formInstance);
		System.out.println("form id: "+formId);
		System.out.println("instanceID: "+instanceID);
		//JSONObject json = JavaRosaUtils.getJsonKeysAndValues(formInstance);
		//System.out.println(json);
		
	}
	
	@Test
	public void loadFormString() throws XFormParseException, FileNotFoundException {
        File formDefinitionFile = new File("/home/rudi/Dropbox/Nexus-Computing/oursci/form.xml"); 
        FormDef formDef = JavaRosaUtils.getFormDef(new FileInputStream(formDefinitionFile));
        
        
        String formId = JavaRosaUtils.getFormId(formDef);
		System.out.println("form id: "+formId);
		String jsonString = JavaRosaUtils.getJsonKeys(formDef);
		System.out.println(jsonString);
	}
	
	@Test
	public void getMetaContents() throws XFormParseException, FileNotFoundException {
        File formDefinitionFile = new File("/home/rudi/oursci/odkbuild/Real-form.xml");
        FormDef formDef = JavaRosaUtils.getFormDef(new FileInputStream(formDefinitionFile));
        System.out.println("listing elements");
        
        for(TreeElement element : formDef.getAdditionalAttributes()) {
        	System.out.println(element.getValue());
        	System.out.println(element.getAttributeValue());
        }
        
        for(Map.Entry<String, Object> entry : formDef.getMetaData().entrySet()) {
        	System.out.println(entry.getKey());
        }
        
        
        
	}
	


    
}


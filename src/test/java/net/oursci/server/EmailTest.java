package net.oursci.server;

public class EmailTest {

	public static void main(String[] args) {
		System.out.println("Hello email test");
		
		String emails = "ar@serverbox.ch; ar@nexus-computing.ch";
		
		String[] splits = emails.split(";");
		
		for(int i=0; i<splits.length; i++) {
			String email = splits[i].trim();
			System.out.println("Sending email to: "+email);
		}
		
	}
	
}

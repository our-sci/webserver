package net.oursci.server;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
public class ColumnsTest {

	
	@Test
	@Ignore
	public void contextLoads() {
		
        ArrayList<String> strings = new ArrayList<>();
        strings.add("Temperature");
        strings.add("Humidity");
        strings.add("Leaf color");
        
        System.out.println(strings);
        
        JSONArray json = new JSONArray(strings);
        
        System.out.println(json);
        
        
		
	}
	
	@SuppressWarnings("unused")
	@Test
	public void keysAndValues() throws FileNotFoundException, IOException {
		File formDefinitionFile = new File("/home/rudi/Dropbox/Nexus-Computing/oursci/form.xml"); 
        File formInstanceFile = new File("/home/rudi/Dropbox/Nexus-Computing/oursci/output.xml");
        
        //JSONObject keysAndValues = JavaRosaUtils.getJsonKeysAndValues(formInstanceFile);
        //System.out.println(keysAndValues);
        
        JSONArray outer = new JSONArray();
        JSONObject inner = new JSONObject();
        try {
			inner.put("color", "red");
	        inner.put("samples", "1");
		} catch (JSONException e) {
			e.printStackTrace();
		}
        outer.put(inner);
        outer.put(inner);
        
        System.out.println(outer);
	}
	

    
}


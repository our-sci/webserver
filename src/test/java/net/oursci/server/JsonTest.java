package net.oursci.server;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
public class JsonTest {

	
	@SuppressWarnings("rawtypes")
	@Test
	public void jsonOrder() {
		String jsonString = "{ \"median0\": \"nan\", \"filename\": \"script.txt\",  \"median1\": \"2\" }";
		
		JSONParser parser = new JSONParser();

		ContainerFactory orderedKeyFactory = new ContainerFactory()
		{
			@Override
		    public List creatArrayContainer() { // <-- that is a typo
		      return new LinkedList();
		    }

			public Map createObjectContainer() {
		      return new LinkedHashMap();
		    }


		};

		Object obj;
		try {
			obj = parser.parse(jsonString,orderedKeyFactory);
			LinkedHashMap map = (LinkedHashMap)obj;
			for(Object o: map.values()) {
				System.out.println(o);
			}

			
			JSONObject json = new JSONObject(map);
			System.out.println(json.toString());
			
		} catch (ParseException e) {
			e.printStackTrace();
		}  
		
	}
	
	

    
}


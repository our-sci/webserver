# Our-Sci Webserver

## Overview

The Our-Sci Webserver is available at [https://app.our-sci.net](https://app.our-sci.net). It is used to **create & browse surveys**, as well as for **viewing & exporting** their uploaded data sets. The web frontend may be viewed anonymously, however, to create surveys one needs to authenticate with either E-Mail/Password, or using a social login (Google sign in).

The Our-Sci Webserver consists of a **React based frontend** and a **Spring-Boot backend**. It is intended to be hosted with [Amazon Elastic Beanstalk](https://aws.amazon.com/de/elasticbeanstalk/). For authentication purposes Firebase login is used. Raw data from survey & measurement uploads is saved to Amazon S3 Buckets, and related metadata (such as date & user information) is stored to a MYSQL database.

## Development

### Frontend (React)

To build production frontend:

```bash
cd src/main/frontend
npm run package
```

This will create the frontend files inside src/main/resources/build.

For testing locally:

```bash
cd src/main/frontend
npm run start
```

This will start a local webpack dev-server on http://localhost:3000.

### Backend (Spring Boot)

Spring Boot Server:

To run locally with local mysql database:
spring.profiles.active=local

To run locally AND use Amazon RDS, set the following

    Environment variables:
    AMAZON_REGION=
    DB_INSTANCE=
    DB_NAME=
    DB_PASSWORD=
    DB_USER=
    FIREBASE_CONFIG=
    FIREBASE_URL=
    S3_BUCKET=
    SMTP_USERNAME=
    SMTP_PASSWORD=

    Spring boot properties:
    cloud.aws.credentials.accessKey
    cloud.aws.credentials.secretKey
    cloud.aws.region.static=us-east-2
    ... and make sure 'local' is not set in spring.profile.active

To run on Amazon EC2, set the following:

    Environment variables:
    AMAZON_REGION=
    DB_INSTANCE=
    DB_NAME=
    DB_PASSWORD=
    DB_USER=
    FIREBASE_CONFIG=
    FIREBASE_URL=
    S3_BUCKET=
    SMTP_USERNAME=
    SMTP_PASSWORD=

    and make the instance use an "Instance profile", e.g. "spring-cloud-aws".
    For this, create an IAM role "spring-cloud-aws" with the following permissions:
    AmazonRDSFullAccess, AmazonEC2FullAccess, AmazonS3FullAccess, AWSCloudFormationReadOnlyAccess

## API examples

### How to get survey result data

#### CSV

```bash
curl https://app.our-sci.net/api/survey/result/csv/$formId
E.g:
curl https://app.our-sci.net/api/survey/result/csv/simple-soil-scan-form > simple-soil-scan-form.csv
```

#### JSON

```bash
curl https://app.our-sci.net/api/survey/result/json/$formId
E.g:
curl https://app.our-sci.net/api/survey/result/json/simple-soil-scan-form > simple-soil-scan-form.json
```

Note: The above data will contain what is publicly visible for everybody. See below on how to retrieve anonymized private data.

#### Authentication for accessing private anonymized fields

If there are anonymized fields private to you inside the survey result set, which you would like to retrieve, you need to supply an authentication token with the request. To do so, visit the [profile page](https://app.our-sci.net/#/profile), click on Show token, and copy the token.

```bash
export TOKEN=*************
curl --header "X-Authorization-Firebase: $TOKEN" https://app.our-sci.net/api/survey/result/csv/$formId
E.g:
curl https://app.our-sci.net/api/survey/result/csv/simple-soil-scan-form > simple-soil-scan-form.csv
```

### Other example API calls

Signup on the server after getting a firebase token. This creates a user on the server's database:
It's OK to signup multiple times (nothing happens if the user already exists).

```bash
curl -vvv -X POST --header "Content-Type: application/json" --header "firebaseToken: $TOKEN" http://localhost:5000/api/signup
```

Get a resource which needs authentication:

```bash
curl -vvv -X GET --header "Content-Type: application/json" --header "X-Authorization-Firebase: $
TOKEN" http://localhost:5000/api/client/example
```

List surveys and read "formId":

```bash
curl http://localhost:5000/api/survey/list
```

Download survey by formId, as zip:

```bash
curl http://localhost:5000/api/survey/download/zip/by-form-id/$formId
curl http://localhost:5000/api/survey/download/zip/by-form-id/build_first-testing-form_1504169611
```

Download survey by formId, get a list of download links to all files:

```bash
curl http://localhost:5000/api/survey/download/listing/by-form-id/$formId
curl http://localhost:5000/api/survey/download/listing/by-form-id/build_first-testing-form_1504169611
```

List scripts and read "scriptId":

```bash
curl http://localhost:5000/api/script/list
```

Download script by scriptId, as zip:

```bash
curl http://localhost:5000/api/script/download/zip/by-script-id/$scriptId
curl http://localhost:5000/api/script/download/zip/by-script-id/bfa_reflectometer_script
```

Download script by scriptId, get a list of download links to all files:

```bash
curl http://localhost:5000/api/script/download/listing/by-script-id/$scriptId
curl http://localhost:5000/api/script/download/listing/by-script-id/bfa_reflectometer_script
```

Upload survey results, multipart files in 'files' parameter:

```bash
curl -v -F 'files=@output.xml' -F 'files=@another.txt' http://localhost:5000/api/survey/result/create
```

Upload script results, one multipart file in 'files' parameter:

```bash
curl -v -F 'files=@result.json' http://localhost:5000/api/script/result/create/bfa_reflectometer_script
```

Download a survey xml file:

```bash
curl https://app.our-sci.net/api/survey/download/xml/by-form-id/$formId
e.g. curl https://app.our-sci.net/api/survey/download/xml/by-form-id/build_Grand-Survey-A-survey-for-testing-a-broad-array-of-features_1526644607
```

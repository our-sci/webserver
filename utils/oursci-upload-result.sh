#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo Must supply path as one and only argument
  exit 1
fi

# for stupid files with spaces
IFS=$(echo -en "\n\b")

CURLCMD="curl -F 'override=true'"

for f in `find $1 -maxdepth 1 -type f`
do
  echo "uploading $f";
  CURLCMD="$CURLCMD -F 'files=@$f'"
done

CURLCMD="$CURLCMD http://localhost:5000/api/survey/result/create"

echo $CURLCMD
eval $CURLCMD

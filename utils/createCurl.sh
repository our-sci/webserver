#!/bin/bash


DEFAULT_TOKEN="ey..."
read -p "API token from your profile page ($DEFAULT_TOKEN): " token

if [[ -z "$token" ]]; then
    token=$DEFAULT_TOKEN
fi

echo ""
echo "Now run these commands:"
echo ""
echo "# export header"
echo "export HEADER=\"X-Authorization-Firebase: $token\""
echo ""
echo "# run curl with custom header"
echo "curl -H \"\$HEADER\" https://app.our-sci.net/api/profile/self | jq"

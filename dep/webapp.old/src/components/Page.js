import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';


import Auth from './Auth';
import Feedback from './Feedback';
import Header from './Header';
import Footer from './Footer';

import OpenExample from './resources/OpenExample';
import ClientExample from './resources/ClientExample';
import ClientProfile from './resources/ClientProfile';
import AdminExample from './resources/AdminExample';
import SurveyList from './resources/SurveyList';
import SurveySingle from './resources/SurveySingle';
import SurveyCreate from './resources/SurveyCreate';
import ScriptList from './resources/ScriptList';
import ScriptSingle from './resources/ScriptSingle';
import ScriptCreate from './resources/ScriptCreate';


export default class Page extends Component {
  render() {
    return (
      <div>
        <Header />
        <Feedback />
        <div className='container main-container'>
          <Switch>
            <Route exact path='/' component={SurveyList} />
            <Route exact path='/login' component={Auth} />
            <Route path='/resources/open/example' component={OpenExample} />
            <Route exact path='/survey' component={SurveyList} />
            <Route exact path='/script' component={ScriptList} />
            <Route path='/script/by-script-id/:scriptId' component={ScriptSingle} />
            <Route path='/script/by-database-id/:databaseId' component={ScriptSingle} />
            <Route path='/script/create' component={ScriptCreate} />


            <Route path='/resources/client/example' component={ClientExample} />
            <Route path='/resources/client/profile' component={ClientProfile} />
            <Route path='/resources/admin/example' component={AdminExample} />
            <Route path='/survey/create' component={SurveyCreate} />
            <Route path='/survey/by-form-id/:formId' component={SurveySingle} />
            <Route path='/survey/by-database-id/:databaseId' component={SurveySingle} />


          </Switch>
        </div>
        <Footer />
      </div>
    );
  }
}

import React, { Component } from 'react';

class SocialButton extends Component {
  render() {
    return (
      <button
        className={`btn btn-block btn-social btn-${this.props.provider}`}
        onClick={this.props.onClick}
      ><span className={`fa fa-${this.props.provider}`} />{this.props.children}</button>
    );
  }
}

export default SocialButton;

import React, { Component } from 'react';

class Token extends Component {

  render() {
    if (!this.props.token) {
      return null;
    }

    return (
      <div>
        Token:<br />
        <textarea value={this.props.token} cols='100' rows='10' readOnly />
      </div>
    );
  }
}

export default Token;

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dropzone from 'react-dropzone';
import _ from 'lodash';
import axios, { post } from 'axios';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import C from '../../constants';

const DEFAULT_ACCEPTED_FILES = [
  //{ type: 'application/zip', desc: 'ZIP archives (*.zip)' },
  //{ type: 'application/x-tar', desc: 'TAR archives (*.tar)' },
  { type: 'text/xml', desc: 'XML files' },
  { type: 'image/*', desc: 'Any type of images' },
  { type: 'text/plain', desc: 'Plain text files' },
  { type: 'application/javascript', desc: 'Javascript files' }
];

const initialState = {
  files: [],
  error: null,
  override: false,
  uploading: false,
  progress: 0,
  source: axios.CancelToken.source()
};

class Uploader extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;

    this.handleUpload = this.handleUpload.bind(this);
    this.renderError = this.renderError.bind(this);
    this.renderFilesToBeUploaded = this.renderFilesToBeUploaded.bind(this);
    this.renderLoginPrompt = this.renderLoginPrompt.bind(this);
    this.renderUploadProgress = this.renderUploadProgress.bind(this);
  }

  onDrop(acceptedFiles) {
    console.log(acceptedFiles);

    this.setState(
      { files: _.uniqBy([...this.state.files, ...acceptedFiles], 'name') }
    );
  }

  cancelUpload() {
    this.setState({ files: [] });
    const { source } = this.state;
    if (source) {
      source.cancel('cancelled by user');
    }
  }

  handleUpload() {
    if (this.state.uploading) {
      return;
    }

    this.setState({ uploading: true });

    const { files, override } = this.state;
    const { token } = this.props.auth;

    const formData = new FormData();

    files.forEach((file) => {
      formData.append('files', file);
    });

    formData.append('override', override);

    //const source = axios.CancelToken.source();
    //const source = CancelToken.source();
    //this.setState({ source });

    const config = {
      cancelToken: this.state.source.token,
      onUploadProgress: progressEvent => {
        const progress = Math.round((progressEvent.loaded / progressEvent.total) * 100);
        this.setState({ progress });
      },
      headers: {
        'Content-Type': 'multipart/form-data',
        'X-Authorization-Firebase': token
      }
    };

    post(this.props.url, formData, config)
      .then((response) => {
        this.setState(initialState);
        this.props.onSuccess();
      })
      .catch((error) => {
        this.setState({ error: error.response, uploading: false, progress: 0 });
      });
  }

  renderError() {
    const { error } = this.state;

    if (!error) {
      return null;
    }

    return (
      <div className='alert alert-warning'>
        <strong>{error.data.key}</strong> {error.data.message}
      </div>
    );
  }

  renderLoginPrompt() {
    if (this.props.auth.status !== C.AUTH_LOGGED_IN) {
      return (<div className='alert alert-warning'>
        <strong>Unauthorized</strong> Please <Link to='/login'>login</Link> to create a {this.props.name}.
      </div>);
    }

    return null;
  }

  renderFilesToBeUploaded() {
    if (this.state.files.length === 0) {
      return <div><h3>No files chosen yet</h3></div>;
    }

    return (
      <div>
      <h3>Files to be uploaded:</h3>
      <ul className='list-group'>
        {
          this.state.files.map(
            f => <li className='list-group-item' key={f.name}>{f.name} - {f.size / 1000} KB</li>
          )
        }
      </ul>
      </div>
    );
  }

  renderUploadProgress() {
    if (!this.state.uploading) {
      return null;
    }

    const { progress } = this.state;

    return (
      <div className="progress" style={{ marginTop: '1em' }}>
        <div
          className="progress-bar" role="progressbar" aria-valuenow={progress}
          aria-valuemin="0" aria-valuemax="100" style={{ width: `${progress}%` }}
        >
            <span className="sr-only">{`${progress}%`} Complete</span>
        </div>
      </div>
    );
  }

  render() {
      return (
        <div>
        {this.renderLoginPrompt()}
        {this.renderError()}
        <h1>Create a {this.props.name}</h1>
        <p>{this.props.children}</p>

        <Dropzone
          className='dropzone'
          activeClassName='dropzone-active'
          rejectClassName='dropzone-reject'
          onDrop={this.onDrop.bind(this)}
          accept={_.map(this.props.acceptedFiles || DEFAULT_ACCEPTED_FILES, 'type').join(', ')}
        >
          <div>
            <h4> Click or drag files into this area</h4>
          <ul>
          {
            _.map(this.props.acceptedFiles || DEFAULT_ACCEPTED_FILES,
              f => <li key={f.type}>{f.type} - {f.desc}</li>)
          }
          </ul>

          </div>
          <div className='text-right'>
            <span className="fa fa-upload fa-2x" aria-hidden="true" />
          </div>
        </Dropzone>
        <aside>
          {this.renderFilesToBeUploaded()}
        </aside>

        <div className='clearfix'>
        <div className='pull-right'>
        <div className='checkbox-inline'>
          <label htmlFor='override'>
          <input
            type="checkbox" id='override'
            onChange={() => this.setState({ override: !this.state.override })}
            checked={this.state.override}
          />
          Override existing {this.props.name}
          </label>
        </div>
        <div className='btn-toolbar'>
        <button
          type='button'
          onClick={this.cancelUpload.bind(this)}
          className='btn btn-danger'
        >Cancel</button>
        <button
          type='button'
          onClick={this.handleUpload}
          className={`btn btn-primary ${this.state.uploading ? 'disabled' : ''}`}
        >Upload {this.props.name}</button>
        </div>
        </div>
        </div>

        {this.renderUploadProgress()}

        </div>
      );
  }
}

Uploader.propTypes = {
  name: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  onSuccess: PropTypes.func,
};

const mapStateToProps = state => ({ auth: state.auth });

export default withRouter(connect(mapStateToProps, null)(Uploader));

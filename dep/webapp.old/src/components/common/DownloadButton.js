import React from 'react';

const DownloadButton = ({ href, children, style }) => {
  return (
    <a href={href} >
    <button type="button" className="btn btn-primary" style={style}>
    <span
      className="glyphicon glyphicon-download"
      aria-hidden="true"
    /> {children}
    </button>
    </a>
  );
};

export default DownloadButton;

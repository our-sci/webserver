import React from 'react';

const Toggle = ({ checked, onToggle, textChecked, textUnchecked }) => {
  if (checked) {
    return (
      <button
        className="btn btn-success"
        type="button"
        onClick={onToggle}
      > {textChecked || 'uncheck'}</button>
    );
  }

  return (
    <button
      className="btn btn-warning"
      type="button"
      onClick={onToggle}
    > {textUnchecked || 'check'}</button>
  );
};

export default Toggle;

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { BrandToggle } from './BrandToggle';
import { Fluid } from './Fluid';
import { Collapse } from './Collapse';

export const NavbarNav = (props) => {
  const alignmentClass = (props.alignment) ? `navbar-${props.alignment}` : '';

  return (
    <ul className={`nav navbar-nav ${alignmentClass}`}>
      {props.children}
    </ul>
  );
};

export const ItemLink = (props) => {
  return (
    <li>
      <Link to={props.to} onClick={props.onClick}>{props.children}</Link>
    </li>
  );
};

export const Dropdown = (props) => {
    return (
      <li className="dropdown">
        <a
          href="#" className="dropdown-toggle" data-toggle="dropdown"
          role="button" aria-haspopup="true" aria-expanded="false"
        >{props.title} <span className="caret" /></a>
        <ul className="dropdown-menu">
          {props.children}
        </ul>
      </li>
    );
};

export const Separator = (props) => {
  return <li role='separator' className='divider' />;
};

/*
Example:
<Navbar title='Oursci Webinterface'>
  <NavbarNav>
    <ItemLink to='/projects'>Projects</ItemLink>
  </NavbarNav>
  <NavbarNav alignment='right'>
    <Dropdown title='More'>
      <ItemLink to='/resources/open'>Open</ItemLink>
      <ItemLink to='/resources/client'>Client</ItemLink>
      <ItemLink to='/resources/admin'>Admin</ItemLink>
      <Separator />
      <ItemLink to='/' onClick={this.props.logoutUser}>Sign out</ItemLink>
    </Dropdown>
  </NavbarNav>
</Navbar>
*/
export default class Navbar extends Component {
  render() {
    return (
      <nav className="navbar navbar-inverse navbar-static-top">
        <Fluid>
          {/* Brand and toggle get grouped for better mobile display */}
          <BrandToggle>{this.props.title}</BrandToggle>

          {/* Collect the nav links, forms, and other content for toggling */}
          <Collapse>
            {this.props.children}
          </Collapse>{/* .navbar-collapse */}
        </Fluid>{/* .container-fluid */}
      </nav>
    );
  }
}

import React from 'react';

export const Fluid = (props) => {
  return (
    <div className='container-fluid'>
      {props.children}
    </div>
  );
};

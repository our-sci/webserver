import React from 'react';
import { Link } from 'react-router-dom';

export const BrandToggle = (props) => {
  return (
    <div className="navbar-header">
      <button type="button" className="navbar-toggle collapsed"
      data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
      aria-expanded="false">
        <span className="sr-only">Toggle navigation</span>
        <span className="icon-bar" />
        <span className="icon-bar" />
        <span className="icon-bar" />
      </button>
      <Link className="navbar-brand" to='/'>{props.children}</Link>
    </div>
  );
};

import React from 'react';

export const Collapse = (props) => {
  return (
    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      {props.children}
    </div>
  );
};

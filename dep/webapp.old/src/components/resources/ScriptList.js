import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import _ from 'lodash';

import { getScripts } from '../../actions/api';
import C from '../../constants';
import CreateButton from '../common/CreateButton';


class ScriptList extends Component {

  constructor(props) {
    super(props);

    this.renderScripts = this.renderScripts.bind(this);
    this.viewScript = this.viewScript.bind(this);
    this.renderArchiveToggle = this.renderArchiveToggle.bind(this);

    this.state = { showArchived: false };
  }

  componentDidMount() {
      this.props.getScripts();
  }

  viewScript(scriptId) {
    return () => {
      this.props.history.push(`/script/by-script-id/${scriptId}`);
    };
  }

  renderScripts() {
    const { list } = this.props.scripts;

    if (list.status === C.STATUS_LOADING) {
      return <tr><td>loading...</td></tr>;
    }

    if (list.status === C.STATUS_ERROR) {
      return <tr><td>Error: {list.data}</td></tr>;
    }

    return _.map(list.data, (script) => {
      if (script.archived && !this.state.showArchived) {
        return null;
      }

      return (
        <tr
          style={script.archived ? { opacity: '0.5' } : {}}
          role='button' key={script.scriptId}
          onClick={this.viewScript(script.scriptId)}
        >
          <td><strong>{script.name}</strong></td>
          <td><small>{script.scriptId}</small></td>
        </tr>
      );
    });
  }

  renderArchiveToggle() {
    return (
      <button
        onClick={() => this.setState({ showArchived: !this.state.showArchived })}
        className='btn btn-info'
      >{this.state.showArchived ? 'Hide Archived' : 'Show Archived'}
      </button>
    );
  }

  render() {
      return (
        <div>
        <div className='btn-toolbar pull-right'>
        {this.renderArchiveToggle()}
        <CreateButton to='/script/create'>Create Script</CreateButton>
        </div>
        <h2>Scripts</h2>
        <table className='table table-striped table-hover'>
          <thead><tr><th>Name</th><th>scriptId</th></tr></thead>
          <tbody>
          {this.renderScripts()}
          </tbody>
        </table>
        </div>
      );
  }

}

const mapStateToProps = state => ({ auth: state.auth, api: state.api, scripts: state.scripts });

const mapDispatchToProps = {
  getScripts
};

export default connect(mapStateToProps, mapDispatchToProps)(ScriptList);

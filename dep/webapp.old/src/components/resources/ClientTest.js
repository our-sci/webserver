import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getClientTest } from '../../actions/api';

class ClientTest extends Component {

  constructor(props) {
    super(props);
    this.renderClientTest = this.renderClientTest.bind(this);
  }

  componentDidMount() {
    const { token } = this.props.auth;
    if (token) {
      this.props.getClientTest(token);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.auth.token && nextProps.auth.token) {
      this.props.getClientTest(nextProps.auth.token);
    }
  }

  renderClientTest() {
    return _.map(this.props.api.client.test, resource => {
      return (
        <tr key={resource.outId} >
          <td>{resource.outId}</td><td>{resource.name}</td>
        </tr>
      );
    });
  }

  render() {
      return (
        <div>
          <h2>Client</h2>
          <table className='table'>
            <thead><tr><th>id</th><th>value</th></tr></thead>
            <tbody>
            {this.renderClientTest()}
            </tbody>
          </table>
        </div>
      );
  }

}

const mapStateToProps = state => ({ auth: state.auth, api: state.api });

const mapDispatchToProps = {
  getClientTest
};

export default connect(mapStateToProps, mapDispatchToProps)(ClientTest);

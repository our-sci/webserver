import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import _ from 'lodash';
import { CSVLink } from 'react-csv';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import C from '../../constants';
import { getSurveyById } from '../../actions/api';
import DownloadButton from '../common/DownloadButton';
import Toggle from '../common/Toggle';

class SurveySingle extends Component {
  constructor(props) {
    super(props);
    this.renderSurvey = this.renderSurvey.bind(this);
    this.renderTable = this.renderTable.bind(this);
    this.getCSV = this.getCSV.bind(this);
    this.stringifyCells = this.stringifyCells.bind(this);

    this.toggleArchived = this.toggleArchived.bind(this);
    this.fetchData = this.fetchData.bind(this);

    this.state = {
      keys: [],
      results: [],
      dateFrom: moment().subtract(7, 'days'),
      dateTo: moment(),
      filterByDate: false
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps, prevState) {
    const { filterByDate, dateFrom, dateTo } = this.state;

    if (filterByDate !== prevState.filterByDate) {
      this.fetchData();
    }
    if (dateFrom !== prevState.dateFrom || dateTo !== prevState.dateTo) {
      this.fetchData();
    }
  }

  getCSV() {
    const header = [...this.state.keys];

    // fill header
    _.map(this.state.results, (result) => {
      _.map(this.state.keys, (key) => {
        const data = result[key];
        if (data && typeof data === 'object') {
            _.map(data, (subvalue, subkey) => { // meta or data
              _.map(subvalue, (subsubvalue, subsubkey) => {
                if (!_.includes(header, `${key}.${subkey}.${subsubkey}`)) {
                  header.splice(_.indexOf(header, key), 0, `${key}.${subkey}.${subsubkey}`);
                }
              });
            });
        }
      });
    });


    // fill rows
    const csv = [header, ..._.map(this.state.results, (result) => {
      return _.map(header, (key) => {
        if (_.includes(this.state.keys, key)) {
          const data = result[key];
          if (data && typeof data === 'object') {
            return '';
          }
          return data;
        }
        const splits = _.split(key, '.');
        const topkey = splits[0];
        const subkey = splits[1];
        const subsubkey = splits[2];
        let ret;
        try {
          ret = result[topkey][subkey][subsubkey];
        } catch (error) {
          ret = '';
        }
        if (ret === undefined) {
          ret = '';
        }
        return ret;
      });
    })];

    return csv;
  }

  fetchData() {
    const { formId } = this.props.match.params;
    const { filterByDate, dateFrom, dateTo } = this.state;

    const keysUrl =
      `/api/survey/keys/by-form-id/${formId}`;

    this.props.getSurveyById(formId);

    axios({
      url: keysUrl,
      method: 'get',
      headers: {
        'Content-Type': 'application/json'
      }
      })
      .then((response) => {
        this.setState({ keys: response.data });
      })
      .catch(() => {
      });

    const keysAndValuesUrl = (filterByDate) ?
      `/api/survey/result/keys-and-values/by-form-id/${formId}?from=${dateFrom.format(C.DATE_FORMAT)}&to=${dateTo.format(C.DATE_FORMAT)}` :
      `/api/survey/result/keys-and-values/by-form-id/${formId}`;

    axios({
      url: keysAndValuesUrl,
      method: 'get',
      headers: {
        'Content-Type': 'application/json'
      }
      })
      .then((response) => {
        this.setState({ results: response.data });
      })
      .catch(() => {
      });
  }

  handleDateFromChange(dateFrom) {
    this.setState({ dateFrom });
    this.fetchData(dateFrom.format(C.DATE_FORMAT), this.state.dateTo.format(C.DATE_FORMAT));
  }

  handleDateToChange(dateTo) {
    this.setState({ dateTo });
    this.fetchData(this.state.dateFrom.format(C.DATE_FORMAT), dateTo.format(C.DATE_FORMAT));
  }

  toggleArchived() {
    const { formId } = this.props.match.params;

    const requestUrl = `/api/survey/toggle/archived/by-form-id/${formId}`;

    axios({
      url: requestUrl,
      method: 'get',
      headers: {
        'Content-Type': 'application/json'
      }
      })
      .then(() => {
        this.props.getSurveyById(formId);
      })
      .catch(() => {

      });
  }

  stringifyCells(data) {
    if (data && typeof data === 'object') {
      return JSON.stringify(data, null, 1);
    }
    return data;
  }

  renderTable() {
    const header = [...this.state.keys];

    // fill header
    _.map(this.state.results, (result) => {
      console.log(result);
      _.map(this.state.keys, (key) => {
        console.log(key);
        const data = result[key];
        if (data && typeof data === 'object') {
          _.map(data, (subvalue, subkey) => { // meta or data
            _.map(subvalue, (subsubvalue, subsubkey) => {
              if (!_.includes(header, `${key}.${subkey}.${subsubkey}`)) {
                header.splice(_.indexOf(header, key), 0, `${key}.${subkey}.${subsubkey}`);
              }
            });
          });
        }
      });
    });

    return (
      <div style={{ overflow: 'auto' }}>
      <table className='table table-bordered table-striped nowrap'>
        <thead>
          <tr>
            {
              _.map(header, (key) =>
                (
                <th key={key}>{key}</th>
                )
              )
            }
          </tr>
        </thead>
        <tbody>
            {
              _.map(this.state.results, (result) => {
                return (
                  <tr key={result.metaInstanceID}>
                    {
                      _.map(header, (key) => {
                        if (_.includes(this.state.keys, key)) {
                          const data = result[key];
                          if (data && typeof data === 'object') {
                            return <td key={key}>{''}</td>;
                          }
                          return <td key={key}>{data}</td>;
                        }
                        const splits = _.split(key, '.');
                        const topkey = splits[0];
                        const subkey = splits[1];
                        const subsubkey = splits[2];
                        let ret;
                        try {
                          ret = result[topkey][subkey][subsubkey];
                        } catch (error) {
                          ret = '';
                        }
                        if (ret === undefined) {
                          ret = '';
                        }
                        if (subsubkey === 'filename') {
                          let linkname = ret.split(/[\\/]/).pop();
                          linkname = decodeURIComponent(linkname);
                          ret = <a href={ret}>{linkname}</a>;
                        }
                        return <td key={key}>{ret}</td>;
                      })
                    }
                  </tr>
                );
              })
            }
        </tbody>
      </table>
      </div>
    );
  }

  renderSurvey() {
    const { survey } = this.props.api.open;

    if (survey.status === C.STATUS_LOADING) {
      return <div>loading...</div>;
    }

    if (survey.status === C.STATUS_ERROR) {
      return <div>Error: {survey.data}</div>;
    }

    const { formId } = this.props.match.params;

    const { dateTo, dateFrom, filterByDate } = this.state;

    return (
      <div>

        <div className="btn-toolbar pull-right">
        <Toggle
          checked={survey.data.archived} onToggle={this.toggleArchived}
          textChecked='RESTORE' textUnchecked='ARCHIVE'
        />
        <DownloadButton
          href={`/api/survey/download/zip/by-form-id/${formId}`}
        >
          Survey as ZIP
        </DownloadButton>
        <CSVLink
          data={this.getCSV()}
          filename={`${formId}.csv`}
        ><button type="button" className="btn btn-primary">
        <span
          className="glyphicon glyphicon-download"
          aria-hidden="true"
        /> Table as CSV
        </button>
        </CSVLink>

        </div>

        <h2>
          {survey.data.formTitle}<br />
          <small>{survey.data.formId}</small>
        </h2>

        <div>
        <div className='checkbox-inline'>
          <label htmlFor='filterByDate'>
          <input
            id='filterByDate'
            type="checkbox"
            onChange={() => this.setState({ filterByDate: !filterByDate })}
            checked={filterByDate}
          />
          Filter by date
          </label>
        </div>
        <div style={{ padding: '0.6em', display: 'inline-block' }}>
        <DatePicker
          disabled={!filterByDate} maxDate={dateTo}
          dateFormat={C.DATE_FORMAT} selected={dateFrom}
          onChange={(date) => this.setState({ dateFrom: date })}
        />
        </div>
        <span>-</span>
        <div style={{ padding: '0.6em', display: 'inline-block' }}>
        <DatePicker
          disabled={!filterByDate} minDate={dateFrom}
          dateFormat={C.DATE_FORMAT} selected={dateTo}
          onChange={(date) => this.setState({ dateTo: date })}
        />
        </div>
        </div>


        {this.renderTable()}
      </div>
    );
  }


  render() {
    return this.renderSurvey();
  }
}

const mapStateToProps = state => ({ auth: state.auth, api: state.api });

const mapDispatchToProps = {
  getSurveyById
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveySingle);

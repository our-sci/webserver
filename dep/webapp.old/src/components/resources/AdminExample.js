import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getAdminExample } from '../../actions/api';
import C from '../../constants';


class AdminExample extends Component {

  constructor(props) {
    super(props);

    this.renderExample = this.renderExample.bind(this);
  }

  componentDidMount() {
    const { token } = this.props.auth;
    if (token) {
      this.props.getAdminExample(token);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.auth.token && nextProps.auth.token) {
      this.props.getAdminExample(nextProps.auth.token);
    }
  }

  renderExample() {
    const { example } = this.props.api.admin;

    if (example.status === C.STATUS_LOADING) {
      return <li>loading...</li>;
    }

    if (example.status === C.STATUS_ERROR) {
      return <li>Error: {example.data}</li>;
    }

    return <li>Action: {example.data.action}, api: {example.data.api}</li>;
  }

  render() {
      return (
        <div>
        <h2>Admin</h2>
        <ul>
        {this.renderExample()}
        </ul>
        </div>
      );
  }

}

const mapStateToProps = state => ({ auth: state.auth, api: state.api });

const mapDispatchToProps = {
  getAdminExample
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminExample);

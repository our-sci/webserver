import axios from 'axios';

import C from '../constants';


export const getClientTest = (token) => (dispatch) => {
    dispatch({ type: C.API_CLIENT_TEST_LOADING });

    axios({
      url: '/api/client/test',
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'X-Authorization-Firebase': token
      }
      })
      .then((response) => dispatch({
        type: C.API_CLIENT_TEST_SUCCESS,
        payload: response.data
      }))
      .catch((error) => dispatch({
        type: C.API_CLIENT_TEST_ERROR,
        payload: error.message
      }));
};

export const getAdminExample = (token) => (dispatch) => {
    axios({
      url: '/api/admin/example',
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'X-Authorization-Firebase': token
      }
      })
      .then((response) => dispatch({
        type: C.API_ADMIN_EXAMPLE_SUCCESS,
        payload: response.data
      }))
      .catch((error) => dispatch({
        type: C.API_ADMIN_EXAMPLE_ERROR,
        payload: error.message
      }));
};

export const getClientExample = (token) => (dispatch) => {
  dispatch({ type: C.API_CLIENT_EXAMPLE_LOADING });

    axios({
      url: '/api/client/example',
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'X-Authorization-Firebase': token
      }
      })
      .then((response) => dispatch({
        type: C.API_CLIENT_EXAMPLE_SUCCESS,
        payload: response.data
      }))
      .catch((error) => dispatch({
        type: C.API_CLIENT_EXAMPLE_ERROR,
        payload: error.message
      }));
};

export const getOpenExample = () => (dispatch) => {
    axios({
      url: '/api/open/example',
      method: 'get',
      headers: {
        'Content-Type': 'application/json'

      }
      })
      .then((response) => dispatch({
        type: C.API_OPEN_EXAMPLE_SUCCESS,
        payload: response.data
      }))
      .catch((error) => dispatch({
        type: C.API_OPEN_EXAMPLE_ERROR,
        payload: error.message
      }));
};


export const getSurveys = () => (dispatch) => {
    axios({
      url: '/api/survey/list',
      method: 'get',
      headers: {
        'Content-Type': 'application/json'
      }
      })
      .then((response) => dispatch({
        type: C.API_OPEN_SURVEYS_SUCCESS,
        payload: response.data
      }))
      .catch((error) => dispatch({
        type: C.API_OPEN_SURVEYS_ERROR,
        payload: error.message
      }));
};

export const getScripts = () => (dispatch) => {
    dispatch({
      type: C.API_SCRIPT_LIST_LOADING
    });

    axios({
      url: '/api/script/list',
      method: 'get',
      headers: {
        'Content-Type': 'application/json'
      }
      })
      .then((response) => dispatch({
        type: C.API_SCRIPT_LIST_SUCCESS,
        payload: response.data
      }))
      .catch((error) => dispatch({
        type: C.API_SCRIPT_LIST_ERROR,
        payload: error.message
      }));
};

export const getScriptByScriptId = (scriptId) => (dispatch) => {
  dispatch({
    type: C.API_SCRIPT_ID_LOADING
  });

  axios({
    url: `/api/script/by-script-id/${scriptId}`,
    method: 'get'
  })
  .then((response) => dispatch({
    type: C.API_SCRIPT_ID_SUCCESS,
    payload: response.data
  }))
  .catch((error) => dispatch({
    type: C.API_SCRIPT_ID_ERROR,
    payload: error.message
  }));
};

export const getScriptByDatabaseId = (databaseId) => (dispatch) => {
  dispatch({
    type: C.API_SCRIPT_ID_LOADING
  });

  axios({
    url: `/api/script/by-database-id/${databaseId}`,
    method: 'get'
  })
  .then((response) => dispatch({
    type: C.API_SCRIPT_ID_SUCCESS,
    payload: response.data
  }))
  .catch((error) => dispatch({
    type: C.API_SCRIPT_ID_ERROR,
    payload: error.message
  }));
};


export const getClientProfile = (token) => (dispatch) => {
    axios({
      url: '/api/profile/self',
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'X-Authorization-Firebase': token
      }
      })
      .then((response) => dispatch({
        type: C.API_CLIENT_PROFILE_SUCCESS,
        payload: response.data
      }))
      .catch((error) => dispatch({
        type: C.API_CLIENT_PROFILE_ERROR,
        payload: error.message
      }));
};

export const getSurveyById = (formId) => (dispatch) => {
  axios({
    url: `/api/survey/by-form-id/${formId}`,
    method: 'get'
  })
  .then((response) => dispatch({
    type: C.API_OPEN_SURVEYS_ID_SUCCESS,
    payload: response.data
  }))
  .catch((error) => dispatch({
    type: C.API_OPEN_SURVEYS_ID_ERROR,
    payload: error.message
  }));
};

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { HashRouter } from 'react-router-dom';

import store from './store';
import { listenToAuth } from './actions/auth';
import Page from './components/Page';


export default class App extends Component {
  componentWillMount() {
    store.dispatch(listenToAuth());
  }

  render() {
    return (
      <Provider store={store}>
        <HashRouter>
        <Page />
        </HashRouter>
      </Provider>
    );
  }
}

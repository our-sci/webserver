import C from '../constants';

const INITIAL_STATE = {
  open: {
    example: null,
    surveys: null
  },
  client: {
    example: null,
    test: null,
    profile: null
  },
  admin: {
    example: { data: null, status: null, error: null }
  }

};

export default (state, action) => {
  switch (action.type) {
    case C.API_CLIENT_TEST_SUCCESS:
      return {
        ...state,
        client: {
          ...state.client,
          test: action.payload
        }
      };
    case C.API_CLIENT_TEST_ERROR:
      return {
        ...state,
        client: {
          ...state.client,
          test: null
        }
      };
    case C.API_CLIENT_EXAMPLE_SUCCESS:
      return {
        ...state,
        client: {
          ...state.client,
          example: action.payload
        }
      };
    case C.API_CLIENT_EXAMPLE_ERROR:
      return {
        ...state,
        client: {
          ...state.client,
          example: null
        }
      };
    case C.API_CLIENT_PROFILE_SUCCESS:
      return {
        ...state,
        client: {
            ...state.client,
          profile: action.payload
        }
      };
    case C.API_CLIENT_PROFILE_ERROR:
      return {
        ...state,
        client: {
          ...state.client,
          profile: null
        }
      };
    case C.API_OPEN_EXAMPLE_SUCCESS:
      return {
        ...state,
        open: {
          example: action.payload
        }
      };
    case C.API_OPEN_EXAMPLE_ERROR:
      return {
        ...state,
        open: {
          example: null
        }
      };
    case C.API_OPEN_SURVEYS_SUCCESS:
      return {
        ...state,
        open: {
          ...state.open,
          surveys: action.payload
        }
      };
    case C.API_OPEN_SURVEYS_ERROR:
      return {
        ...state,
        open: {
          ...state.open,
          surveys: null
        }
      };
    case C.API_ADMIN_EXAMPLE_SUCCESS:
      return {
        ...state,
        admin: {
          example: {
            data: action.payload,
            status: null,
            error: null
          }
        }
      };
    case C.API_ADMIN_EXAMPLE_ERROR:
      return {
        ...state,
        admin: {
          example: {
            data: null,
            status: null,
            error: action.payload
          }
        }
      };
    default:
      return state || INITIAL_STATE;
  }
};

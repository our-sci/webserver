import C from '../constants';

const INITIAL_STATE = {
  example: {
    data: null,
    status: C.STATUS_LOADING
  },
  test: {
    data: null,
    status: C.STATUS_LOADING
  },
  profile: {
    data: null,
    status: C.STATUS_LOADING
  }
};

export default (state, action) => {
  switch (action.type) {
    /* api/client/example */
    case C.API_CLIENT_EXAMPLE_LOADING:
      return { ...state, example: { data: null, status: C.STATUS_LOADING } };
    case C.API_CLIENT_EXAMPLE_SUCCESS:
      return { ...state, example: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_CLIENT_EXAMPLE_ERROR:
      return { ...state, example: { data: action.payload, status: C.STATUS_ERROR } };
    /* api/client/test */
    case C.API_CLIENT_TEST_LOADING:
      return { ...state, test: { data: null, status: C.STATUS_LOADING } };
    case C.API_CLIENT_TEST_SUCCESS:
      return { ...state, test: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_CLIENT_TEST_ERROR:
      return { ...state, test: { data: action.payload, status: C.STATUS_ERROR } };
    /* api/client/profile */
    case C.API_CLIENT_PROFILE_LOADING:
      return { ...state, profile: { data: null, status: C.STATUS_LOADING } };
    case C.API_CLIENT_PROFILE_SUCCESS:
      return { ...state, profile: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_CLIENT_PROFILE_ERROR:
      return { ...state, profile: { data: action.payload, status: C.STATUS_ERROR } };
    default:
    return state || INITIAL_STATE;
  }
};

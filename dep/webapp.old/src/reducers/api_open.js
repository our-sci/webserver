import C from '../constants';

const INITIAL_STATE = {
  example: {
    data: null,
    status: C.STATUS_LOADING
  },
  surveys: {
    data: null,
    status: C.STATUS_LOADING
  },
  survey: {
    data: null,
    status: C.STATUS_LOADING
  }
};

export default (state, action) => {
  switch (action.type) {
    case C.API_OPEN_EXAMPLE_LOADING:
      return { ...state, example: { data: null, status: C.STATUS_LOADING } };
    case C.API_OPEN_EXAMPLE_SUCCESS:
      return { ...state, example: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_OPEN_EXAMPLE_ERROR:
      return { ...state, example: { data: action.payload, status: C.STATUS_ERROR } };
    case C.API_OPEN_SURVEYS_LOADING:
      return { ...state, surveys: { data: null, status: C.STATUS_LOADING } };
    case C.API_OPEN_SURVEYS_SUCCESS:
      return { ...state, surveys: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_OPEN_SURVEYS_ERROR:
      return { ...state, surveys: { data: action.payload, status: C.STATUS_ERROR } };
    case C.API_OPEN_SURVEYS_ID_LOADING:
      return { ...state, survey: { data: null, status: C.STATUS_LOADING } };
    case C.API_OPEN_SURVEYS_ID_SUCCESS:
      return { ...state, survey: { data: action.payload, status: C.STATUS_SUCCESS } };
    case C.API_OPEN_SURVEYS_ID_ERROR:
      return { ...state, survey: { data: action.payload, status: C.STATUS_ERROR } };
    default:
      return state || INITIAL_STATE;
  }
};

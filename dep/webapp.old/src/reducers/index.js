import { combineReducers } from 'redux';

import auth from './auth';
import feedback from './feedback';
import open from './api_open';
import client from './api_client';
import admin from './api_admin';
import api from './api';
import scripts from './scripts';


const rootReducer = combineReducers({
  auth,
  feedback,
  api: combineReducers({ open, client, admin }),
  scripts
});

export default rootReducer;
